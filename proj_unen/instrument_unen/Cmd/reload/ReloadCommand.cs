﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Cmd;

namespace UNen.Cmd
{
    public class ReloadCommand : AbstractCommand
    {
        public static readonly string CmdName = "reload";
        public override string Id => CmdName;

        public ReloadCommand()
        {
            m_options.Clear();
            AddCmdOption(new ReloadAllOption(this));
            //AddCmdOption(new PrintSysConfOption(this));
            //AddCmdOption(new PrintDataTableOption(this));
            //AddCmdOption(new PrintSliceDataTableOption(this));

            DefaultOption = m_options[0];
        }
    }
}
