﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UKon.Cmd;

namespace UNen.Cmd
{
    class OpenSystemPathOption : AbstractCmdOption
    {
        public override string type => "system_path";

        private readonly string[] m_name = new string[] { "--system_path", "-p" };
        public override string[] names => m_name;

        public override uint ArgumentCnt => 1;

        public OpenSystemPathOption(OpenCommand cmd)
            : base(cmd)
        {
        }

        public override bool Process(string[] args)
        {
            string path = args[2];
            path = path.ToLower();
            switch (path)
            {
                case "root":
                    OpenRoot();
                    break;
                default:
                    OpenSystemPath(path);
                    break;
            }

            return true;
        }

        private void OpenRoot()
        {
            ////AppDomain.CurrentDomain.BaseDirectory

            System.Diagnostics.Process.Start("explorer.exe",
                AppDomain.CurrentDomain.BaseDirectory);
        }

        private void OpenSystemPath(string name)
        {

            string path = string.Empty;
            if (name.StartsWith("sys_") == true)
            {
                string index = name.Substring(4);
                bool result = int.TryParse(index, out int value);
                if (result == true)
                {
                    path = ConfigMgr.sysconf.GetPathByIndex(value);
                }
            }
            else
            {
                path = ConfigMgr.sysconf.GetPathByName(name);
            }

            if (path == string.Empty)
            {
                DebugMgr.LogError("路径无法识别");
                return;
            }

            if (Directory.Exists(path) == true)
            {
                System.Diagnostics.Process.Start("explorer.exe", path);
            }
            else
            {
                DebugMgr.LogError($"path={path}");
                DebugMgr.LogError("该路径尚不存在");
            }
        }
    }
}
