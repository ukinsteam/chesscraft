﻿using UKon.Cmd;

namespace UNen.Cmd
{
    class BuildCommand : AbstractCommand
    {
        public static readonly string CmdName = "build";
        public override string Id => CmdName;

        public BuildCommand()
        {
            m_options.Clear();
            //cmdoptions.Add(new CommonAllOption(this));
            m_options.Add(new BuildAloneOption(this));
            m_options.Add(new BuildUniteOption(this));

            //DefaultOption = m_options[1];
        }
    }
}
