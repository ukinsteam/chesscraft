﻿using UKon.Cmd;

namespace UNen.Cmd
{
    public class HelpDefaultOption : AbstractCmdOption
    {

        public override string type => "default";
        private readonly string[] m_name = new string[] { "--default", "-d" };
        public override string[] names => m_name;

        public HelpDefaultOption(HelpCommand cmd)
           : base(cmd)
        {

        }

        public override bool Process(string[] args)
        {
            return true;
        }

    }
}
