﻿using UKon.Cmd;
using UNen.Table;

namespace UNen.Cmd
{
    public class PrintSliceDataTableOption : AbstractCmdOption
    {
        public override string type => "datatablepath";
        private readonly string[] m_name = new string[] { "--datatablepath", "-dtp" };
        public override string[] names => m_name;

        public override uint ArgumentCnt => 1;

        public PrintSliceDataTableOption(PrintCommand cmd)
           : base(cmd)
        {

        }
        public override bool Process(string[] args)
        {
            int argementidx = GetArgumentIdx(args);
            if (args.Length <= argementidx)
            {
                DebugMgr.LogWarning("Warning! 请输入对应参数。");
                DebugMgr.WriteLine();
                return true;
            }
            string path = args[argementidx];
            DiscreteVO dvo = ConfigMgr.tableConf.FindDiscreteVOByTablePath(args[argementidx]);

            if (dvo == null)
            {
                DebugMgr.LogError("Error!  配置表路径不存在。");
                var tempvo = ConfigMgr.tableConf.FindDiscreteVOByTablePath(args[argementidx].ToLower().Replace('\\', '/'));
                if (tempvo != null)
                {
                    DebugMgr.LogError("\t配置表路径大小写敏感。");
                    DebugMgr.LogError("\t配置表路径需要使用反斜杠（'/'）。");
                }
                DebugMgr.WriteLine();
                return true;
            }

            var datatable = dvo.GetDataTable();
            if (datatable == null)
            {
                DebugMgr.LogWarning("Warning! 配置表数据尚未加载。");
                DebugMgr.Log("请先输入指令scan --data");
                DebugMgr.WriteLine();
                return true;
            }
            DebugMgr.Log(datatable.ToString());
            DebugMgr.WriteLine();
            return true;
        }
    }
}
