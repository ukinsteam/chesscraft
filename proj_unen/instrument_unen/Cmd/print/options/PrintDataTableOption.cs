﻿using UKon.Cmd;
using UNen.Table;

namespace UNen.Cmd
{
    public class PrintDataTableOption : AbstractCmdOption
    {
        public override string type => "datatable";
        private readonly string[] m_name = new string[] { "--datatable", "-dt" };
        public override string[] names => m_name;

        public override uint ArgumentCnt => 1;

        public PrintDataTableOption(PrintCommand cmd)
           : base(cmd)
        {

        }

        //DiscreteVO dvo = ConfigMgr.tableConf.FindDiscreteVOByTablePath(args[argementidx]);
        public override bool Process(string[] args)
        {
            int argementidx = GetArgumentIdx(args);
            if (args.Length <= argementidx)
            {
                DebugMgr.LogWarning("Warning! 请输入对应参数。");
                DebugMgr.WriteLine();
                return true;
            }

            string tablename = args[argementidx];
            var tvo = ConfigMgr.tableConf.FindVO(tablename);
            if (tvo == null)
            {
                DebugMgr.LogError("Error! 配置表不存在。");
                DebugMgr.WriteLine();
                return true;
            }

            var datatable = tvo.GetDataTable();
            DebugMgr.Log(datatable.ToString());
            DebugMgr.WriteLine();
            return true;
        }
    }
}
