﻿using ETT.Table;
using ETT.Table.Analysis;
using System;
using System.Collections.Generic;
using System.IO;
using UNen.DT;
using UNen.Protocol;
using UNen.Table;

namespace UNen.Cmd
{
    class GeneProtoUtils
    {
        private enum ESwitchLine
        {
            None,
            Single,
            Multi
        }

        private static bool CheckProtoList(List<ProtFieldVO> fields)
        {
            foreach (var field in fields)
            {
                if (field.Type.FieldType == EFieldType.List)
                {
                    return true;
                }
            }
            return false;
        }


        private static void WriteProtoMgr(StreamWriter sw)
        {
            var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine("using UKon.Proto;");
            sw.WriteLine();
            sw.WriteLine("namespace {0}", SysDefine.RootNamespace);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic partial class ProtoMgr");
            sw.WriteLine("\t{");

            sw.WriteLine("\t\tprivate void InnerInit()");
            sw.WriteLine("\t\t{");
            foreach (string fieldtype in typeset)
            {
                TypeParser parser = new TypeParser(fieldtype);
                //string[] elements = fieldtype.Split(':');
                int cnt = ConfigMgr.protoConf.GetEnumCnt(parser.FirstElementType);
                string firstname = parser.FirstElementType;
                bool isenum = cnt > 0;

                if (parser.FieldType == EFieldType.List)
                {
                    if (isenum == true)
                    {
                        sw.WriteLine($"\t\t\tm_dict.Add(\"{fieldtype}\", new Proto{firstname}Decoder(EProtoDecoderType.List));");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\tm_dict.Add(\"{fieldtype}\", new CustomProtoVOListDecoder<{firstname}>());");
                    }
                }
                else if (parser.FieldType == EFieldType.Array)
                {
                    if (isenum == true)
                    {
                        sw.WriteLine($"\t\t\tm_dict.Add(\"{fieldtype}\", new Proto{firstname}Decoder(EProtoDecoderType.Array)));");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\tm_dict.Add(\"{fieldtype}\", new CustomProtoVOArrayDecoder<{firstname}>());");
                    }
                }
                else if (parser.FieldType == EFieldType.Single)
                {
                    if (isenum == true)
                    {
                        sw.WriteLine($"\t\t\tm_dict.Add(\"{firstname}\", new Proto{firstname}Decoder(EProtoDecoderType.Simple));");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\tm_dict.Add(\"{firstname}\", new CustomProtoVODecoder<{firstname}>());");
                    }
                }
                //sw.WriteLine();
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneProtoMgr(string path)
        {
            string filepath = Path.Combine(path, "ProtoMgr.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoMgr(sw);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteProtoStruct(StreamWriter sw, ProtStructureVO vo)
        {
            var fields = vo.FindAll();
            bool iscontainlist = CheckProtoList(fields);

            sw.WriteLine("using ETT.Model.proto;");
            sw.WriteLine("using System.IO;");
            if (iscontainlist == true)
            {
                sw.WriteLine("using System.Collections.Generic;");
            }
            sw.WriteLine();
            sw.WriteLine($"namespace {SysDefine.RootNamespace}");
            sw.WriteLine("{");
            //string clsstr = vo.IsStruct == true ? "struct" : "class";
            if (vo.IsPartial == true)
            {
                sw.WriteLine($"\tpublic partial class {vo.Name} : AbstractProtoPacker");
            }
            else
            {
                sw.WriteLine($"\tpublic class {vo.Name} : AbstractProtoPacker");
            }

            sw.WriteLine("\t{");


            foreach (var field in fields)
            {
                //string[] elements = field.Type.Split(':');
                string firstname = field.Type.FirstElementType;
                string fieldname = field.Name;
                sw.WriteLine("\t\t/// <summary>");
                sw.WriteLine($"\t\t/// {field.Desc}");
                sw.WriteLine("\t\t/// <summary>");
                switch (field.Type.FieldType)
                {
                    case EFieldType.Single:
                        sw.WriteLine($"\t\tpublic {DTDefine.GetTypeByString(firstname)} {fieldname};");
                        break;
                    case EFieldType.List:
                        sw.WriteLine($"\t\tpublic List<{DTDefine.GetTypeByString(firstname)}> {fieldname};");
                        break;
                    case EFieldType.Array:
                        sw.WriteLine($"\t\tpublic {DTDefine.GetTypeByString(firstname)}[] {fieldname};");
                        break;
                }
                sw.WriteLine();
            }

            //ESwitchLine flag = ESwitchLine.None;
            //Encode Method
            sw.WriteLine("\t\tpublic override void Pack(BinaryWriter writer)");
            sw.WriteLine("\t\t{");
            foreach (var field in fields)
            {
                //string[] elements = field.Type.Split(':');
                bool isSysType = SysDefine.CheckType(field.Type.FirstElementType, SysDefine.ProtoTypeConst);
                int enumcnt = ConfigMgr.protoConf.GetEnumCnt(field.Type.FirstElementType);
                if (field.Type.FieldType == EFieldType.Single)
                {
                    if (isSysType == true)
                    {
                        sw.WriteLine($"\t\t\twriter.Write({field.Name});");
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine($"\t\t\twriter.Write((int){field.Name});");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\t{field.Name}.Pack(writer);");
                    }
                }
                else if (field.Type.FieldType == EFieldType.List)
                {
                    sw.WriteLine($"\t\t\twriter.Write({field.Name}.Count);");
                    sw.WriteLine($"\t\t\tfor (int i = 0; i < {field.Name}.Count; i++)");
                    sw.WriteLine("\t\t\t{");
                    if (isSysType == true)
                    {
                        sw.WriteLine($"\t\t\t\twriter.Write({field.Name}[i]);");
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine($"\t\t\t\twriter.Write((int){field.Name}[i]);");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\t\t{field.Name}[i].Pack(writer);");
                    }

                    sw.WriteLine("\t\t\t}");
                }
                else if (field.Type.FieldType == EFieldType.Array)
                {
                    sw.WriteLine($"\t\t\twriter.Write({field.Name}.Length);");
                    sw.WriteLine($"\t\t\tfor (int i = 0; i < {field.Name}.Length; i++)");
                    sw.WriteLine("\t\t\t{");
                    //bool isSysType = SysDefine.CheckType(elements[0], SysDefine.ProtoTypeConst);
                    if (isSysType == true)
                    {
                        sw.WriteLine($"\t\t\t\twriter.Write({field.Name}[i]);");
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine($"\t\t\t\twriter.Write((int){field.Name}[i]);");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\t\t{field.Name}[i].Pack(writer);");
                    }

                    sw.WriteLine("\t\t\t}");
                }
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine();
            //flag = ESwitchLine.None;
            //Decode Method
            sw.WriteLine("\t\tpublic override void UnPack(BinaryReader reader)");
            sw.WriteLine("\t\t{");
            foreach (var field in fields)
            {
                string firstname = field.Type.FirstElementType;
                string fieldname = field.Name;
                int enumcnt = ConfigMgr.protoConf.GetEnumCnt(firstname);
                bool isSysType = SysDefine.CheckType(firstname, SysDefine.ProtoTypeConst);

                if (field.Type.FieldType == EFieldType.Single)
                {
                    if (isSysType == true)
                    {
                        sw.WriteLine($"\t\t\t{fieldname} = {SupportFieldType.GetBinaryReaderMethodName(firstname, "reader")};");
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine($"\t\t\t{fieldname} = ({firstname}){SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "reader")};");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\t{fieldname}.UnPack(reader);");
                    }
                }
                else if (field.Type.FieldType == EFieldType.List)
                {
                    sw.WriteLine("\t\t\tint len = reader.ReadInt32();");
                    sw.WriteLine($"\t\t\t{fieldname} = new List<{firstname}>(len); ");
                    sw.WriteLine("\t\t\tfor (int i = 0; i < len; i++)");
                    sw.WriteLine("\t\t\t{");
                    if (isSysType == true)
                    {
                        sw.WriteLine($"\t\t\t\t{fieldname}[i] = {SupportFieldType.GetBinaryReaderMethodName(firstname, "reader")};");
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine($"\t\t\t\t{fieldname}[i] = ({firstname}){SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "reader")};");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\t\t{fieldname}.Add(new {firstname}());");
                        sw.WriteLine($"\t\t\t\t{fieldname}[i].UnPack(reader);");
                    }

                    sw.WriteLine("\t\t\t}");
                }
                else if (field.Type.FieldType == EFieldType.Array)
                {
                    sw.WriteLine("\t\t\tint len = reader.ReadInt32();");
                    sw.WriteLine($"\t\t\t{fieldname} = new {firstname}[len]; ");
                    sw.WriteLine("\t\t\tfor (int i = 0; i < len; i++)");
                    sw.WriteLine("\t\t\t{");
                    if (isSysType == true)
                    {
                        sw.WriteLine($"\t\t\t\t{fieldname}[i] = {SupportFieldType.GetBinaryReaderMethodName(firstname, "reader")};");
                    }
                    else if (enumcnt > 0)
                    {
                        sw.WriteLine($"\t\t\t\t{fieldname}[i] = ({firstname}){SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "reader")};");
                    }
                    else
                    {
                        sw.WriteLine($"\t\t\t\t{fieldname}[i] = new {firstname}();");
                        sw.WriteLine($"\t\t\t\t{fieldname}[i].UnPack(reader);");
                    }

                    sw.WriteLine("\t\t\t}");
                }
            }
            sw.WriteLine("\t\t}");

            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneProtoStruct(ProtStructureVO vo, string path)
        {
            string srcpath = System.IO.Path.Combine(path, vo.Name);

            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            string filepath = System.IO.Path.Combine(info.DirectoryName, $"{vo.Name}.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoStruct(sw, vo);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteProto(StreamWriter sw, DiscreteVO vo)
        {
            var tb = vo.GetDataTable();
            var tablevo = vo.TableVO;
            //var exportfields = tablevo.FindAllExportFields();

            var modulevo = vo.Owner;
            var featurevo = modulevo.Owner;

            //var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine("using ETT.Model;");
            sw.WriteLine("using ETT.Model.proto;");
            sw.WriteLine("using System.IO;");
            sw.WriteLine("using System.Collections.Generic;");

            sw.WriteLine();
            if (string.IsNullOrWhiteSpace(modulevo.Name) == true)
            {
                sw.WriteLine($"namespace {SysDefine.RootNamespace}.{featurevo.Name}");
            }
            else
            {
                sw.WriteLine($"namespace {SysDefine.RootNamespace}.{featurevo.Name}");
            }
            sw.WriteLine("{");
            //sw.WriteLine($"\tpublic class {disname}RecvServer : AbstractRecvServer");
            //sw.WriteLine("\t{");

            for (int i = 0; i < tb.Rows.Count; ++i)
            {
                GenericDataRow row = tb.Rows[i];
                string proto_name = row["Name"][0].Value;
                string proto_id = row["Id"][0].Value;
                string proto_desc = row["Desc"][0].Value;
                string proto_from = row["From"][0].Value;
                string proto_to = row["To"][0].Value;
                string proto_style = row["Style"][0].Value;
                string proto_isactor = row["IsActor"][0].Value;

                if (SysDefine.isClient == true && !(proto_from.ToLower() == "client" || proto_to.ToLower() == "client"))
                {
                    continue;
                }
                bool isactor = proto_isactor.ToLower() == "true" ? true : false;
                if (isactor == true)
                {
                    //DebugMgr.Log("isactor");
                }
                var proto_param_names = row["Parameter.Name"];
                var proto_param_types = row["Parameter.Type"];
                var proto_param_descs = row["Parameter.Desc"];
                int len = proto_param_names.Count;

                sw.WriteLine();
                sw.WriteLine("\t/// <summary>");
                sw.WriteLine($"\t/// {proto_desc} 【{proto_from}, {proto_to}】");
                sw.WriteLine("\t/// </summary>");
                //for (int k = 0; k < len; k++)
                //{
                //    string name = proto_param_names[k].Value;
                //    string desc = proto_param_descs.Count > k ? proto_param_descs[k].Value : "";
                //    sw.WriteLine($"\t\t/// <param name=\"{name}\">{desc}</param>");
                //}
                //sw.WriteLine($"\t\t[Message((uint)E{disname}Proto.{CmdUtils.ChangeNameFormatToUnderLineStype(proto_name)})]");
                char c_from = CmdUtils.GetFirstChar(proto_from);
                char c_to = CmdUtils.GetFirstChar(proto_to);
                string s_style = string.Empty;
                switch (proto_style)
                {
                    case "Req":
                        s_style = isactor == true ? "AbstractLocationRequest" : "AbstractRequest";
                        break;
                    case "Res":
                        s_style = isactor == true ? "AbstractLocationResponse" : "AbstractResponse";
                        break;
                    case "Ntf":
                        s_style = isactor == true ? "AbstractLocationMessage" : "AbstractMessage";
                        break;
                }
                string s_proto = $"{c_from}2{c_to}_{proto_style}_{proto_name}";
                sw.WriteLine($"\t[Message((uint)E{modulevo.Name}Proto.{s_proto})]");
                sw.WriteLine($"\tpublic class {s_proto} : {s_style}");
                sw.WriteLine("\t{");
                #region Field
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string type = proto_param_types.Count > k ? proto_param_types[k].Value : "";
                    //string[] elements = type.Split(':');
                    if (string.IsNullOrWhiteSpace(name) == true)
                    {
                        continue;
                    }
                    TypeParser parser = new TypeParser(type);

                    string newtype = type;
                    switch (parser.FieldType)
                    {
                        case EFieldType.List:
                            newtype = $"List<{parser.FirstElementType}>";
                            break;
                        case EFieldType.Array:
                            newtype = $"{parser.FirstElementType}[]";
                            break;
                        case EFieldType.Dict:
                            newtype = $"Dictionary<{parser.FirstElementType}, {parser.SecondElementType}>";
                            break;
                        default: break;
                    }
                    sw.WriteLine($"\t\tpublic {newtype} {name} {{get; set;}}");
                }
                #endregion


                sw.WriteLine();
                sw.WriteLine("\t\tpublic override void Pack(BinaryWriter writer)");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tbase.Pack(writer);");
                #region Pack
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string type = proto_param_types.Count > k ? proto_param_types[k].Value : "";
                    //string[] elements = type.Split(':');
                    if (string.IsNullOrWhiteSpace(name) == true)
                    {
                        continue;
                    }
                    TypeParser parser = new TypeParser(type);
                    bool isSysType = SysDefine.CheckType(parser.FirstElementType, SysDefine.ProtoTypeConst);
                    int enumcnt = ConfigMgr.protoConf.GetEnumCnt(parser.FirstElementType);

                    //string newtype = type;
                    switch (parser.FieldType)
                    {
                        case EFieldType.List:
                            {
                                sw.WriteLine($"\t\t\twriter.Write({name}.Count);");
                                sw.WriteLine($"\t\t\tfor (int i = 0; i < {name}.Count; i++)");
                                sw.WriteLine("\t\t\t{");
                                if (isSysType == true)
                                {
                                    sw.WriteLine($"\t\t\t\twriter.Write({name}[i]);");
                                }
                                else if (enumcnt > 0)
                                {
                                    sw.WriteLine($"\t\t\t\twriter.Write((int){name}[i]);");
                                }
                                else
                                {
                                    sw.WriteLine($"\t\t\t\t{name}[i].Pack(writer);");
                                }

                                sw.WriteLine("\t\t\t}");
                            }
                            break;
                        case EFieldType.Array:
                            {
                                sw.WriteLine($"\t\t\twriter.Write({name}.Length);");
                                sw.WriteLine($"\t\t\tfor (int i = 0; i < {name}.Length; i++)");
                                sw.WriteLine("\t\t\t{");
                                if (isSysType == true)
                                {
                                    sw.WriteLine($"\t\t\t\twriter.Write({name}[i]);");
                                }
                                else if (enumcnt > 0)
                                {
                                    sw.WriteLine($"\t\t\t\twriter.Write((int){name}[i]);");
                                }
                                else
                                {
                                    sw.WriteLine($"\t\t\t\t{name}[i].Pack(writer);");
                                }

                                sw.WriteLine("\t\t\t}");
                            }
                            break;
                        case EFieldType.Dict:
                            //newtype = $"Dictionary<{parser.FirstElementType}, {parser.SecondElementType}>";
                            break;
                        default:
                            {
                                if (isSysType == true)
                                {
                                    sw.WriteLine($"\t\t\twriter.Write({name});");
                                }
                                else if (enumcnt > 0)
                                {
                                    sw.WriteLine($"\t\t\twriter.Write((int){name});");
                                }
                                else
                                {
                                    sw.WriteLine($"\t\t\t{name}.Pack(writer);");
                                }
                            }
                            break;
                    }
                    //sw.WriteLine($"\t\t\twriter.Write({name});");
                }
                #endregion
                sw.WriteLine("\t\t}");

                sw.WriteLine();
                sw.WriteLine("\t\tpublic override void UnPack(BinaryReader reader)");
                sw.WriteLine("\t\t{");
                sw.WriteLine("\t\t\tbase.UnPack(reader);");
                #region UnPack
                for (int k = 0; k < len; k++)
                {
                    string name = proto_param_names[k].Value;
                    string type = proto_param_types.Count > k ? proto_param_types[k].Value : "";
                    //string[] elements = type.Split(':');
                    if (string.IsNullOrWhiteSpace(name) == true)
                    {
                        continue;
                    }
                    TypeParser parser = new TypeParser(type);
                    int enumcnt = ConfigMgr.protoConf.GetEnumCnt(parser.FirstElementType);
                    bool isSysType = SysDefine.CheckType(parser.FirstElementType, SysDefine.ProtoTypeConst);

                    //string newtype = type;
                    switch (parser.FieldType)
                    {
                        case EFieldType.List:
                            {
                                sw.WriteLine("\t\t\tint len = reader.ReadInt32();");
                                sw.WriteLine($"\t\t\t{name} = new List<{parser.FirstElementType}>(len); ");
                                sw.WriteLine("\t\t\tfor (int i = 0; i < len; i++)");
                                sw.WriteLine("\t\t\t{");
                                if (isSysType == true)
                                {
                                    sw.WriteLine($"\t\t\t\t{name}[i] = {SupportFieldType.GetBinaryReaderMethodName(parser.FirstElementType, "reader")};");
                                }
                                else if (enumcnt > 0)
                                {
                                    sw.WriteLine($"\t\t\t\t{name}[i] = ({parser.FirstElementType}){SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "reader")};");
                                }
                                else
                                {
                                    sw.WriteLine($"\t\t\t\t{name}.Add(new {parser.FirstElementType}());");
                                    sw.WriteLine($"\t\t\t\t{name}[i].UnPack(reader);");
                                }

                                sw.WriteLine("\t\t\t}");
                            }
                            break;
                        case EFieldType.Array:
                            //newtype = $"{parser.FirstElementType}[]";
                            break;
                        case EFieldType.Dict:
                            //newtype = $"Dictionary<{parser.FirstElementType}, {parser.SecondElementType}>";
                            break;
                        default:
                            {
                                if (isSysType == true)
                                {
                                    sw.WriteLine($"\t\t\t{name} = {SupportFieldType.GetBinaryReaderMethodName(parser.FirstElementType, "reader")};");
                                }
                                else if (enumcnt > 0)
                                {
                                    sw.WriteLine($"\t\t\t{name} = ({parser.FirstElementType}){SupportFieldType.GetBinaryReaderMethodName(DTDefine.INT, "reader")};");
                                }
                                else
                                {
                                    sw.WriteLine($"\t\t\t{name} = new {parser.FirstElementType}();");
                                    sw.WriteLine($"\t\t\t{name}.UnPack(reader);");
                                }
                            }
                            break;
                    }
                    //sw.WriteLine($"\t\t\t{name} = {SupportFieldType.GetBinaryReaderMethodName(parser.FirstElementType, "reader")};");
                }
                #endregion
                sw.WriteLine("\t\t}");

                //sw.Write(")");
                //sw.WriteLine();
                //sw.WriteLine("\t\t{");
                //sw.WriteLine("\t\t\treturn true;");
                sw.WriteLine("\t}");

            }


            //sw.WriteLine("\t}");
            sw.WriteLine("}");

        }

        public static bool GeneProto(DITableVO vo, DiscreteVO dvo, string path)
        {
            bool result = true;

            var modulevo = dvo.Owner;
            var featurevo = modulevo.Owner;

            string subpath = $"{modulevo.Name}Proto.cs";

            string srcpath = Path.Combine(path, subpath);

            FileStream fs1 = null;
            StreamWriter sw = null;
            try
            {
                fs1 = new FileStream(srcpath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProto(sw, dvo);
            }
            catch (Exception e)
            {
                DebugMgr.LogError(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }
        //private static bool GeneProtoSendServer(DITableVO vo, string path, string endpointname)
        //{
        //    var protocoltables = ConfigMgr.tableConf.FindDiscreteVOByTableName(vo.Name);

        //    bool result = true;
        //    foreach (var discretevo in protocoltables)
        //    {
        //        string disname = CmdUtils.ToUpperFirstChar(discretevo.Owner.Name);

        //        string modulename = $"{disname}Feature";
        //        string subpath = $"{modulename}/server/{disname}SendServer.cs";
        //        string srcpath = System.IO.Path.Combine(path, subpath);

        //        FileInfo info = new FileInfo(srcpath);

        //        if (Directory.Exists(info.DirectoryName) == false)
        //        {
        //            Directory.CreateDirectory(info.DirectoryName);
        //        }

        //        FileStream fs1 = null;
        //        StreamWriter sw = null;
        //        try
        //        {
        //            fs1 = new FileStream(srcpath, FileMode.Create, FileAccess.Write);//创建写入文件 
        //            sw = new StreamWriter(fs1);
        //            WriteProtoSendServer(sw, discretevo, disname, endpointname);
        //        }
        //        catch (Exception e)
        //        {
        //            DebugMgr.LogError(e.Message);
        //            result = false;
        //        }
        //        finally
        //        {
        //            if (sw != null)
        //            {
        //                sw.Close();
        //            }
        //            if (fs1 != null)
        //            {
        //                fs1.Close();
        //            }
        //        }
        //        if (result == false)
        //        {
        //            break;
        //        }
        //    }
        //    return result;
        //}

        //public static bool GeneAllProto(DITableVO vo, string path)
        //{
        //    bool result = false;
        //    result = GeneProto(vo, path);
        //    if (result == false)
        //    {
        //        DebugMgr.LogError("生成Proto文件失败");
        //        return false;
        //    }

        //    return result;
        //}

        private static void WriteProtoEnum(DiscreteVO dvo, StreamWriter sw)
        {
            //bool result = true;
            //foreach (var discretevo in protocoltables)
            //{
            var modulevo = dvo.Owner;
            var featurevo = modulevo.Owner;

            var typeset = ConfigMgr.protoConf.FindAllCustomType();
            sw.WriteLine($"namespace {SysDefine.RootNamespace}.{featurevo.Name}");
            sw.WriteLine("{");

            sw.WriteLine("\t/// <summary>");
            sw.WriteLine($"\t/// {modulevo.Name}协议枚举 Range[{dvo.KeyRangeVO.Min},{dvo.KeyRangeVO.Max})");
            sw.WriteLine("\t/// <summary>");
            //string name = CmdUtils.ToUpperFirstChar(vo.Owner.Name);
            sw.WriteLine($"\tpublic enum E{modulevo.Name}Proto");
            sw.WriteLine("\t{");

            var dt = dvo.GetDataTable();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                var row = dt.Rows[i];
                var id = row["Id"][0].Value;
                var proto_name = row["Name"][0].Value;
                var proto_from = row["From"][0].Value;
                var proto_to = row["To"][0].Value;
                var proto_style = row["Style"][0].Value;

                if (SysDefine.isClient == true && !(proto_from.ToLower() == "client" || proto_to.ToLower() == "client"))
                {
                    continue;
                }

                char c_from = CmdUtils.GetFirstChar(proto_from);
                char c_to = CmdUtils.GetFirstChar(proto_to);
                sw.WriteLine($"\t\t{c_from}2{c_to}_{proto_style}_{proto_name} = {id},");
            }

            sw.WriteLine("\t}");
            sw.WriteLine();

            sw.WriteLine("}");
        }

        public static bool GeneProtoEnum( DiscreteVO dvo, string path)
        {
            var modulevo = dvo.Owner;
            var featurevo = modulevo.Owner;

            string filepath = Path.Combine(path, $"E{modulevo.Name}Enum.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoEnum(dvo, sw);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteProtoEnumVO(StreamWriter sw, ProtEnumVO vo)
        {
            var fields = vo.FindAll();

            sw.WriteLine("using ETT.Model;");
            sw.WriteLine();
            sw.WriteLine($"namespace {SysDefine.RootNamespace}");
            sw.WriteLine("{");
            sw.WriteLine($"\tpublic enum {vo.Name}");
            sw.WriteLine("\t{");
            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                sw.WriteLine($"\t\t{field.Name} = {i},");

            }
            sw.WriteLine("\t}");
            //sw.WriteLine();

            //sw.WriteLine($"\tpublic class Proto{vo.Name}Decoder : AbstractProtoDecoder");
            //sw.WriteLine("\t{");
            //sw.WriteLine("\t\tprivate EProtoDecoderType m_type = EProtoDecoderType.Simple;");
            //sw.WriteLine($"\t\tpublic Proto{vo.Name}Decoder(EProtoDecoderType type)");
            //sw.WriteLine("\t\t{");
            //sw.WriteLine("\t\t\tm_type = type;");
            //sw.WriteLine("\t\t}");

            //sw.WriteLine();
            //sw.WriteLine("\t\tpublic override object Decode(ByteStream stream)");
            //sw.WriteLine("\t\t{");

            //sw.WriteLine("\t\t\tswitch (m_type)");
            //sw.WriteLine("\t\t\t{");
            //sw.WriteLine("\t\t\t\tcase EProtoDecoderType.Simple:");
            //sw.WriteLine("\t\t\t\t\treturn stream.ReadInt32();");
            //sw.WriteLine("\t\t\t\tcase EProtoDecoderType.List:");
            //sw.WriteLine("\t\t\t\t\t{");
            //sw.WriteLine("\t\t\t\t\t\tint len = stream.ReadInt32();");
            //sw.WriteLine($"\t\t\t\t\t\tList<{vo.Name}> lst = new List<{vo.Name}>(len);");
            //sw.WriteLine("\t\t\t\t\t\tfor (int i = 0; i < len; i++)");
            //sw.WriteLine("\t\t\t\t\t\t{");
            //sw.WriteLine($"\t\t\t\t\t\t\tlst.Add(({vo.Name})stream.ReadInt32());");
            //sw.WriteLine("\t\t\t\t\t\t}");
            //sw.WriteLine("\t\t\t\t\t\treturn lst;");
            //sw.WriteLine("\t\t\t\t\t}");
            //sw.WriteLine("\t\t\t\tcase EProtoDecoderType.Array:");
            //sw.WriteLine("\t\t\t\t\t{");
            //sw.WriteLine("\t\t\t\t\t\tint len = stream.ReadInt32();");
            //sw.WriteLine($"\t\t\t\t\t\t{vo.Name}[] arr = new {vo.Name}[len];");
            //sw.WriteLine("\t\t\t\t\t\tfor (int i = 0; i < len; i++)");
            //sw.WriteLine("\t\t\t\t\t\t{");
            //sw.WriteLine($"\t\t\t\t\t\t\tarr[i] = ({vo.Name})stream.ReadInt32();");
            //sw.WriteLine("\t\t\t\t\t\t}");
            //sw.WriteLine("\t\t\t\t\t\treturn arr;");
            //sw.WriteLine("\t\t\t\t\t}");
            //sw.WriteLine("\t\t\t\tdefault: break;");
            //sw.WriteLine("\t\t\t}");
            //sw.WriteLine($"\t\t\tthrow new Exception(\"Proto{vo.Name}Decoder .Decode Error!\");");

            //sw.WriteLine("\t\t}");
            //sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneProtoEnumVO(ProtEnumVO vo, string path)
        {
            string srcpath = System.IO.Path.Combine(path, vo.Name);

            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            string filepath = System.IO.Path.Combine(info.DirectoryName, $"{vo.Name}.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteProtoEnumVO(sw, vo);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

    }
}
