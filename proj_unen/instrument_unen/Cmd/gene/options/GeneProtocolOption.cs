﻿using UKon.Cmd;
using System.Collections.Generic;
using System.IO;
using UNen.Protocol;

namespace UNen.Cmd
{
    class GeneProtocolOption : AbstractCmdOption
    {
        public override string type => "protocol";
        private readonly string[] m_name = new string[] { "--protocol", "-p" };
        public override string[] names => m_name;

        public GeneProtocolOption(GeneCommand cmd)
            : base(cmd)
        {
        }

        private bool GeneFiles(string rootDir)
        {
            bool result = true;
            CmdUtils.ClearDir(rootDir);

            string structpath = Path.Combine(rootDir, "structs");
            List<ProtStructureVO> protolist = ConfigMgr.protoConf.FindAllStruct();
            foreach (var vo in protolist)
            {
                result = GeneProtoUtils.GeneProtoStruct(vo, structpath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. C#协议数据结构文件生成失败! structname = {0}", vo.Name));
                    return false;
                }
            }

            string enumpath = Path.Combine(rootDir, "enums");
            List<ProtEnumVO> enumlist = ConfigMgr.protoConf.FindAllEnum();
            foreach (var vo in enumlist)
            {
                result = GeneProtoUtils.GeneProtoEnumVO(vo, enumpath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. C#协议枚举文件生成失败! enumname = {0}", vo.Name));
                    return false;
                }
            }

            //result = GeneProtoUtils.GeneProtoEnum(rootDir);
            //if (result == true)
            //{
            //    DebugMgr.Log("Succ. ProtoEnum.cs生成成功!");
            //}
            //else
            //{
            //    DebugMgr.LogWarning("Fail. ProtoEnum.cs生成失败!");
            //    return false;
            //}

            //var dir = Path.Combine(rootDir, "generic");
            //var tablevo = ConfigMgr.tableConf.FindVO("ProtocolTable");
            //result = GeneProtoUtils.GeneAllProto(tablevo, dir);
            //if (result == true)
            //{
            //    DebugMgr.Log("Succ. Proto生成成功");
            //    //DebugMgr.WriteLine();
            //    //return false;
            //}
            //else
            //{
            //    DebugMgr.Log("Fail. Proto生成失败");
            //    return false;
            //}

            return true;
        }

        private void ClearDir(string rootDir)
        {
            var features = ConfigMgr.tableConf.FindAllFeature();

            foreach (var fvo in features)
            {
                string f_path = Path.Combine(rootDir, $"games/{fvo.Name.ToLower()}/generate/proto");
                CmdUtils.ClearDir(f_path);
            }
        }

        private bool GeneProtoFile(string rootDir)
        {
            var tvo = ConfigMgr.tableConf.FindVO("ProtocolTable");
            var features = ConfigMgr.tableConf.FindAllFeature();
            foreach (var featurevo in features)
            {
                var dis_list = featurevo.FindDiscreteVOsByName(tvo.Name);
                if (dis_list.Count == 0)
                {
                    continue;
                }
                string dir_path = Path.Combine(rootDir, $"games/{featurevo.Name}/generate/proto");
                foreach (var discretevo in dis_list)
                {
                    var resut = GeneProtoUtils.GeneProto(tvo, discretevo, dir_path);
                    if (resut == false)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private bool GeneProtoEnumFile(string rootDir)
        {
            var tvo = ConfigMgr.tableConf.FindVO("ProtocolTable");
            var features = ConfigMgr.tableConf.FindAllFeature();

            foreach (var featurevo in features)
            {
                var dis_list = featurevo.FindDiscreteVOsByName(tvo.Name);
                if (dis_list.Count == 0)
                {
                    continue;
                }
                string dir_path = Path.Combine(rootDir, $"games/{featurevo.Name}/generate/proto");
                foreach (var discretevo in dis_list)
                {
                    var resut = GeneProtoUtils.GeneProtoEnum(discretevo, dir_path);
                    if (resut == false)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public override bool Process(string[] args)
        {
            bool result = false;

            var clientdir = Path.Combine(ConfigMgr.sysconf.GeneCodeClientPath, "generate/Protocol");
            SysDefine.isClient = true;
            result = GeneFiles(clientdir);
            if (result == false)
            {
                DebugMgr.Log("client proto gene fail");
                return false;
            }

            ClearDir(ConfigMgr.sysconf.GeneCodeClientPath);

            result = GeneProtoFile(ConfigMgr.sysconf.GeneCodeClientPath);
            if (result == false)
            {
                DebugMgr.Log("client proto code gene fail");
                return false;
            }
            result = GeneProtoEnumFile(ConfigMgr.sysconf.GeneCodeClientPath);
            if (result == false)
            {
                DebugMgr.Log("client proto enum gene fail");
                return false;
            }
            //--------------------------Server----------------------------

            var serverdir = Path.Combine(ConfigMgr.sysconf.GeneCodeServerPath, "generate/Protocol");
            SysDefine.isClient = false;
            result = GeneFiles(serverdir);
            if (result == false)
            {
                DebugMgr.Log("server proto code gene fail");
                return false;
            }

            ClearDir(ConfigMgr.sysconf.GeneCodeServerPath);

            result = GeneProtoFile(ConfigMgr.sysconf.GeneCodeServerPath);
            if (result == false)
            {
                DebugMgr.Log("server proto code gene fail");
                return false;
            }
            result = GeneProtoEnumFile(ConfigMgr.sysconf.GeneCodeServerPath);
            if (result == false)
            {
                DebugMgr.Log("server proto enum gene fail");
                return false;
            }

            DebugMgr.Log("gene proto code succ");

            DebugMgr.WriteLine();
            return result;
        }
    }
}
