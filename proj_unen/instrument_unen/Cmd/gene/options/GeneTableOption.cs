﻿using System.Collections.Generic;
using System.IO;
using UKon.Cmd;
using UNen.DT;
using UNen.TableGeneric;

namespace UNen.Cmd
{
    class GeneTableOption : AbstractCmdOption
    {
        public override string type => "table";
        private readonly string[] m_name = new string[] { "--table", "-t" };
        public override string[] names => m_name;

        public GeneTableOption(GeneCommand cmd)
            : base(cmd)
        {
        }

        private bool GeneFiles(List<DITableVO> tablevos, string rootDir)
        {
            CmdUtils.ClearDir(rootDir);

            bool result = true;
            //生成Table文件
            string tablepath = Path.Combine(rootDir, "tables");
            foreach (var vo in tablevos)
            {
                if (vo.IsSeparate == false)
                {
                    result = GeneTableUtils.GeneCodeCSharp(vo, tablepath);
                }
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. Table解释文件生成失败! tablename = {0}", vo.Name));
                    return false;
                }
            }

            //生成Struct文件
            string structpath = Path.Combine(rootDir, "structs");
            List<TableStructureVO> protolist = ConfigMgr.tableGenericConf.FindAllStruct();
            foreach (var vo in protolist)
            {
                result = GeneTableStructUtils.GeneTableStruct(vo, structpath);
                if (result == false)
                {
                    DebugMgr.LogWarning(string.Format("Fail. TableStruct文件生成失败! structname = {0}", vo.Name));
                    return false;
                }
            }

            //生成TableMgr文件
            result = GeneTableUtils.GeneCodeCSharpMgr(tablevos, rootDir);
            if (result == false)
            {
                DebugMgr.LogWarning("Fail. TableMgr.cs生成失败!");
                return false;
            }

            //生成TableEnum文件
            result = GeneTableStructUtils.GeneTableEnum(rootDir);
            if (result == false)
            {
                DebugMgr.LogWarning("Fail. TableEnum枚举文件生成失败! ");
                return false;
            }
            return true;
        }

        private void ClearDir(string rootDir)
        {
            var features = ConfigMgr.tableConf.FindAllFeature();

            foreach (var fvo in features)
            {
                string f_path = Path.Combine(rootDir, $"games/{fvo.Name.ToLower()}/generate/table");
                CmdUtils.ClearDir(f_path);
            }
        }

        private bool GeneSeperateFiles(string rootDir)
        {
            bool result = true;

            var features = ConfigMgr.tableConf.FindAllFeature();

            var tables = ConfigMgr.tableConf.FindAll();

            foreach (var tvo in tables)
            {
                if (tvo.IsSeparate == false)
                {
                    continue;
                }

                foreach (var fvo in features)
                {
                    var dis_list = fvo.FindDiscreteVOsByName(tvo.Name);
                    if (dis_list.Count == 0)
                    {
                        continue;
                    }
                    string dir_path = Path.Combine(rootDir, $"games/{fvo.Name}/generate/table");

                    foreach (var dvo in dis_list)
                    {
                        result = GeneTableUtils.GeneCodeCSharpSeperate(tvo, dvo, dir_path);
                        if (result == false)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        private bool GeneEnumTableFile(string rootDir)
        {
            List<DITableVO> tables = ConfigMgr.tableConf.FindAll();

            var features = ConfigMgr.tableConf.FindAllFeature();

            foreach (var tvo in tables)
            {
                if (tvo.IsEnum == false)
                {
                    continue;
                }

                foreach (var fvo in features)
                {
                    var dis_list = fvo.FindDiscreteVOsByName(tvo.Name);
                    if (dis_list.Count == 0)
                    {
                        continue;
                    }
                    string dir_path = Path.Combine(rootDir, $"games/{fvo.Name}/generate/table");

                    foreach (var dvo in dis_list)
                    {
                        bool result = GeneTableUtils.GeneTableEnumFile(tvo, dvo, dir_path);
                        if (result == false)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public override bool Process(string[] args)
        {
            bool result = false;

            List<DITableVO> tablevos = ConfigMgr.tableConf.FindAll();

            if (tablevos.Count == 0)
            {
                DebugMgr.LogWarning("Fail. 找不到匹配的表格");
                return true;
            }

            var clientdir = Path.Combine(ConfigMgr.sysconf.GeneCodeClientPath, "generate/Config");
            SysDefine.isClient = true;
            result = GeneFiles(tablevos, clientdir);
            if (result == false)
            {
                DebugMgr.Log("client table code gene fail");
                return false;
            }

            ClearDir(ConfigMgr.sysconf.GeneCodeClientPath);

            result = GeneSeperateFiles(ConfigMgr.sysconf.GeneCodeClientPath);
            if (result == false)
            {
                DebugMgr.Log("client seperate table code gene fail");
                return false;
            }
            result = GeneEnumTableFile(ConfigMgr.sysconf.GeneCodeClientPath);
            if (result == false)
            {
                DebugMgr.Log("client enum table code gene fail");
                return false;
            }

            //-----------------------------Server-----------------------------

            var serverdir = Path.Combine(ConfigMgr.sysconf.GeneCodeServerPath, "generate/Config");
            SysDefine.isClient = false;
            result = GeneFiles(tablevos, serverdir);
            if (result == false)
            {
                DebugMgr.Log("server table code gene fail");
                return false;
            }

            ClearDir(ConfigMgr.sysconf.GeneCodeServerPath);

            result = GeneSeperateFiles(ConfigMgr.sysconf.GeneCodeServerPath);
            if (result == false)
            {
                DebugMgr.Log("server seperate table code gene fail");
                return false;
            }

            result = GeneEnumTableFile(ConfigMgr.sysconf.GeneCodeServerPath);
            if (result == false)
            {
                DebugMgr.Log("client enum table code gene fail");
                return false;
            }

            DebugMgr.Log("table code gene succ");

            DebugMgr.WriteLine();
            return true;
        }
    }
}
