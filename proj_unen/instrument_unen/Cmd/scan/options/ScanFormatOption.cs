﻿using UKon.Cmd;

namespace UNen.Cmd
{
    class ScanFormatOption : AbstractCmdOption
    {
        public override string type => "format";
        private readonly string[] m_name = new string[] { "--format", "-f" };
        public override string[] names => m_name;


        public ScanFormatOption(ScanCommand cmd)
            : base(cmd)
        {

        }

        public override bool Process(string[] args)
        {
            bool result = false;
            result = ConfigMgr.tableConf.VerifyStruct();
            if (result == true)
            {
                DebugMgr.Log("Succ. 表结构验证成功\n");
            }
            else
            {
                DebugMgr.LogWarning("Fail. 表结构验证失败\n");
            }
            return result;
        }
    }
}
