﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using UNen.TableGeneric;

namespace UNen
{
    public class TableGenericConf
    {
        public static readonly string FILE_NAME = "table_generic.config";
        private XmlDocument xmldoc = null;
        public XmlDocument XmlDoc => xmldoc;

        private List<TableStructureVO> m_structvos = new List<TableStructureVO>();
        public List<TableStructureVO> FindAllStruct() => m_structvos;

        private List<TableEnumVO> m_enumvos = new List<TableEnumVO>();
        public List<TableEnumVO> FindAllEnum() => m_enumvos;

        public int GetStructCnt(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_structvos.Count; i++)
            {
                if (m_structvos[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public int GetEnumCnt(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_enumvos.Count; i++)
            {
                if (m_enumvos[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public TableEnumVO FindEnum(string name)
        {
            return m_enumvos.Find(c => c.Name == name);
        }
        public TableStructureVO FindStruct(string name)
        {
            return m_structvos.Find(c => c.Name == name);
        }

        //public bool Init()
        //{
        //    bool result = Load();
        //    if (result == false)
        //    {
        //        return false;
        //    }
        //    result = Analyze();
        //    if (result == false)
        //    {
        //        return false;
        //    }
        //    return Verify();
        //}

        public bool Load()
        {
            string path = ConfigMgr.sysconf.AppPath;
            string filepath = Path.Combine(path, FILE_NAME);
            xmldoc = new XmlDocument();
            try
            {
                xmldoc.Load(filepath);
            }
            catch (XmlException e)
            {
                DebugMgr.LogWarning(e.Message);
                return false;
            }
            return true;
        }

        public bool Analyze()
        {
            m_structvos.Clear();
            m_enumvos.Clear();

            var xmlelement = xmldoc.SelectSingleNode("/config/structures");
            if (xmlelement == null)
            {
                return false;
            }

            XmlNodeList list = null;
            bool result = true;

            list = xmlelement.SelectNodes("structure");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new TableStructureVO();
                result = vo.Analyze(list[i]);
                if (result == false)
                {
                    DebugMgr.LogError("Fail! table 表结构 解析失败");
                    return false;
                }
                m_structvos.Add(vo);
            }

            xmlelement = xmldoc.SelectSingleNode("/config/enums");
            if (xmlelement == null)
            {
                return false;
            }
            list = xmlelement.SelectNodes("enum");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new TableEnumVO();
                result = vo.Analyze(list[i]);
                if (result == false)
                {
                    DebugMgr.LogError("Fail! Table枚举模块 解析失败");
                    return false;
                }
                m_enumvos.Add(vo);
            }

            return true;
        }

        public bool Verify()
        {
            for (int i = 0; i < m_structvos.Count; ++i)
            {
                if (m_structvos[i].Verify() == false)
                {
                    return false;
                }
            }

            for (int i = 0; i < m_enumvos.Count; ++i)
            {
                if (m_enumvos[i].Verify() == false)
                {
                    return false;
                }
            }

            DebugMgr.Log("Succ! table_generic.config 解析成功");
            return true;
        }
    }
}
