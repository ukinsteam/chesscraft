﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Xml;

namespace UNen.DT
{
    public class SINodeVO : NodeFieldVO, IStructDecorator
    {
        public override void Parse(XmlNode node)
        {
            ParseField(node);

            ParseChildren<SIFieldVO, SINodeVO>(node);
        }

        protected bool CheckStructFunc(IStructDecorator vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("CheckStructFunc 类型转换失败");
                return false;
            }
            return vo.CheckStruct();
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError(string.Format("Error! field结构name属性不能为空，field name={0}", FullName));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError(string.Format("Error。field结构name属性命名不规范; field name = {0}", FullName));
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! field结构type属性类型不能为空，field name={0}", FullName));
                return false;
            }

            if (m_type == DTDefine.TUPLE)
            {
                for (int i = 0; i < m_fields.Count; i++)
                {
                    if (m_fields[i].Type.IsCollection == true)
                    {
                        DebugMgr.LogError(string.Format("Error! tuple类型的结点不可以直接包含容器类型的子节点，field name={0}", FullName));
                        return false;
                    }
                    if (m_fields[i].Type.FirstElementType == DTDefine.TUPLE ||
                        m_fields[i].Type.SecondElementType == DTDefine.TUPLE)
                    {
                        DebugMgr.LogError(string.Format("Error! tuple类型不允许嵌套，field name={0}", FullName));
                        return false;
                    }
                }
            }
            //if (Type.IsCollection == true && InCollection == true)
            //{
            //    DebugMgr.LogError(string.Format("Error! 容器类型不允许嵌套，field name={0}", FullName));
            //    return false;
            //}

            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckStructFunc(m_fields[i] as IStructDecorator);
                if (result == false)
                {
                    return false;
                }
            }

            return true;
        }
    }
}
