﻿using ETT.Table.Analysis;

namespace UNen.DT
{
    public class SICustomStructFieldVO : LeafFieldVO, IStructDecorator
    {
        private string m_ownername;
        public SICustomStructFieldVO(string ownername)
        {
            m_ownername = ownername;
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError(string.Format("Error! field结构name属性不能为空，field name={0}", m_ownername));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError(string.Format("Error。field结构name属性命名不规范; field name = {0}", Name));
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! field结构type属性类型不能为空，field name={0}", Name));
                return false;
            }

            var result = FieldTypeInspector.CheckFieldType(this);
            if (result == false)
            {
                return false;
            }

            return true;
        }
    }
}
