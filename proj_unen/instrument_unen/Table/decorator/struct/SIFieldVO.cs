﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Xml;

namespace UNen.DT
{
    public class SIFieldVO : LeafFieldVO, IStructDecorator
    {
        public SITableVO ForeignTableVO { get; protected set; }

        public override void Parse(XmlNode node)
        {
            ParseField(node);
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError($"Error! field结构name属性不能为空，field name={FullName}");
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError($"Error。field结构name属性命名不规范; field name = {FullName}");
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError($"Error! field结构type属性类型不能为空，field name={FullName}");
                return false;
            }

            if (m_type == DTDefine.TUPLE)
            {
                DebugMgr.LogError($"Error! tuple类型必须包含子节点，field name={FullName}");
                return false;
            }
            //if (Type.IsCollection == true && InCollection == true)
            //{
            //    DebugMgr.LogError($"Error! 容器类型不允许嵌套，field name={0}", FullName));
            //    return false;
            //}

            var result = FieldTypeInspector.CheckFieldType(this);
            if (result == false)
            {
                return false;
            }

            if (IsPrimaryKey == true || IsUnionKey == true)
            {
                if ((Owner is TableVO) == false)
                {
                    DebugMgr.LogError($"Error！ 唯一主键不能出现在嵌套字段中。tablename={Root.Name}, fieldname = {FullName}");
                    return false;
                }

                bool isvaluetype = SysDefine.CheckType(m_type, SysDefine.ProtoTypeConst);
                if (isvaluetype == false)
                {
                    var enumvo = ConfigMgr.tableGenericConf.FindEnum(m_type);
                    if (enumvo == null)
                    {
                        DebugMgr.LogError($"Error！ 主键类型无效。主键类型必须是值类型或字符串类型。tablename={Root.Name}, fieldname = {FullName}");
                        return false;
                    }
                }
            }


            if (Foreigntable != string.Empty)
            {
                ForeignTableVO = ConfigMgr.tableConf.FindVO(Foreigntable);
                if (ForeignTableVO == null)
                {
                    DebugMgr.LogError($"Error！ 外键无效。 外界表不存在 tablename={Root.Name}, fieldname = {FullName}, type = {m_type}");
                    return false;
                }
                if (Type.FirstElementType != ForeignTableVO.PrimaryKey.Type.Value)
                {
                    DebugMgr.LogError($"Error！ 外键无效。类型与关联表主键类型不符 tablename={Root.Name}, fieldname = {FullName}, type = {m_type} ");
                    return false;
                }
            }

            int cnt = Owner.CountChildVO(Name);
            if (cnt > 1)
            {
                DebugMgr.LogError($"Error！ 字段重复。tablename={ Root.Name}, fieldname = {FullName}");
                return false;
            }
            return true;
        }
    }
}
