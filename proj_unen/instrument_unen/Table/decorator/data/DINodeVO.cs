﻿using System;
using System.Xml;

namespace UNen.DT
{
    public class DINodeVO : SINodeVO, IDataDecorator
    {
        public override void Parse(XmlNode node)
        {
            ParseField(node);

            ParseChildren<DIFieldVO, DINodeVO>(node);
        }

        protected bool CheckInternalDataFunc(IDataDecorator vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("DINodeVO.CheckInternalDataFunc 类型转换失败");
                return false;
            }
            return vo.CheckInternalData();
        }

        protected bool CheckExternalDataFunc(IDataDecorator vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("DINodeVO.CheckExternalDataFunc 类型转换失败");
                return false;
            }
            return vo.CheckExternalData();
        }

        public bool CheckInternalData()
        {
            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckInternalDataFunc(m_fields[i] as IDataDecorator);
                if (result == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool CheckExternalData()
        {
            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckExternalDataFunc(m_fields[i] as IDataDecorator);
                if (result == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
