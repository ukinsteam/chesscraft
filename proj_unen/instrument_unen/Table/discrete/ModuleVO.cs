﻿using System.Collections.Generic;
using System.Xml;

namespace UNen.Table
{
    public class ModuleVO : ITableVerify
    {
        public readonly FeatureVO Owner;
        public string Name { get; private set; }
        public string FullName
        {
            get
            {
                string name = Owner.Name;
                if (string.IsNullOrWhiteSpace(Name) == false)
                {
                    name = System.IO.Path.Combine(name, Name);
                }
                return name;
            }
        }

        private List<DiscreteVO> m_discretevolst = new List<DiscreteVO>();

        public ModuleVO(FeatureVO vo)
        {
            Owner = vo;
        }

        public int CountDiscreteVO(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_discretevolst.Count; ++i)
            {
                if (m_discretevolst[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }
        public List<DiscreteVO> FindDiscreteVOsByName(string name)
        {
            return m_discretevolst.FindAll(c => c.Name == name);
        }

        public List<DiscreteVO> FindAllDiscreteVO()
        {
            return m_discretevolst;
        }

        public bool Analyze(XmlNode node)
        {
            Name = AttrUtils.ExtractString(node, "name");

            bool result = true;
            XmlNodeList list = node.SelectNodes("table");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new DiscreteVO(this);
                result = vo.Analyze(list[i]);
                if (result == false)
                {
                    DebugMgr.LogError("Fail! module.table 表路径结构 解析失败");
                    return false;
                }
                m_discretevolst.Add(vo);
            }

            return true;
        }

        public bool VerifyStruct()
        {
            int cnt = Owner.CountModule(Name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Fail! module名称重复 module.name = {0}", Name));
                return false;
            }

            foreach (var vo in m_discretevolst)
            {
                if (vo.VerifyStruct() == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool VerifySelfData()
        {
            foreach (var vo in m_discretevolst)
            {
                if (vo.VerifySelfData() == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool VerifyLinkData()
        {
            foreach (var vo in m_discretevolst)
            {
                if (vo.VerifyLinkData() == false)
                {
                    return false;
                }
            }
            return true;
        }

    }
}
