﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Xml;

namespace UNen.Table
{
    public class DiscreteVO : ITableVerify
    {
        public TableVO TableVO { get; private set; }
        public RangeVO<ulong> KeyRangeVO { get; private set; }
        public string Name { get; private set; }
        public string Path { get; private set; }
        public string KeyRange { get; private set; }
        public ModuleVO Owner { get; private set; }

        private LeafFieldVO keyfield;

        private AbstractDataTable dataTable = null;
        public AbstractDataTable GetDataTable()
        {
            return dataTable;
        }

        public string FullName
        {
            get
            {
                string name = Owner.FullName;
                if (string.IsNullOrWhiteSpace(Name) == false)
                {
                    name = System.IO.Path.Combine(name, Name);
                }
                return name;
            }
        }

        public string FullPath
        {
            get
            {
                string name = Owner.FullName;
                if (string.IsNullOrWhiteSpace(Name) == false)
                {
                    name = System.IO.Path.Combine(name, Path);
                }
                return name;
            }
        }

        public DiscreteVO(ModuleVO owner)
        {
            Owner = owner;
        }

        public bool Analyze(XmlNode node)
        {
            Name = AttrUtils.ExtractString(node, "name");
            Path = AttrUtils.ExtractString(node, "path");
            KeyRange = AttrUtils.ExtractString(node, "keyrange");

            if (string.IsNullOrWhiteSpace(Name) == true || string.IsNullOrWhiteSpace(Path) == true)
            {
                return false;
            }

            return true;
        }

        public bool VerifyStruct()
        {
            TableVO = ConfigMgr.tableConf.FindVO(Name);
            if (TableVO == null)
            {
                //DebugMgr.LogError($"Error！ 表结构不存在。 module.table.name={Name}");
                throw new System.Exception($"Error！ 表结构不存在。 module.table.name={Name}");
                //DebugMgr.WriteLine();
                //return false;
            }

            if (KeyRange != string.Empty)
            {
                string[] ss = KeyRange.Split(',');
                if (ss.Length != 2)
                {
                    DebugMgr.LogError($"Error！ keyrange字段的格式必须是“min,max”。 module.table.name={Name}");
                    DebugMgr.WriteLine();
                    return false;
                }

                if (TableVO.PrimaryKey != null)
                {
                    keyfield = TableVO.PrimaryKey;
                }
                else
                {
                    keyfield = TableVO.UnionKey.First;
                }

                if (SysDefine.CheckType(keyfield.Type.Value.ToLower(), SysDefine.ValueTypeConst))
                {
                    ulong min = ulong.Parse(ss[0]);
                    ulong max = ulong.Parse(ss[1]);
                    if (min > max)
                    {
                        DebugMgr.LogError($"Error！ keyrange字段的格式必须是“min,max”，且min不能大于max。 module.table.name={Name}");
                        DebugMgr.WriteLine();
                        return false;
                    }
                    KeyRangeVO = new RangeVO<ulong>(min, max);
                }
            }

            return true;
        }

        public bool VerifySelfData()
        {
            var filePath = System.IO.Path.Combine(ConfigMgr.sysconf.SrcProjPath, FullPath);

            int cnt = Owner.CountDiscreteVO(Name);
            if (cnt > 1)
            {
                DebugMgr.LogError($"Error! module.table重复 module.table.name = {Name}");
                DebugMgr.WriteLine();
                return false;
            }

            if (System.IO.File.Exists(filePath) == false)
            {
                DebugMgr.LogError("Error！  文件不存在。");
                DebugMgr.LogError($"\t feature.name={Owner.Owner.Name}; module.name = {Owner.Name}; table.name = {Name}; path={Path}");
                DebugMgr.LogError($"\t filepath={filePath}");
                DebugMgr.WriteLine();
                return false;
            }


            //验证分表数据有效性
            bool result = CSVDataTableAgent.Parse(filePath, TableVO, out dataTable);
            if (result == false)
            {
                return false;
            }

            if (KeyRangeVO != null)
            {
                for (int i = 0; i < dataTable.Rows.Count; ++i)
                {
                    var row = dataTable.Rows[i];
                    ulong value = ulong.Parse(row[keyfield.Name][0].Value);

                    if (value >= KeyRangeVO.Min && value < KeyRangeVO.Max)
                    {
                        continue;
                    }
                    DebugMgr.LogError("Error!  module.table有数据不在keyrange定义的范围内。");
                    DebugMgr.LogError($"\tmodule.table name = {Name}, path={Path};");
                    DebugMgr.LogError($"\trange.min = {KeyRangeVO.Min}, range.max={KeyRangeVO.Max};");
                    DebugMgr.LogError($"\tcurrvalue = {value}. ");
                    DebugMgr.WriteLine();
                    return false;
                }
            }

            return true;
        }

        public bool VerifyLinkData()
        {
            //没有外部数据需要验证
            throw new System.NotImplementedException();
        }
    }
}
