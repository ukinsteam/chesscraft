﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using UNen.DT;

namespace UNen.TableGeneric
{
    public class TableStructureVO
    {
        private string m_name = string.Empty;
        public string Name => m_name;


        private bool m_extern = false;
        public bool Extern => m_extern;

        private List<SICustomStructFieldVO> m_fields = new List<SICustomStructFieldVO>();
        public List<SICustomStructFieldVO> FindAll() => m_fields;

        public bool Analyze(XmlNode node)
        {
            bool result = AttrUtils.GetAttr(node.Attributes["name"], out m_name, true);
            if (result == false || string.IsNullOrWhiteSpace(m_name) == true)
            {
                DebugMgr.LogError(string.Format("Error! 自定义表结构name字段不能为空，protostructname={0}", m_name));
                return false;
            }



            result = AttrUtils.CheckNamingSpecification(m_name);
            if (result == false)
            {
                DebugMgr.LogError(string.Format("Error。自定义表结构命名不规范; protostructname = {0}", m_name));
                return false;
            }

            AttrUtils.GetAttr(node.Attributes["extern"], out m_extern);

            if (m_extern == true)
            {
                return true;
            }

            XmlNodeList fields = node.SelectNodes("field");
            for (int i = 0; i < fields.Count; ++i)
            {
                SICustomStructFieldVO vo = new SICustomStructFieldVO(m_name);
                vo.Parse(fields[i]);
                m_fields.Add(vo);
            }

            return true;
        }

        public bool Verify()
        {
            int cnt = ConfigMgr.tableGenericConf.GetStructCnt(m_name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error。自定义表结构重名; tablestructname = {0}", m_name));
                return false;
            }

            if (m_extern == true)
            {
                return true;
            }

            foreach (var field in m_fields)
            {
                bool result = field.CheckStruct();
                if (result == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
