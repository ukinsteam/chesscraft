﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNen
{
    public class VOPool<T> where T : new()
    {
        private static Stack<T> cmdparampool = new Stack<T>();
        public static T Pop()
        {
            if (cmdparampool.Count > 0)
            {
                return cmdparampool.Pop();
            }
            return new T();
        }
        public static void Push(T vo)
        {
            cmdparampool.Push(vo);
        }
    }
}
