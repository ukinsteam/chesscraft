﻿using ETT.Table.Analysis;

namespace UNen
{
    public interface IStructDecorator : IXmlParse
    {
        bool CheckStruct();
    }
}
