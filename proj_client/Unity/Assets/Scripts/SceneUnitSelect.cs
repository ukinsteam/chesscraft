﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using ETT.Model;

namespace ETT.Client
{
    public class SceneUnitSelect : MonoBehaviour, ISceneUnitSelect
    {
        public SceneUnit Unit { get; set; }

        private Outline m_outline;

        private void Awake()
        {
            m_outline = GetComponent<Outline>();
            if (m_outline != null)
            {
                m_outline.enabled = false;
            }
        }

        public void Select(bool state)
        {
            Context.Event?.Run(Unit.SelectEvt, Unit, state);
        }

        public void ConfirmSelect(bool state)
        {
            if (m_outline != null)
            {
                m_outline.enabled = state;
            }
        }

    }
}
