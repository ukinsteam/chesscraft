﻿using ETT.Client;
using ETT.Client.Res;
using ETT.Model;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UnityEditor
public class AssetDataBaseLoader : MonoBehaviour, IResMode
{
    private readonly Dictionary<string, LoadVO> m_dict = new Dictionary<string, LoadVO>();

    public AssetVO GetAsset(string path)
    {
        if (m_dict.ContainsKey(path) == false)
        {
            return null;
        }
        var waiter = m_dict[path];

        if (waiter.State != ELoadAssetState.Complete)
        {
            return null;
        }

        return waiter.Ent;
    }

    public AssetVO Load(string path)
    {
        LoadVO waiter = null;
        if (m_dict.ContainsKey(path) == false)
        {
            waiter = new LoadVO
            {
                Ent = new AssetVO(path),
                State = ELoadAssetState.None,
            };
            m_dict.Add(path, waiter);
        }
        waiter = m_dict[path];
        switch (waiter.State)
        {
            case ELoadAssetState.Complete:
                return waiter.Ent;
            case ELoadAssetState.Loading:
                return null;
            case ELoadAssetState.None:
                waiter.Ent.Asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                waiter.State = ELoadAssetState.Complete;
                return waiter.Ent;
            default: break;
        }
        return null;
    }

    public async ETTask<AssetVO> LoadAync(string path)
    {
        if (m_dict.ContainsKey(path) == false)
        {
            LoadVO waiter = new LoadVO
            {
                Ent = new AssetVO(path),
                State = ELoadAssetState.None,
            };
            m_dict.Add(path, waiter);

        }

        var loadwaiter = m_dict[path];
        switch (loadwaiter.State)
        {
            case ELoadAssetState.None:
            case ELoadAssetState.Loading:
                loadwaiter.Ent.Asset = AssetDatabase.LoadAssetAtPath<UnityEngine.Object>(path);
                loadwaiter.State = ELoadAssetState.Complete;
                return loadwaiter.Ent;
            case ELoadAssetState.Complete:
                return loadwaiter.Ent;
        }

        await ETTask.CompletedTask;

        return null;
    }
}
#endif