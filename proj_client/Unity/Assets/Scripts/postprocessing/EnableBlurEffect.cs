﻿using ETT.Client;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.ImageEffects;

public class EnableBlurEffect : MonoBehaviour, IEnableEffect
{
    public void Enable(bool enable)
    {
        var effect = GetComponent<BlurOptimized>();
        if (effect != null)
        {
            effect.enabled = enable;
        }
    }
}
