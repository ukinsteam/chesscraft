﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System.Linq;
using System.Text.RegularExpressions;
using ETT.Client;
using System.Diagnostics;

public class RunOption
{

    public static void RunUnen()
    {
        string fileName = "unen.exe";
        string filePath = Application.dataPath + @"/../../tool_unen/";
        if (!File.Exists(filePath + fileName))
        {
            EditorUtility.DisplayDialog("Error!", "未找到打包bat，请尝试更新svn.", "OK");
            return;
        }
        Process process = new Process();
        process.StartInfo.WorkingDirectory = filePath;
        process.StartInfo.FileName = fileName;
        process.Start();
        process.WaitForExit();
    }
}
