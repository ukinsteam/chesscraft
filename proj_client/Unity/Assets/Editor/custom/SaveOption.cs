﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SaveOption
{
    [MenuItem("CONTEXT/Transform/SavePrefab")]
    static public void SavePrefab()
    {
        GameObject source = PrefabUtility.GetCorrespondingObjectFromSource(Selection.activeGameObject);
        if (source == null) return;
        string prefabPath = AssetDatabase.GetAssetPath(source);
        if (prefabPath.ToLower().EndsWith(".prefab") == false) return;
        PrefabUtility.SaveAsPrefabAsset(Selection.activeGameObject, prefabPath);
    }
}
