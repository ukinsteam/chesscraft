﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;
using System.Linq;
using System.Text.RegularExpressions;
using ETT.Client;

public class ScanOption
{

    //private static readonly string outPath = Application.streamingAssetsPath + "/AssetBundles";
    private static readonly string assetFullPath = Application.dataPath + "/Resources/";

    public static void ScanABSize()
    {
        if (Directory.Exists(assetFullPath))
        {
            DirectoryInfo di = new DirectoryInfo(assetFullPath);
            ScanDir(di);
        }
    }

    private static void ScanDir(DirectoryInfo di)
    {
        long totalsize = 0;
        foreach (FileInfo f in di.GetFiles())
        {
            if (f.Extension == ".meta")
            {
                continue;
            }
            totalsize += f.Length;
        }
        if (totalsize > 0)
        {
            Debug.Log(string.Format("The Size Of Prefab is {0}KB, Path = {1}", Mathf.Ceil(totalsize * 1.0f / 1024), di.FullName));
            if (totalsize > 1024 * 1024)
            {
                Debug.LogWarning("The Size Of Prefab Is Too Big");
            }
            else if (totalsize < 1024)
            {
                Debug.LogWarning("The Size Of Prefab Is Too Small");
            }
        }

        foreach (DirectoryInfo dir in di.GetDirectories())
        {
            if (dir.Name.StartsWith("#_") == true)
            {
                continue;
            }
            ScanDir(dir);
        }
    }

    private static void SetBuildSettingScene(List<string> allscenefiles)
    {

        List<string> scenepath = new List<string>();
        for (int i = 0; i < allscenefiles.Count; ++i)
        {
            string path = allscenefiles[i].Replace("\\", "/").Replace(Application.dataPath, "");
            //string path = allscenefiles[i].Replace("\\", "/");
            //path = path.Substring(1).Replace(".unity", "");
            path = "Assets" + path;
            Debug.Log(path);
            scenepath.Add(path);
        }
        EditorBuildSettingsScene[] scenes = new EditorBuildSettingsScene[scenepath.Count];
        for (int i = 0; i < scenepath.Count; i++)
        {
            scenes[i] = new EditorBuildSettingsScene(scenepath[i], true);
        }
        EditorBuildSettings.scenes = scenes;
        Debug.Log("场景设置成功");
    }

    public static void ScanProjScene()
    {
        List<string> allscenefiles = new List<string>();

        FileUtils.GetAllFiles(Path.Combine(Application.dataPath, "Resources"), ref allscenefiles, "*.unity", "#_");

        SetBuildSettingScene(allscenefiles);
    }

    public static void ScanAllScene()
    {
        List<string> allscenefiles = new List<string>();

        FileUtils.GetAllFiles(Application.dataPath, ref allscenefiles, "*.unity", "#_");

        SetBuildSettingScene(allscenefiles);
    }

    public static void FindReferences()
    {
        EditorSettings.serializationMode = SerializationMode.ForceText;
        string path = AssetDatabase.GetAssetPath(Selection.activeObject);
        if (!string.IsNullOrEmpty(path))
        {
            string guid = AssetDatabase.AssetPathToGUID(path);
            List<string> withoutExtensions = new List<string>() { ".prefab", ".unity", ".mat", ".asset" };
            string[] files = Directory.GetFiles(Application.dataPath, "*.*", SearchOption.AllDirectories)
                .Where(s => withoutExtensions.Contains(Path.GetExtension(s).ToLower())).ToArray();
            int startIndex = 0;

            EditorApplication.update = delegate ()
            {
                string file = files[startIndex];

                bool isCancel = EditorUtility.DisplayCancelableProgressBar("匹配资源中", file, (float)startIndex / (float)files.Length);

                if (Regex.IsMatch(File.ReadAllText(file), guid))
                {
                    Debug.Log(file, AssetDatabase.LoadAssetAtPath<Object>(GetRelativeAssetsPath(file)));
                }

                startIndex++;
                if (isCancel || startIndex >= files.Length)
                {
                    EditorUtility.ClearProgressBar();
                    EditorApplication.update = null;
                    startIndex = 0;
                    Debug.Log("匹配结束");
                }

            };
        }
    }

    //[MenuItem("Assets/Find References", true)]
    //static private bool VFind()
    //{
    //    string path = AssetDatabase.GetAssetPath(Selection.activeObject);
    //    return (!string.IsNullOrEmpty(path));
    //}

    static private string GetRelativeAssetsPath(string path)
    {
        return "Assets" + Path.GetFullPath(path).Replace(Path.GetFullPath(Application.dataPath), "").Replace('\\', '/');
    }

    public static void FindMissingScripts()
    {
        List<string> listString = new List<string>();

        CollectFiles(Application.dataPath, listString);

        for (int i = 0; i < listString.Count; i++)
        {
            string Path = listString[i];

            float progressBar = (float)i / listString.Count;

            EditorUtility.DisplayProgressBar("Check Missing Scripts", "The progress of ： " + ((int)(progressBar * 100)).ToString() + "%", progressBar);

            if (!Path.EndsWith(".prefab"))//只处理prefab文件
            {
                continue;
            }

            Path = ChangeFilePath(Path);

            AssetImporter tmpAssetImport = AssetImporter.GetAtPath(Path);

            GameObject prefab = AssetDatabase.LoadAssetAtPath<GameObject>(tmpAssetImport.assetPath);

            if (prefab == null)
            {
                Debug.LogError("空的预设 ： " + tmpAssetImport.assetPath);

                continue;
            }

            Transform[] transforms = prefab.GetComponentsInChildren<Transform>();
            //获取所有的子节点;

            for (int j = 0; j < transforms.Length; j++)
            {
                GameObject obj = transforms[j].gameObject;

                var components = obj.GetComponents<Component>();
                //获取对象所有的Component组件
                //所有继承MonoBehaviour的脚本都继承Component

                for (int k = 0; k < components.Length; k++)
                {
                    if (components[k] == null)
                    {
                        Debug.LogError("这个预制中有空的脚本 ：" + tmpAssetImport.assetPath + " 挂在对象 : " + obj.name + " 上");
                    }
                }
            }
        }
        EditorUtility.ClearProgressBar();
    }

    //改变路径  
    //这种格式的路径 "C:/Users/XX/Desktop/aaa/New Unity Project/Assets\a.prefab" 改变成 "Assets/a.prefab"
    static string ChangeFilePath(string path)
    {
        path = path.Replace("\\", "/");
        path = path.Replace(Application.dataPath + "/", "");
        path = "Assets/" + path;

        return path;
    }

    //迭代获取文件路径;
    static void CollectFiles(string directory, List<string> outfiles)
    {
        string[] files = Directory.GetFiles(directory);

        outfiles.AddRange(files);

        string[] childDirectories = Directory.GetDirectories(directory);

        if (childDirectories != null && childDirectories.Length > 0)
        {
            for (int i = 0; i < childDirectories.Length; i++)
            {
                string dir = childDirectories[i];
                if (string.IsNullOrEmpty(dir)) continue;
                CollectFiles(dir, outfiles);
            }
        }
    }
}
