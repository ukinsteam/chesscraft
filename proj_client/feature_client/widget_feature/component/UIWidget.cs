﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Widgets
{
    public class UIWidget : Model.Component, IAwake<GameObject>
    {
        public GameObject gameObject { get; protected set; }

        public virtual string Prefix => "go";

        public virtual void Awake(GameObject t)
        {
            IsFromPool = true;
            gameObject = t;
            if (t == null)
            {
                throw new Exception("Error! UIWidget.gameObject is null");
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            GameObject.Destroy(gameObject);
        }

        protected GameObject Find(string name)
        {
            var child = gameObject.transform.Find(name);
            return child.gameObject;
        }

        protected GameObject Find(GameObject parent, string name)
        {
            var child = parent.transform.Find(name);
            return child.gameObject;
        }

        public virtual void SetActive(bool active)
        {
            if (gameObject == null || gameObject.activeSelf == active) return;

            gameObject.SetActive(active);
        }
    }
}
