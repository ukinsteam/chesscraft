﻿using ETT.Client;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Widgets
{
    public class UIImage : UIWidget
    {
        protected Image image;

        public override string Prefix => "img";

        public override void Awake(GameObject t)
        {
            base.Awake(t);
            image = gameObject.GetComponent<Image>();
            if (image == null)
            {
                throw new Exception($"UIImage找不到Image组件");
            }

            raycastTarget = false;
        }

        public override void Dispose()
        {
            base.Dispose();
            image = null;
        }

        public bool raycastTarget
        {
            get { return image.raycastTarget; }
            set { image.raycastTarget = value; }
        }

        public Color color
        {
            get { return image.color; }
            set { image.color = value; }
        }

        private string m_sprite_name;
        public string sprite
        {
            get { return m_sprite_name; }
            set
            {
                m_sprite_name = value;
                LoadSprite().Coroutine();
            }
        }

        public float fillAmount
        {
            get { return image.fillAmount; }
            set
            {
                var v = Mathf.Clamp01(value);
                image.fillAmount = value;
            }
        }

        private async ETVoid LoadSprite()
        {
            var res_mgr = Context.Game.GetComponent<ResourcesComponent>();
            Texture2D tex2d = await res_mgr.GetTexture(m_sprite_name);
            try
            {
                Sprite s = Sprite.Create(tex2d, new Rect(0, 0, tex2d.width, tex2d.height), new Vector2(0, 0));
                image.sprite = s;
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
