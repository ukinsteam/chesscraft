﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ETT.Widgets
{
    public class UIEventListener : EventTrigger
    {
        public Action<GameObject> onClick;
        public Action<GameObject> onDown;
        public Action<GameObject> onEnter;
        public Action<GameObject> onExit;
        public Action<GameObject> onUp;
        public Action<GameObject> onSelect;
        public Action<GameObject> onUpdateSelect;

        public static UIEventListener Get(GameObject go)
        {
            UIEventListener listener = go.GetComponent<UIEventListener>();
            if (listener == null) listener = go.AddComponent<UIEventListener>();
            return listener;
        }

        public static UIEventListener Get(UIWidget widget)
        {
            return Get(widget.gameObject);
        }

        public override void OnPointerClick(PointerEventData eventData)
        {
            onClick?.Invoke(gameObject);
        }
        public override void OnPointerDown(PointerEventData eventData)
        {
            onDown?.Invoke(gameObject);
        }
        public override void OnPointerEnter(PointerEventData eventData)
        {
            onEnter?.Invoke(gameObject);
        }
        public override void OnPointerExit(PointerEventData eventData)
        {
            onExit?.Invoke(gameObject);
        }
        public override void OnPointerUp(PointerEventData eventData)
        {
            onUp?.Invoke(gameObject);
        }
        public override void OnSelect(BaseEventData eventData)
        {
            onSelect?.Invoke(gameObject);
        }
        public override void OnUpdateSelected(BaseEventData eventData)
        {
            onUpdateSelect?.Invoke(gameObject);
        }
    }
}
