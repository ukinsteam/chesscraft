﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ETT.Widgets
{
    public class LongPressListener : EventTrigger
    {
        public event Action<bool> OnLongPress;

        private bool m_isdown = false;
        private float m_delaytime = 1.5f;
        private bool m_invokeOnce = true;
        private float m_intervaltime = 0.2f;
        private float m_elapsedtime = 0;
        private uint m_cnt = 0;

        public static LongPressListener Get(GameObject go)
        {
            LongPressListener listener = go.GetComponent<LongPressListener>();
            if (listener == null) listener = go.AddComponent<LongPressListener>();
            return listener;
        }

        public static LongPressListener Get(UIWidget widget)
        {
            return Get(widget.gameObject);
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            m_isdown = true;
            m_elapsedtime = 0;
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
            m_isdown = false;
            CallEvent(false);
            m_cnt = 0;
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            m_isdown = false;
            CallEvent(false);
            m_cnt = 0;
        }

        public void OnUpdate()
        {
            if (m_isdown == true)
            {
                m_elapsedtime += Time.deltaTime;
                if (m_cnt == 0)
                {
                    if (m_elapsedtime >= m_delaytime)
                    {
                        CallEvent(true);
                    }
                }
                else
                {
                    if (m_invokeOnce == false)
                    {
                        if (m_elapsedtime >= m_intervaltime)
                        {
                            CallEvent(true);
                        }
                    }
                }
            }
        }
        private void CallEvent(bool isPress)
        {
            OnLongPress?.Invoke(isPress);
            m_elapsedtime = 0;
        }

        public void SetLongPress(float delaytime, bool invokeOnce = true, float intervaltime = 0)
        {
            m_delaytime = delaytime;
            m_invokeOnce = invokeOnce;
            m_intervaltime = intervaltime;
        }
    }
}
