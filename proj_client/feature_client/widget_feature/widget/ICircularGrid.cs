﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public interface ICircularGrid
    {
        void SetUpdateFunc(Action<GameObject, int> func);

        void SetSelectFunc(Action<GameObject, int> func);

        void SetAmount(int count);
    }
}
