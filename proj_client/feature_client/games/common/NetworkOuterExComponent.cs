﻿using ETT.Model.network;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public class NetworkOuterExComponent : NetworkOuterComponent
    {
        public IPEndPoint GateAddr { get; set; }

        public long AccountId { get; set; }

        public long ActorId { get; set; }

    }
}
