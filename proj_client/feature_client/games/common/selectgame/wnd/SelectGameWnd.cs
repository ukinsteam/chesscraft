﻿using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Widgets;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace ETT.Client.Common
{
    [Window((int)ECommonWndTable.SelectGame)]
    public class SelectGameWnd : AbstractWnd
    {
#pragma warning disable 0649,0169
        [UIField("root/game_list/grid_scroll/grid_container")]
        private UICircularGrid grid_game;
#pragma warning restore 0649,0169

        private List<GameKindTableConf> m_list = new List<GameKindTableConf>();

        protected override void OnShow()
        {
            m_list.Clear();
            m_list.AddRange(Context.Table.GetComponent<GameKindTable>().FindAllVO());

            grid_game.SetSelectFunc(OnSelectHandler);
            grid_game.SetUpdateFunc(OnUpdateHandler);
            grid_game.SetAmount(m_list.Count);
        }

        protected override void OnHide()
        {
        }

        #region Event Handler

        private void OnUpdateHandler(GameObject go, int index)
        {
            if (index >= m_list.Count || index < 0)
            {
                return;
            }

            GameObject text = UIUtils.FindChild(go, "Text");
            UIUtils.SetLabel(text, m_list[index].Desc);
        }

        private void OnSelectHandler(GameObject go, int index)
        {
            if (index >= m_list.Count || index < 0)
            {
                return;
            }
            var kind = (ECommonGameKindTable)m_list[index].Id;
            Context.Game.GetComponent<SelectGameComponent>().Select(kind);
        }

        #endregion
    }
}
