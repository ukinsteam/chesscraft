﻿using ETT.Client.Wnd;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public abstract class LogicComponent : Component
    {
        protected SceneComponent SceneMgr => Context.Game.GetComponent<SceneComponent>();
        protected WndComponent WndMgr => Context.Game.GetComponent<WndComponent>();
        protected ResourcesComponent ResMgr => Context.Game.GetComponent<ResourcesComponent>();
        protected UIEventComponent UIEventMgr => Context.Game.GetComponent<UIEventComponent>();
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        protected virtual void ShowErrorCode(int errcode)
        {
            UIUtils.ShowErrorCode(errcode);
        }

        protected string Localize(string keycode)
        {
            return UIUtils.Localize(keycode);
        }
    }
}
