namespace ETT.Client.Common
{
	/// <summary>
	/// Common协议枚举 Range[10000,20000)
	/// <summary>
	public enum ECommonProto
	{
		C2L_Req_Login = 10000,
		L2C_Res_Login = 10001,
		C2G_Req_Login = 10002,
		G2C_Res_Login = 10003,
		C2G_Req_EnterMap = 10100,
		G2C_Res_EnterMap = 10101,
	}

}
