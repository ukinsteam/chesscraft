using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Client.Common
{

	/// <summary>
	/// 登陆协议 【Client, Login】
	/// </summary>
	[Message((uint)ECommonProto.C2L_Req_Login)]
	public class C2L_Req_Login : AbstractRequest
	{
		public string name {get; set;}
		public string pwd {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(name);
			writer.Write(pwd);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			name = reader.ReadString();
			pwd = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆协议 【Login, Client】
	/// </summary>
	[Message((uint)ECommonProto.L2C_Res_Login)]
	public class L2C_Res_Login : AbstractResponse
	{
		public long key {get; set;}
		public string addr {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(addr);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			addr = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆网关 【Client, Gate】
	/// </summary>
	[Message((uint)ECommonProto.C2G_Req_Login)]
	public class C2G_Req_Login : AbstractRequest
	{
		public long key {get; set;}
		public string AccountName {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(AccountName);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			AccountName = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆网关 【Gate, Client】
	/// </summary>
	[Message((uint)ECommonProto.G2C_Res_Login)]
	public class G2C_Res_Login : AbstractResponse
	{
		public long playerid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(playerid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			playerid = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 请求进入地图 【Client, Gate】
	/// </summary>
	[Message((uint)ECommonProto.C2G_Req_EnterMap)]
	public class C2G_Req_EnterMap : AbstractRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 进入地图响应 【Gate, Client】
	/// </summary>
	[Message((uint)ECommonProto.G2C_Res_EnterMap)]
	public class G2C_Res_EnterMap : AbstractResponse
	{
		public long UnitId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(UnitId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			UnitId = reader.ReadInt64();
		}
	}
}
