﻿using ETT.Client.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.TiaoQi
{
    public class ChessRoot : AbstractChessEntity, IAwake, IChessRoot
    {
        public override ECommonGameKindTable Kind => ECommonGameKindTable.TiaoQi;

        public void Awake()
        {
            Root = this;
        }

        public override void Enter()
        {
            Log.Debug("Enter TiaoQi");
        }

        public override void Exit()
        {
            Log.Debug("Exit TiaoQi");
        }

        public override void GameOver()
        {

        }
    }
}
