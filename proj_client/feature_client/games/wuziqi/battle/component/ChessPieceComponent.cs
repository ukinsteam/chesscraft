﻿using ETT.AI.Common;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;

namespace ETT.Client.WuZiQi
{
    public class ChessPieceComponent : Component,
         IAwake<ChessmanTableConf>
    {

        public ChessPieceEntity ChessET { get; private set; }

        public ChessmanTableConf Conf { get; private set; }

        public void Awake(ChessmanTableConf t)
        {
            Conf = t;

            ChessET = GetParent<ChessPieceEntity>();
        }

        public void Move(UVector2Int pos)
        {
            ChessET.SetPos(pos);
        }

    }
}
