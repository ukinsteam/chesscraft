﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.WuZiQi
{
    public class PathPointManager : Model.Component, IAwake
    {
        private Dictionary<UVector2Int, PathPointSceneUnit> m_units = new Dictionary<UVector2Int, PathPointSceneUnit>();

        private ModelTableConf m_path_conf;
        //private int m_showcnt = 0;

        public void Awake()
        {
            if (m_path_conf == null)
            {
                m_path_conf = Context.Table.GetComponent<ModelTable>()[(uint)EBattleModelTable.PathPoint];
            }
            InitPathPoint().Coroutine();
        }

        private async ETVoid InitPathPoint()
        {
            var table = Context.Table.GetComponent<FootholdTable>();
            var lst = table.FindAllVO();

            foreach (var conf in lst)
            {
                PathPointSceneUnit unit = Root.Factory.Create<PathPointSceneUnit, ModelTableConf>(m_path_conf);
                unit.SelectEvt = Define.Select;
                await unit.LoadModel();
                unit.SetInfo(conf.Pos, TranslateVec(conf.Pos));
                m_units.Add(conf.Pos, unit);
            }

            Context.Game.GetComponent<WndComponent>().Hide((int)ECommonWndTable.SceneLoading);
        }


        public void ShowPathPoint()
        {
            foreach (var pair in m_units)
            {
                pair.Value.SetActive(true);
            }
        }

        public void HidePathPoint()
        {
            foreach (var pair in m_units)
            {
                pair.Value.SetActive(false);
            }
        }

        public void RemovePathPoint(UVector2Int pos)
        {
            if (m_units.ContainsKey(pos) == false)
            {
                return;
            }
            var unit = m_units[pos];
            m_units.Remove(pos);

            unit.Dispose();
        }

        private Vector3 TranslateVec(UVector2Int pos)
        {
            var footholdtable = Context.Table.GetComponent<FootholdTable>();
            var conf = footholdtable[pos];
            return new Vector3(conf.Axis.X, 0, conf.Axis.Y);
        }

    }
}
