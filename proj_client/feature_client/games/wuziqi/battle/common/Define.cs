﻿using ETT.AI.Common;
using ETT.AI.WuZiQi;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.WuZiQi
{
    public static class Define
    {
        public const string Place = "wuziqi_place";
        public const string Select = "wuziqi_select";

        public const string Exe_Place = "wuziqi_exe_place";
        public const string Exe_GameOver = "wuziqi_exe_gameover";

        public static RoomVO VO;
        public static long StartTime;
        public static bool isOnTurn = false;
        public static EChessPieceFaction MineFaction = EChessPieceFaction.BLACK;

        public static bool isGameOver = true;
        public static EChessPieceFaction WinFaction = EChessPieceFaction.INVALID;

        public static ChessBoardEntity BoardET;

        public static uint GetId(UVector2Int pos)
        {
            return (uint)(pos.x + 1000) << 16 | (uint)(pos.y + 1000);
        }
        public static UVector2Int GetPos(uint id)
        {
            UVector2Int pos = new UVector2Int
            {
                y = (int)(id & 0xFFFF) - 1000,
                x = (int)((id >> 16) & 0xFFFF) - 1000
            };
            return pos;
        }

    }
}
