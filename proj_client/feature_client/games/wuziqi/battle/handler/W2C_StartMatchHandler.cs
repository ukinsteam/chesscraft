﻿using ETT.AI.WuZiQi;
using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.WuZiQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_StartMatchHandler : AbstractMsgHandler<W2C_Ntf_StartMatch>
    {
        protected SceneComponent SceneMgr => Context.Game.GetComponent<SceneComponent>();
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        protected override async ETTask Run(Session session, W2C_Ntf_StartMatch msg)
        {
            Define.VO = msg.RoomVO;
            Define.StartTime = msg.StartTime;
            Define.isGameOver = false;

            if (msg.RoomVO.Players[0].PlayerId == NetworkMgr.AccountId)
            {
                Define.MineFaction = EChessPieceFaction.RED;
            }
            else if (msg.RoomVO.Players[1].PlayerId == NetworkMgr.AccountId)
            {
                Define.MineFaction = EChessPieceFaction.BLACK;
            }

            //SceneMgr.Switch((uint)EGenericSceneTable.scene_battle);

            Context.Game.GetComponent<SelectGameComponent>().SwitchModule<BattleComponent>();

            await ETTask.CompletedTask;
        }
    }
}
