﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Model;
using ETT.Model.network;

namespace ETT.Client.WuZiQi.Battle
{
    [Event(Define.Place)]
    public class PlaceEventHandler : IEvent<UVector2Int>
    {
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        public void Run(UVector2Int pos)
        {
#if ONLINE_MODE
            RunAsync(pos).Coroutine();
#else
            RunSync(pos);
#endif
        }


#if ONLINE_MODE
        private async ETVoid RunAsync(UVector2Int pos)
#else
        private void RunSync(UVector2Int pos)
#endif
        {
            var foothold = Context.Table.GetComponent<FootholdTable>()[pos];

            if (foothold == null)
            {
                Log.Error($"Foothold is not exist point = {pos}");
                return;
            }

            var board = Define.BoardET.GetComponent<ChessBoardEntity>();
            if (Define.BoardET[pos] != null)
            {
                Log.Error($"Can not place here; point = {pos}");
                return;
            }

            bool result = true;

#if ONLINE_MODE
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_Place res = (W2C_Res_Place)await gate_session.Call(new C2W_Req_Place()
            {
                chessid = Define.GetId(pos)
            });
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                result = true;
            }
            else
            {
                result = false;
                UIUtils.ShowErrorCode(res.Error);
            }
#endif

            if (result == false) { return; }

            Context.Event.Run(Define.Exe_Place, pos);
        }
    }
}
