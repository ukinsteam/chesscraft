﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;

namespace ETT.Client.WuZiQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_PlaceHandler : AbstractMsgHandler<W2C_Ntf_Place>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_Place msg)
        {
            Context.Event.Run(Define.Exe_Place, Define.GetPos(msg.chessid));

            await ETTask.CompletedTask;
        }
    }
}
