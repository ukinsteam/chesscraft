﻿using ETT.AI.Common;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.WuZiQi.Battle
{
    [Event(Define.Select)]
    public class SelectEventHandler : IEvent<SceneUnit, bool>
    {
        private ChessSceneUnit CurrUnit { get; set; }

        public void Run(SceneUnit t, bool state)
        {
            if (t == null || t.Root == null)
            {
                return;
            }

            if (state == true)
            {
                Select(t);
            }
            else
            {
                Deselect(t);
            }
        }

        private void Switch(SceneUnit unit)
        {
            CurrUnit?.ConfirmSelect(false);
            if (unit is ChessSceneUnit)
            {
                unit.ConfirmSelect(true);
                CurrUnit = unit as ChessSceneUnit;
            }
        }

        private void Select(SceneUnit unit)
        {
            if (unit == null)
            {
                return;
            }
            switch (unit)
            {
                case ChessSceneUnit chess_unit:
                    Switch(unit);
                    break;
                case PathPointSceneUnit path_unit:
                    CurrUnit?.ConfirmSelect(false);
                    Context.Event.Run(Define.Place, unit.Pos);
                    break;
            }
        }

        private void Deselect(SceneUnit unit)
        {
            unit.ConfirmSelect(false);

            if (CurrUnit == unit)
            {
                CurrUnit = null;
            }
        }
    }
}
