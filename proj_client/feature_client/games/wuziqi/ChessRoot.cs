﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Client.Scene;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.WuZiQi
{
    /// <summary>
    /// 五子棋
    /// </summary>
    public class ChessRoot : AbstractChessEntity, IAwake, IStart
    {
        public override ECommonGameKindTable Kind => ECommonGameKindTable.WuZiQi;

        public void Awake()
        {
            Root = this;

            Log.Debug("WuZiQi.ChessRoot Awake");
        }

        public void Start()
        {
            SceneMgr.OnSwitchFinish += OnSceneLoadCompleteHnadler;
        }

        public override void Enter()
        {
            AddComponent<LoginComponent>();
            AddComponent<LobbyComponent>();
            AddComponent<BattleComponent>();

            GetComponent<LoginComponent>().Enter();
        }

        public override void Exit()
        {
            RemoveComponent<LoginComponent>();
            RemoveComponent<LobbyComponent>();
            RemoveComponent<BattleComponent>();
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            SceneMgr.OnSwitchFinish -= OnSceneLoadCompleteHnadler;
        }

        private void OnSceneLoadCompleteHnadler(GenericScene src, GenericScene tgt)
        {
            if (tgt != null && tgt.ConfId == (uint)EGenericSceneTable.scene_battle)
            {
                Define.BoardET = Root.Factory.CreateWithParent<ChessBoardEntity>(this);
                Define.BoardET.AddComponent<ChessBoardComponent>();
                Define.BoardET.AddComponent<PathPointManager>();

                WndMgr.Show((int)EGenericWndTable.Battle);

                var render = Context.Game.GetComponent<RenderModelComponent>();
                var model_id = (uint)EGenericModelTable.Elven;
                if (render.Get(model_id) == null)
                {
                    render.UnloadModel(model_id);
                }
                else
                {
                    render.SetActive((uint)EGenericModelTable.Elven, false);
                }

                var background = GameObject.Find("Background");
                var root = GameObject.Find("game/root");

                if (Define.MineFaction == AI.WuZiQi.EChessPieceFaction.RED)
                {
                    UIUtils.SetLocalRotation(background, 0, 90, 0);
                    UIUtils.SetLocalRotation(root, 0, 0, 0);
                }
                else if (Define.MineFaction == AI.WuZiQi.EChessPieceFaction.BLACK)
                {
                    UIUtils.SetLocalRotation(background, 0, -90, 0);
                    UIUtils.SetLocalRotation(root, 0, 180, 0);
                }
            }
        }


        public override void GameOver()
        {
            Define.BoardET.Dispose();
            Define.BoardET = null;
        }
    }
}
