﻿using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Model.network;
using ETT.Widgets;
using UnityEngine;

namespace ETT.Client.WuZiQi.Lobby
{
    [Window((int)EGenericWndTable.Lobby)]
    public class LobbyWnd : AbstractWnd
    {
#pragma warning disable 0649
        //[UIField("room_list/grid_scroll/grid_container")]
        //private UICircularGrid grid_rooms;
        [UIField("root/create")]
        private UIButton btn_create;
        [UIField("root/join")]
        private UIButton btn_join;
#pragma warning restore 0649

        protected override void OnShow()
        {
            UIEventListener.Get(btn_create).onClick += OnBtnCreateClickHandler;
            UIEventListener.Get(btn_join).onClick += OnBtnJoinClickHandler;
        }

        protected override void OnHide()
        {
            UIEventListener.Get(btn_create).onClick -= OnBtnCreateClickHandler;
            UIEventListener.Get(btn_join).onClick -= OnBtnJoinClickHandler;
        }

        #region Event Handler
#if ONLINE_MODE
        private async void OnBtnCreateClickHandler(GameObject obj)
        {
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_CreateRoom res = (W2C_Res_CreateRoom)await gate_session.Call(new C2W_Req_CreateRoom() { });
            Log.Debug($"res.Error = {res.Error}");
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                MatchWnd.WindowParam param = new MatchWnd.WindowParam()
                {
                    RoomId = res.RoomId,
                    MasterName = PlayerPrefs.GetString("login.name")
                };
                WndMgr.Show((int)EGenericWndTable.Match, param);
            }
            else
            {
                ShowErrorCode(res.Error);
            }
        }
#else
        private void OnBtnCreateClickHandler(GameObject obj)
        {
            Context.Game.GetComponent<SelectGameComponent>().SwitchModule<BattleComponent>();
        }
#endif

        private void OnBtnJoinClickHandler(GameObject obj)
        {
            WndMgr.Show((int)EGenericWndTable.JoinRoom);
        }
        #endregion
    }
}
