﻿using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model.network;
using ETT.Widgets;
using UnityEngine;

namespace ETT.Client.WuZiQi.Lobby
{
    [Window((int)EGenericWndTable.Match)]
    [UIEventLabel]
    public class MatchWnd : AbstractWnd
    {
        public const string ShowApplierEvt = "WZQ_ShowApplierEvt";

        public class WindowParam : IWindowParam
        {
            public uint RoomId { get; set; }
            public string MasterName { get; set; }
        }

        private WindowParam Param => WndParam as WindowParam;

        private long m_applier_id;

#pragma warning disable 0649
        [UIField("root/refuse")]
        private UIButton btn_refuse;
        [UIField("root/accept")]
        private UIButton btn_accept;
        [UIField("root/cancel")]
        private UIButton btn_cancel;
        [UIField("root/panel_accepter/name")]
        private UILabel lbl_accepter_name;
        [UIField("root/panel_applier/name")]
        private UILabel lbl_applier_name;
        //[UIField("root/matching")]
        //private UILabel lbl_matching;
        [UIField("root/panel_roomid/roomid")]
        private UILabel lbl_roomid;
#pragma warning restore 0649

        protected override void OnInit()
        {
            SetBtnGroupVisible(false);

            lbl_accepter_name.text = "";
            lbl_applier_name.text = "";
            lbl_roomid.text = "";
        }

        protected override void OnShow()
        {
            lbl_roomid.text = Param.RoomId.ToString();
            lbl_accepter_name.text = Param.MasterName;

            UIEventListener.Get(btn_accept).onClick += OnBtnAcceptClickHandler;
            UIEventListener.Get(btn_refuse).onClick += OnBtnRefuseClickHandler;
            UIEventListener.Get(btn_cancel).onClick += OnBtnCancelClickHandler;
        }

        protected override void OnHide()
        {
            UIEventListener.Get(btn_accept).onClick -= OnBtnAcceptClickHandler;
            UIEventListener.Get(btn_refuse).onClick -= OnBtnRefuseClickHandler;
            UIEventListener.Get(btn_cancel).onClick -= OnBtnCancelClickHandler;
        }

        #region Custom Method
        public void SetBtnGroupVisible(bool b)
        {
            btn_accept.SetActive(b);
            btn_refuse.SetActive(b);
            btn_cancel.SetActive(b == false);
        }
        #endregion

        #region Event Handler

        private async void OnBtnAcceptClickHandler(GameObject obj)
        {
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_DealJoin res = (W2C_Res_DealJoin)await gate_session.Call(new C2W_Req_DealJoin() { IsAccept = true, PlayerId = m_applier_id });
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                Close();
            }
            else
            {
                ShowErrorCode(res.Error);
            }
        }

        private async void OnBtnRefuseClickHandler(GameObject obj)
        {
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_DealJoin res = (W2C_Res_DealJoin)await gate_session.Call(new C2W_Req_DealJoin() { IsAccept = false, PlayerId = m_applier_id });
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                m_applier_id = 0;
                lbl_applier_name.text = "";

                SetBtnGroupVisible(false);
            }
            else
            {
                ShowErrorCode(res.Error);
            }
        }

        private async void OnBtnCancelClickHandler(GameObject obj)
        {
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_QuitRoom res = (W2C_Res_QuitRoom)await gate_session.Call(new C2W_Req_QuitRoom() { });
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                Close();
            }
            else
            {
                ShowErrorCode(res.Error);
            }
        }

        [UIEventHandler(ShowApplierEvt)]
        private void ShowApplier(long id, string name)
        {
            m_applier_id = id;
            lbl_applier_name.text = name;

            SetBtnGroupVisible(true);

            UIEventMgr.Run(GenericTipsWnd.ShowTipsEvent, $"{name}请求加入");
        }

        #endregion
    }
}
