namespace ETT.Client.ZhongGuoXiangQi
{
	/// <summary>
	/// Range [140000,150000)
	/// <summary>
	public enum EBattleErrCodeTable
	{
		//比赛不存在
		NoMatch = 143000,
		//棋子不存在
		NoChessman = 143001,
		//操作非法，棋子不属于己方
		NotMyChess = 143002,
		//移动操作非法，目标位置无效
		InvalidPos = 143003,
		//操作非法，无法击败目标单位
		CannotKill = 143004,
		//正在倒计时，请耐心等待
		OnCD = 143005,
		//正等在对方落子，请耐心等待
		NotMyTurn = 143006,
		//对方掉线
		TargetOffline = 143007,
	}

}
