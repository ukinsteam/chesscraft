namespace ETT.Client.ZhongGuoXiangQi
{
	/// <summary>
	/// Range [1000,2000)
	/// <summary>
	public enum EGenericSceneTable
	{
		//登陆场景
		scene_login = 1000,
		//大厅场景
		scene_lobby = 1001,
		//中国象棋比赛场景
		scene_battle = 1002,
	}

}
