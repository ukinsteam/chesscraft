namespace ETT.Client.ZhongGuoXiangQi
{
	/// <summary>
	/// Generic协议枚举 Range[20000,25000)
	/// <summary>
	public enum EGenericProto
	{
		C2W_Req_CreateRoom = 20200,
		W2C_Res_CreateRoom = 20201,
		C2W_Req_QuitRoom = 20205,
		W2C_Res_QuitRoom = 20206,
		C2W_Req_JoinRoom = 20210,
		W2C_Res_JoinRoom = 20211,
		W2C_Ntf_JoinRoom = 20212,
		C2W_Req_DealJoin = 20215,
		W2C_Res_DealJoin = 20216,
		W2C_Ntf_DealJoin = 20217,
		W2C_Ntf_StartMatch = 20220,
	}

}
