﻿using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Model.network;
using ETT.Widgets;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi.Lobby
{
    [Window((int)EGenericWndTable.JoinRoom)]
    public class JoinWnd : AbstractWnd
    {

#pragma warning disable 0649
        [UIField("root/BG")]
        private UIImage img_bg;
        [UIField("root/panel_roomid/InputField")]
        private UIInputField ipt_roomid;
        [UIField("root/join")]
        private UIButton btn_join;
#pragma warning restore 0649

        protected override void OnShow()
        {
            img_bg.raycastTarget = true;
            UIEventListener.Get(img_bg).onClick += OnImgBGClickHandler;
            UIEventListener.Get(btn_join).onClick += OnBtnJoinClickHandler;
        }

        protected override void OnHide()
        {
            UIEventListener.Get(img_bg).onClick -= OnImgBGClickHandler;
            UIEventListener.Get(btn_join).onClick -= OnBtnJoinClickHandler;
        }


        #region Event Handler

        private async void OnBtnJoinClickHandler(GameObject obj)
        {
            uint.TryParse(ipt_roomid.text, out uint roomid);
            if (roomid == 0)
            {
                Log.Error("Error，无效的房间ID");
                return;
            }
            img_bg.raycastTarget = false;

            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_JoinRoom res = (W2C_Res_JoinRoom)await gate_session.Call(new C2W_Req_JoinRoom() { RoomId = roomid });
            if (res.Error != (int)ECommonErrCodeTable.Success)
            {
                ShowErrorCode(res.Error);
            }

            img_bg.raycastTarget = true;
        }

        private void OnImgBGClickHandler(GameObject obj)
        {
            Close();
        }

        #endregion
    }
}
