﻿using ETT.AI.ZhongGuoXiangQi;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.MayBeKilled)]
    public class BeKilledEventHandler : IEvent<ChessSceneUnit, ChessSceneUnit>
    {
        public void Run(ChessSceneUnit attacker, ChessSceneUnit defender)
        {

        }
    }
}
