﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Model;
using ETT.Model.network;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.Move)]
    public class MoveEventHandler : IEvent<ChessSceneUnit, UVector2Int>
    {
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        public void Run(ChessSceneUnit unit, UVector2Int pos)
        {
#if ONLINE_MODE
            RunAsync(unit, pos).Coroutine();
#else
            RunSync(unit, pos);
#endif
        }


#if ONLINE_MODE
        private async ETVoid RunAsync(ChessSceneUnit unit, UVector2Int pos)
#else
        private void RunSync(ChessSceneUnit unit, UVector2Int pos)
#endif
        {
            var chess_piece = unit.GetParent<ChessPieceEntity>();

            var foothold = Context.Table.GetComponent<FootholdTable>()[pos];

            if (foothold == null)
            {
                Log.Error($"Foothold is not exist point = {pos}");
                return;
            }
            var agentMgr = chess_piece.GetComponent<ChessPieceComponent>();
            if (agentMgr.CanMove(pos) == false)
            {
                Log.Error($"Can not Move To = {pos}");
                return;
            }

            bool result = true;

#if ONLINE_MODE
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_Move res = (W2C_Res_Move)await gate_session.Call(new C2W_Req_Move()
            {
                chess = new ChessVO()
                {
                    id = agentMgr.Conf.Id,
                    x = pos.x,
                    y = pos.y
                }
            });
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                result = true;
            }
            else
            {
                result = false;
                UIUtils.ShowErrorCode(res.Error);
            }
#endif

            if (result == false) { return; }

            Context.Event.Run(Define.Exe_Move, unit, pos);
        }
    }
}
