﻿using ETT.AI.Common;
using ETT.Model;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.Exe_Kill)]
    public class Execute_KillEventHandler : IEvent<ChessSceneUnit, ChessSceneUnit>
    {
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();
        protected TimeComponent TimeMgr => Context.Game.GetComponent<TimeComponent>();

        public void Run(ChessSceneUnit attacker, ChessSceneUnit defender)
        {
            RunAsync(attacker, defender).Coroutine();
        }

        private Vector3 TranslateVec(UVector2Int pos)
        {
            var footholdtable = Context.Table.GetComponent<FootholdTable>();
            var conf = footholdtable[pos];
            return new Vector3(conf.Axis.X, 0, conf.Axis.Y);
        }

        private async ETVoid RunAsync(ChessSceneUnit attacker, ChessSceneUnit defender)
        {
            var atk_piece = attacker.GetParent<ChessPieceEntity>();
            var def_piece = defender.GetParent<ChessPieceEntity>();

            var agent_atk = atk_piece.GetComponent<ChessPieceComponent>();
            var agent_def = def_piece.GetComponent<ChessPieceComponent>();

            var anim_atk = atk_piece.GetComponent<AnimateComponent>();
            var move_atk = atk_piece.GetComponent<MovableComponent>();

            var anim_def = def_piece.GetComponent<AnimateComponent>();
            var move_def = def_piece.GetComponent<MovableComponent>();


            anim_atk.Run();
            await move_atk.NearAsync(TranslateVec(def_piece.CurrPos));

            anim_atk.Stop();

            //atk_播放攻击动画
            anim_atk.Attack();
            await TimeMgr.WaitAsync(800);

            //def_播放死亡动画
            anim_def.Dead();

            await TimeMgr.WaitAsync(200);

            anim_atk.StopAttack();

            //def_消失
            defender.gameObject.SetActive(false);
            defender.transform.localPosition = Vector3.zero;
            def_piece.IsAvailable = false;

            anim_atk.Run();
            await move_atk.MoveAsync(TranslateVec(def_piece.CurrPos));

            anim_atk.Stop();

            agent_atk.Kill(def_piece.CurrPos);
            //atk_播放位移动画
            //atk_位移

            //atk_调整朝向

            if (Define.isGameOver == true)
            {
                Context.Event.Run(Define.Exe_GameOver);
            }
        }
    }
}
