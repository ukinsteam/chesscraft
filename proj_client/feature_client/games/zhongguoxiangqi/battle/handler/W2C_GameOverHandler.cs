﻿using ETT.AI.ZhongGuoXiangQi;
using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_GameOverHandler : AbstractMsgHandler<W2C_Ntf_GameOver>
    {
        private TimeComponent TimeMgr => Context.Game.GetComponent<TimeComponent>();
        protected SceneComponent SceneMgr => Context.Game.GetComponent<SceneComponent>();


        protected override async ETTask Run(Session session, W2C_Ntf_GameOver msg)
        {
            Define.isGameOver = true;
            Define.WinFaction = (EChessPieceFaction)msg.faction;

            await ETTask.CompletedTask;
        }
    }
}
