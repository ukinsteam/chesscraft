﻿using ETT.AI.ZhongGuoXiangQi;
using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：Execute_GameOverEventHandler
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2019/10/13 21:47:49
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.Exe_GameOver)]
    public class Execute_GameOverEventHandler : IEvent
    {
        public void Run()
        {
            var str = Define.WinFaction == Define.MineFaction ? UIUtils.Localize("胜利") : UIUtils.Localize("失败");
            UIEventUtils.Run(GenericTipsWnd.ShowTipsEvent, str);

            Define.VO = null;
            Define.StartTime = 0;
            Define.isOnTurn = false;
            Define.MineFaction = EChessPieceFaction.INVALID;

            var select = Context.Game.GetComponent<SelectGameComponent>();
            var model = Context.Game.GetComponent<RenderModelComponent>();
            var model_id = (uint)EGenericModelTable.Elven;
            select.GameOver();

            model.SetActive(model_id, true);

            select.SwitchModule<LobbyComponent>();
        }
    }
}
