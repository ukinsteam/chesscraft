﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_SelectHandler : AbstractMsgHandler<W2C_Ntf_Select>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_Select msg)
        {

            await ETTask.CompletedTask;
        }
    }
}
