﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Model;
using ETT.Model.network;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.Kill)]
    public class KillEventHandler : IEvent<ChessSceneUnit, ChessSceneUnit>
    {
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        public void Run(ChessSceneUnit attacker, ChessSceneUnit defender)
        {
#if ONLINE_MODE
            RunAsync(attacker, defender).Coroutine();
#else
            RunSync(attacker, defender);
#endif
        }

#if ONLINE_MODE
        private async ETVoid RunAsync(ChessSceneUnit attacker, ChessSceneUnit defender)
#else
        private void RunSync(ChessSceneUnit attacker, ChessSceneUnit defender)
#endif
        {
            var atk_piece = attacker.GetParent<ChessPieceEntity>();
            var def_piece = defender.GetParent<ChessPieceEntity>();

            var agent_atk = atk_piece.GetComponent<ChessPieceComponent>();
            var agent_def = def_piece.GetComponent<ChessPieceComponent>();
            if (agent_atk.CanKill(def_piece.CurrPos) == false)
            {
                return;
            }

            bool result = true;

#if ONLINE_MODE
            Session gate_session = NetworkMgr.Get(NetworkMgr.GateAddr);
            W2C_Res_Kill res = (W2C_Res_Kill)await gate_session.Call(new C2W_Req_Kill()
            {
                attackerid = agent_atk.Conf.Id,
                defenderid = agent_def.Conf.Id
            });
            if (res.Error == (int)ECommonErrCodeTable.Success)
            {
                result = true;
            }
            else
            {
                result = false;
                UIUtils.ShowErrorCode(res.Error);
            }
#endif

            if (result == false) { return; }

            Context.Event.Run(Define.Exe_Kill, attacker, defender);
        }
    }
}
