﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_MoveHandler : AbstractMsgHandler<W2C_Ntf_Move>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_Move msg)
        {
            var select_comp = Context.Game.GetComponent<SelectGameComponent>();
            var board = select_comp.CurrChess.GetComponent<ChessBoardEntity>();

            var chess_piece = board[msg.chess.id];
            if (chess_piece == null)
            {
                throw new Exception($"Error！ Unit Is Not Exist, id={msg.chess.id}");
            }

            var unit = chess_piece.GetComponent<ChessSceneUnit>();

            Context.Event.Run(Define.Exe_Move, unit, UVector2Int.Parse(msg.chess.x, msg.chess.y));

            await ETTask.CompletedTask;
        }
    }
}
