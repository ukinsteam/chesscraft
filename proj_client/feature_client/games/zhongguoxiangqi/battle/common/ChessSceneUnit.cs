﻿using ETT.AI.ZhongGuoXiangQi;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi
{
    public class ChessSceneUnit : SceneUnit, IAwake<ChessmanTableConf>
    {
        public ChessmanTableConf ChessConf { get; private set; }

        public void Awake(ChessmanTableConf conf)
        {
            ChessConf = conf;

            base.Awake(conf.ModelConf);

        }

        protected override void CreateShell()
        {
            base.CreateShell();

            var container = GameObject.Find("root/chessman_container");
            var foothold = Context.Table.GetComponent<FootholdTable>()[ChessConf.BirthPoint];
            transform.SetParent(container.transform);
            UIUtils.SetLayer(gameObject, container.layer);
            if (foothold != null)
            {
                transform.localPosition = new Vector3(foothold.Axis.X, 0, foothold.Axis.Y);
            }
            else
            {
                transform.localPosition = new Vector3(0, 0, 0);
            }
            gameObject.name = string.Format("chess_{0}_{1}", ChessConf.Type, ChessConf.Id);
            transform.localRotation = Quaternion.Euler(new Vector3().Copy(ChessConf.Forward));
        }

        protected override void OnLoadComplete()
        {
            base.OnLoadComplete();

            ModelGO.name = $"{ChessConf.Id}";

            Pos = ChessConf.BirthPoint;
        }
    }
}
