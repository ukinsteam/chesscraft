﻿using ETT.AI.Common;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi
{
    public class PathPointManager : Model.Component
    {
        private List<PathPointSceneUnit> m_units = new List<PathPointSceneUnit>();

        private ModelTableConf m_path_conf;
        private int m_showcnt = 0;

        private ChessSceneUnit CurrUnit { get; set; }


        public void ShowPathPoint(ChessSceneUnit unit)
        {
            if (unit == null)
            {
                HidePathPoint();
                return;
            }
            CurrUnit = unit;

            if (m_path_conf == null)
            {
                m_path_conf = Context.Table.GetComponent<ModelTable>()[(uint)EBattleModelTable.PathPoint];
            }
            var curr_piece = CurrUnit.GetParent<ChessPieceEntity>();

            if (curr_piece.Faction == (int)Define.MineFaction)
            {
                ShowPathPointAsync().Coroutine();
            }
            else
            {
                HidePathPoint();
            }
        }

        private async ETVoid ShowPathPointAsync()
        {
            var curr_piece = CurrUnit.GetParent<ChessPieceEntity>();
            var agent = curr_piece.GetComponent<ChessPieceComponent>();
            var points = agent.FindPoints();
            var board = curr_piece.Board;

            if (m_units.Count < points.Count)
            {
                var cnt = points.Count - m_units.Count;
                for (int i = 0; i < cnt; ++i)
                {
                    PathPointSceneUnit unit = CurrUnit.Root.Factory.Create<PathPointSceneUnit, ModelTableConf>(m_path_conf);
                    unit.SelectEvt = CurrUnit.SelectEvt;
                    await unit.LoadModel();
                    m_units.Add(unit);
                }
            }

            m_showcnt = points.Count;

            for (int i = 0; i < m_units.Count; ++i)
            {
                if (i >= m_showcnt)
                {
                    m_units[i].SetActive(false);
                }
                else
                {
                    m_units[i].SetActive(true);
                    var chess_piece = board[points[i]] as ChessPieceEntity;
                    if (chess_piece != null)
                    {
                        m_units[i].SetInfo(points[i], TranslateVec(points[i]), false);
                        Context.Event.Run(Define.MayBeKilled, CurrUnit, chess_piece.GetParent<ChessSceneUnit>());
                    }
                    else
                    {
                        m_units[i].SetInfo(points[i], TranslateVec(points[i]), true);
                    }
                }
            }
        }

        public void HidePathPoint()
        {
            foreach (var point in m_units)
            {
                point.SetActive(false);
            }
        }


        private Vector3 TranslateVec(UVector2Int pos)
        {
            var footholdtable = Context.Table.GetComponent<FootholdTable>();
            var conf = footholdtable[pos];
            return new Vector3(conf.Axis.X, 0, conf.Axis.Y);
        }
    }
}
