﻿using ETT.Client.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.DouShouQi
{
    /// <summary>
    /// 斗兽棋
    /// </summary>
    public class ChessRoot : AbstractChessEntity, IAwake
    {
        public override ECommonGameKindTable Kind => ECommonGameKindTable.DouShouQi;

        public void Awake()
        {
            Root = this;
        }

        public override void Enter()
        {
            Log.Debug("Enter DouShouQi");
        }

        public override void Exit()
        {
            Log.Debug("Exit DouShouQi");
        }

        public override void GameOver()
        {

        }
    }
}
