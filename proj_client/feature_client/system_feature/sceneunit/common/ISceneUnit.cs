﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public interface ISceneUnit
    {
#pragma warning disable IDE1006 // 命名样式
        GameObject gameObject { get; }
        Transform transform { get; }
#pragma warning restore IDE1006 // 命名样式
    }
}
