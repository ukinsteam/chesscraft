﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class AnimateComponent : Model.Component, IAwake<Transform>, IUpdate
    {
        private Animator m_animator;

        private float m_move_speed = 0;
        private float m_curr_move_speed = 0;

        public void Awake(Transform go)
        {
            m_animator = go.GetComponent<Animator>();
            if (m_animator == null)
            {
                throw new Exception("Error！Animator不存在");
            }
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            m_animator = null;
        }

        public void Update()
        {
            if (IsDisposed == true)
            {
                return;
            }

            if (Mathf.Abs(m_curr_move_speed - m_move_speed) < 0.1f)
            {
                m_curr_move_speed = m_move_speed;
            }
            else
            {
                m_curr_move_speed += (m_move_speed - m_curr_move_speed) * Time.deltaTime * 2;
            }
            m_animator?.SetFloat("Speed_f", m_curr_move_speed);
        }


        public void Move()
        {
            m_move_speed = 0.5f;
            //m_animator.SetFloat("Speed_f", 0.5f);
        }

        public void Stop()
        {
            m_move_speed = 0.0f;
            //m_animator.SetFloat("Speed_f", 0.0f);
        }

        public void Run()
        {
            m_move_speed = 1.0f;
        }

        public void Attack()
        {
            m_animator?.SetInteger("WeaponType_int", 12);
        }

        public void StopAttack()
        {
            m_animator?.SetInteger("WeaponType_int", 0);
        }

        public void Dead()
        {

        }
    }
}
