﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class SceneUnitComponent : Model.Component, IAwake
    {
        private Dictionary<long, SceneUnit> m_all_dict = new Dictionary<long, SceneUnit>();

        public void Awake()
        {
        }

        public void Add(SceneUnit unit)
        {

        }

        public void Remove(SceneUnit unit)
        {

        }

    }
}
