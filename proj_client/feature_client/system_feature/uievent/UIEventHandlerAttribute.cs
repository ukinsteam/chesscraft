﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class UIEventHandlerAttribute : BaseAttribute
    {
        public string Type { get; }

        public UIEventHandlerAttribute(string str)
        {
            Type = str;
        }
    }
}
