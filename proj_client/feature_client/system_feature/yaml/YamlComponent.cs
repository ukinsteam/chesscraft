﻿using ETT.Client.YAML;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using YamlDotNet.Serialization;

namespace ETT.Client
{
    public class YamlComponent : Model.Component, IAwake
    {
        public ClobalYML Global { get; private set; }

        public void Awake()
        {
            Deserializer deserializer;

            var obj = Resources.Load(@"common/common/yaml/global") as TextAsset;
            deserializer = new Deserializer();
            Global = deserializer.Deserialize<ClobalYML>(obj.text);
        }
    }
}
