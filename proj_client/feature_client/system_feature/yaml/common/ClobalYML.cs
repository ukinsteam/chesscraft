﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.YAML
{
    public sealed class ClobalYML
    {
        public string LoginAddr { get; set; }
        public string RenderShellPath { get; set; }
    }
}
