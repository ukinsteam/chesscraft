﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.Res
{
    public class ResourcesMode : Model.Component, IResMode
    {
        private readonly Dictionary<string, LoadVO> m_dict = new Dictionary<string, LoadVO>();
        private readonly Dictionary<string, ResourceLoadAsync> m_loading_dict = new Dictionary<string, ResourceLoadAsync>();

        public AssetVO GetAsset(string path)
        {
            if (m_dict.ContainsKey(path) == false)
            {
                return null;
            }
            var waiter = m_dict[path];

            if (waiter.State != ELoadAssetState.Complete)
            {
                return null;
            }

            return waiter.Ent;
        }

        public ILoadAsync GetLoadAsync(string path)
        {
            if (m_loading_dict.ContainsKey(path) == false)
            {
                return null;
            }
            return m_loading_dict[path];
        }

        //public AssetVO Load(string path)
        //{
        //    LoadVO waiter = null;
        //    if (m_dict.ContainsKey(path) == false)
        //    {
        //        waiter = new LoadVO
        //        {
        //            Ent = new AssetVO(path),
        //            State = ELoadAssetState.None,
        //        };
        //        m_dict.Add(path, waiter);
        //    }
        //    waiter = m_dict[path];
        //    switch (waiter.State)
        //    {
        //        case ELoadAssetState.Complete:
        //            return waiter.Ent;
        //        case ELoadAssetState.Loading:
        //            return null;
        //        case ELoadAssetState.None:
        //            waiter.Ent.Asset = Resources.Load(path);
        //            waiter.State = ELoadAssetState.Complete;
        //            return waiter.Ent;
        //        default: break;
        //    }
        //    return null;
        //}


        public async ETTask<AssetVO> LoadAync(string path)
        {
            if (m_dict.ContainsKey(path) == false)
            {
                LoadVO waiter = new LoadVO
                {
                    Ent = new AssetVO(path),
                    State = ELoadAssetState.None,
                };
                m_dict.Add(path, waiter);
            }

            var loadwaiter = m_dict[path];
            switch (loadwaiter.State)
            {
                case ELoadAssetState.None:
                case ELoadAssetState.Loading:
                    loadwaiter.State = ELoadAssetState.Loading;
                    using (var async_operate = Context.Game.Factory.Create<ResourceLoadAsync>())
                    {
                        m_loading_dict.Add(path, async_operate);
                        loadwaiter.Ent.Asset = await async_operate.LoadAsync(path);
                        m_loading_dict.Remove(path);
                    }
                    return loadwaiter.Ent;
                case ELoadAssetState.Complete:
                    return loadwaiter.Ent;
            }
            return null;
        }
    }
}
