﻿using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Client
{
    public static class UIUtils
    {
        public static void ParseUIFields(GameObject root, IUIFieldParser parser)
        {
            FieldInfo[] fs = parser.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance);
            Type fieldType = typeof(UIFieldAttribute);

            foreach (FieldInfo f in fs)
            {
                UIFieldAttribute attr = (UIFieldAttribute)Attribute.GetCustomAttribute(f, fieldType);
                if (attr == null)
                {
                    continue;
                }

                Transform trans = root.transform.Find(attr.Path);
                if (trans == null)
                {
                    Log.Error($"组件路径无效：path = {attr.Path}; wnd = {root.name}");
                    continue;
                }

                GameObject go = trans.gameObject;

                var m = typeof(AbstractWnd).GetMethod("AddWidget", new Type[] { typeof(string), typeof(GameObject) });

                MethodInfo method = m.MakeGenericMethod(f.FieldType);
                var r = method.Invoke(parser, new object[] { attr.Path, go });
                f.SetValue(parser, r);
            }
        }

        #region LocalScale
        public static void SetLocalScale(GameObject go, float scale)
        {
            if (go == null) return;

            SetLocalScale(go.transform, scale);
        }

        public static void SetLocalScale(Transform trans, float scale)
        {
            if (trans == null) return;

            trans.localScale = Vector3.one * scale;
        }

        public static void SetLocalScale(GameObject go, Vector3 scale)
        {
            if (go == null) return;

            SetLocalScale(go.transform, scale);
        }

        public static void SetLocalScale(Transform trans, Vector3 scale)
        {
            if (trans == null) return;

            trans.localScale = scale;
        }
        #endregion

        #region LocalRotation
        public static void SetLocalRotation(GameObject go, float x, float y, float z)
        {
            if (go == null) return;
            SetLocalRotation(go.transform, x, y, z);
        }
        public static void SetLocalRotation(Transform go, float x, float y, float z)
        {
            if (go == null) return;
            go.localRotation = Quaternion.Euler(x, y, z);
        }

        public static void SetLocalRotation(GameObject go, Vector3 vec)
        {
            if (go == null) return;
            SetLocalRotation(go.transform, vec);
        }

        public static void SetLocalRotation(Transform go, Vector3 vec)
        {
            if (go == null) return;
            go.localRotation = Quaternion.Euler(vec);
        }
        #endregion

        public static void SetLayer(GameObject go, int layer, bool withchildren = true)
        {
            if (go == null) { return; }

            if (withchildren == false)
            {
                go.layer = layer;
                return;
            }
            var children = go.GetComponentsInChildren<Transform>();
            foreach (var child in children)
            {
                child.gameObject.layer = layer;
            }
        }

        public static void DestroyChildren(GameObject go)
        {
            if (go == null) { return; }

            var tr = go.transform;
            for (int i = tr.childCount - 1; i >= 0; i--)
            {
                UnityEngine.Object.Destroy(tr.GetChild(i).gameObject);
            }
        }

        public static void SetParent(GameObject go, GameObject parent)
        {
            if (go == null || parent == null) { return; }

            SetParent(go.transform, parent.transform);
        }

        public static void SetParent(Transform go, Transform parent)
        {
            if (go == null || parent == null) { return; }

            go.SetParent(parent);
            go.localPosition = Vector3.zero;
            go.localRotation = Quaternion.identity;
            go.localScale = Vector3.one;
        }

        public static void SetActive(GameObject go, bool active)
        {
            if (go == null || go.activeSelf == active) return;

            go.SetActive(active);
        }

        public static void SetLabel(GameObject go, string content)
        {
            if (go == null) return;

            Text ui_text = go.GetComponent<Text>();
            if (ui_text != null)
            {
                ui_text.text = content;
            }
        }

        public static GameObject FindChild(GameObject go, string name)
        {
            var child = go.transform.Find(name);
            if (child == null)
            {
                return null;
            }
            return child.gameObject;
        }

        public static void ShowErrorCode(int errcode)
        {
            var conf = Context.Table.GetComponent<ErrCodeTable>()[(uint)errcode];
            if (conf != null)
            {
                UIEventUtils.Run(GenericTipsWnd.ShowTipsEvent, Localize(conf.Desc));
            }
        }

        public static string Localize(string keycode)
        {
            var conf = Context.Table.GetComponent<LocalizeTable>()[keycode];
            if (conf == null)
            {
                return keycode;
            }
            return conf.Translation;
        }

        //public static void PlayAnim(GameObject go, string name)
        //{
        //    //var animator = go.GetComponent<Animator>();
        //}

        //public static async void AttachFx(GameObject parent, string assetname)
        //{
        //    var res_mgr = Context.Game.GetComponent<ResourcesComponent>();
        //    AssetVO vo = await res_mgr.LoadAsync(assetname);
        //    AttachFx(parent, vo);
        //}

        //public static void AttachFx(GameObject parent, AssetVO ent)
        //{
        //    if (ent == null)
        //    {
        //        return;
        //    }
        //    UnityEngine.Object obj = ent.Asset;
        //    if (obj != null)
        //    {
        //        GameObject go = GameObject.Instantiate(obj) as GameObject;
        //        SetParent(go, parent);

        //        var root = parent.transform;
        //        Canvas canvas = root.GetComponent<Canvas>();
        //        while (canvas == null && root != null)
        //        {
        //            root = root.transform.parent;
        //            canvas = root?.GetComponent<Canvas>();
        //        }

        //        if (canvas != null)
        //        {
        //            Renderer[] renders = go.GetComponentsInChildren<Renderer>();

        //            foreach (Renderer render in renders)
        //            {
        //                render.sortingLayerName = canvas.sortingLayerName;
        //                render.sortingOrder = canvas.sortingOrder + 1;
        //            }
        //        }
        //    }
        //}
    }
}
