﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public static class DataUtils
    {

        public static Vector3 Copy(this Vector3 v, UVector3 vec)
        {
            v.x = vec.x;
            v.y = vec.y;
            v.z = vec.z;
            return v;
        }

        public static Vector3 Copy(this Vector3 v, UVector3Int vec)
        {
            v.x = vec.x;
            v.y = vec.y;
            v.z = vec.z;
            return v;
        }
    }
}
