﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public static class FileUtils
    {
        /// <summary>
        /// 计算文件的MD5值
        /// </summary>
        public static string ToMd5(string file)
        {
            try
            {
                FileStream fs = new FileStream(file, FileMode.Open);
                System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                byte[] retVal = md5.ComputeHash(fs);
                fs.Close();

                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < retVal.Length; i++)
                {
                    sb.Append(retVal[i].ToString("x2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw new Exception("md5file() fail, error:" + ex.Message);
            }
        }

        /// <summary>
        /// 获取文件的大小
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static long ToSize(string file)
        {
            try
            {
                if (File.Exists(file) == false)
                {
                    return 0;
                }
                FileInfo fileInfo = new FileInfo(file);
                return fileInfo.Length;
            }
            catch (Exception ex)
            {
                throw new Exception("ToSize() fail, error:" + ex.Message);
            }
        }

        /// <summary>
        /// 清空文件夹
        /// </summary>
        /// <param name="path"></param>
        public static void DeleteDir(string path)
        {
            try
            {
                if (Directory.Exists(path) == false)
                {
                    Directory.CreateDirectory(path);
                }
                DirectoryInfo dir = new DirectoryInfo(path);
                FileSystemInfo[] fileinfo = dir.GetFileSystemInfos();  //返回目录中所有文件和子目录
                foreach (FileSystemInfo i in fileinfo)
                {
                    if (i is DirectoryInfo)            //判断是否文件夹
                    {
                        DirectoryInfo subdir = new DirectoryInfo(i.FullName);
                        subdir.Delete(true);          //删除子目录和文件
                    }
                    else
                    {
                        File.Delete(i.FullName);      //删除指定文件
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        /// <summary>
        /// 获取该目录下的所有文件，包括所有层级的子目录
        /// </summary>
        /// <param name="path"></param>
        /// <param name="files"></param>
        /// <param name="pattern"></param>
        /// <param name="ignoredirstartwithstr"></param>
        public static void GetAllFiles(string path, ref List<string> files, string pattern, string ignoredirstartwithstr = "")
        {
            if (files == null || Directory.Exists(path) == false)
            {
                return;
            }

            files.Clear();
            DirectoryInfo dir = new DirectoryInfo(path);
            if (ignoredirstartwithstr.Length > 0 && dir.Name.StartsWith(ignoredirstartwithstr) == true)
            {
                return;
            }
            ErgodicGetAllFile(dir, ref files, pattern, ignoredirstartwithstr);
        }

        /// <summary>
        /// 获取多个目录下的所有文件，包括所有层级的子目录
        /// </summary>
        /// <param name="path"></param>
        /// <param name="files"></param>
        /// <param name="pattern"></param>
        /// <param name="ignoredirstartwithstr"></param>
        public static void GetAllFiles(string[] path, ref List<string> files, string pattern, string ignoredirstartwithstr = "")
        {
            if (files == null)
            {
                return;
            }
            files.Clear();

            for (int i = 0; i < path.Length; ++i)
            {
                if (Directory.Exists(path[i]) == false)
                {
                    continue;
                }

                DirectoryInfo dir = new DirectoryInfo(path[i]);
                if (ignoredirstartwithstr.Length > 0 && dir.Name.StartsWith(ignoredirstartwithstr) == true)
                {
                    continue;
                }
                ErgodicGetAllFile(dir, ref files, pattern, ignoredirstartwithstr);
            }
        }

        private static void ErgodicGetAllFile(DirectoryInfo dir, ref List<string> outfiles, string pattern, string ignoredirstartwithstr = "")
        {
            foreach (FileInfo f in dir.GetFiles(pattern))
            {
                outfiles.Add(f.FullName.Replace('\\', '/'));
            }
            foreach (DirectoryInfo d in dir.GetDirectories())
            {
                if (ignoredirstartwithstr.Length > 0 && d.Name.StartsWith(ignoredirstartwithstr) == true)
                {
                    continue;
                }
                ErgodicGetAllFile(d, ref outfiles, pattern, ignoredirstartwithstr);
            }
        }

        /// <summary>
        /// 重命名文件名
        /// </summary>
        /// <param name="srcname"></param>
        /// <param name="tgtname"></param>
        /// <returns></returns>
        public static bool RenameFile(string srcname, string tgtname)
        {
            if (File.Exists(srcname) == false)
            {
                return false;
            }
            FileInfo fileInfo = new FileInfo(srcname);
            fileInfo.MoveTo(tgtname);
            return true;
        }
    }
}
