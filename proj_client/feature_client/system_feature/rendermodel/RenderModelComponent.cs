﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class RenderModelComponent : Model.Component, IAwake
    {
        public GameObject RenderLayer { get; private set; }

        private Queue<GenericRender> m_pool = new Queue<GenericRender>();
        private Dictionary<uint, GenericRender> m_dict = new Dictionary<uint, GenericRender>();

        public void Awake()
        {
            RenderLayer = GameObject.Find("Render");

            UnityEngine.Object.DontDestroyOnLoad(RenderLayer);
        }

        public void LoadModel(uint id, bool enable_light = true)
        {
            if (m_dict.ContainsKey(id) == true)
            {
                return;
            }

            GenericRender shell;
            if (m_pool.Count == 0)
            {
                shell = new GenericRender();
            }
            else
            {
                shell = m_pool.Dequeue();
            }
            shell.Load(id, enable_light);
            m_dict.Add(id, shell);
        }

        public void UnloadModel(uint id)
        {
            if (m_dict.ContainsKey(id) == false)
            {
                return;
            }
            var shell = m_dict[id];
            shell.Unload();
            shell.SetActive(false);
            m_pool.Enqueue(shell);
            m_dict.Remove(id);
        }

        public GenericRender Get(uint id)
        {
            if (m_dict.ContainsKey(id) == false)
            {
                return null;
            }
            return m_dict[id];
        }

        public void SetActive(uint id, bool active)
        {
            if (m_dict.ContainsKey(id) == false)
            {
                return;
            }
            var shell = m_dict[id];
            shell.SetActive(false);
        }
    }
}
