﻿using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class GenericRender
    {
        protected GameObject render_model;
        protected Camera m_camera;
        protected GameObject m_root;
        protected Light m_light;

        protected GameObject m_model;

        private bool m_enable_light = false;

        public void Load(uint id, bool enable_light = true)
        {
            m_enable_light = enable_light;
            LoadAsync(id).Coroutine();
        }

        public void Unload()
        {
            GameObject.Destroy(m_model);
            m_model = null;
        }

        private async ETVoid LoadAsync(uint id)
        {
            if (render_model == null)
            {
                var path = Context.Game.GetComponent<YamlComponent>().Global.RenderShellPath;
                render_model = await Context.Game.GetComponent<ResourcesComponent>().GetInstantiate(path);

                var trans = Context.Game.GetComponent<RenderModelComponent>().RenderLayer;
                UIUtils.SetParent(render_model, trans);
                UIUtils.SetLayer(render_model, trans.layer);

                SetRender(render_model);
            }

            UIUtils.SetActive(m_light.gameObject, m_enable_light);

            var model_conf = Context.Table.GetComponent<ModelTable>()[id];
            m_model = await Context.Game.GetComponent<ResourcesComponent>().GetInstantiate(model_conf.Path);
            UIUtils.SetParent(m_model, m_root);
            UIUtils.SetLayer(m_model, m_root.layer);

            SetActive(true);
        }

        protected void SetRender(GameObject go)
        {
            m_camera = UIUtils.FindChild(go, "Camera").GetComponent<Camera>();
            m_root = UIUtils.FindChild(go, "Root");
            m_light = UIUtils.FindChild(go, "Light").GetComponent<Light>();

            UIUtils.SetLayer(go, 8, true);
        }

        public virtual void SetActive(bool active)
        {
            if (render_model == null || render_model.activeSelf == active) return;

            render_model.SetActive(active);
        }

        public void EnableCameraBlur(bool enable)
        {
            var blur = m_camera.GetComponent<IEnableEffect>();
            blur.Enable(enable);
        }

        public void SetRenderTexture(RenderTexture texture)
        {
            if (m_camera != null)
            {
                m_camera.targetTexture = texture;
            }
        }
    }
}
