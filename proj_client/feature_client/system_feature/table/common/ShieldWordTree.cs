﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Table
{
    internal class ShieldWordNode
    {
        public Dictionary<int, ShieldWordNode> Next { get; private set; } = new Dictionary<int, ShieldWordNode>();

        public void Construct(ShieldwordTableConf conf, int index)
        {
            if (index >= conf.Word.Length)
            {
                return;
            }

            char c = conf.Word[index];
            if (Next.ContainsKey(c) == false)
            {
                Next.Add(c, new ShieldWordNode());
            }
            Next[c].Construct(conf, index + 1);
        }

        public bool Match(char[] word, ref int index)
        {
            if (index >= word.Length)
            {
                return false;
            }
            char c = word[index];
            if (Next.ContainsKey(c) == false)
            {
                return false;
            }
            if (Next[c].Next.Count == 0)
            {
                return true;
            }
            index++;
            return Next[c].Match(word, ref index);
        }
    }

    internal class ShieldWordTree
    {
        private readonly ShieldwordTable m_table;

        private ShieldWordNode root;

        public ShieldWordTree(ShieldwordTable table)
        {
            m_table = table;

            root = new ShieldWordNode();

            var words = table.FindAllVO();
            for (int i = 0; i < words.Count; ++i)
            {
                root.Construct(words[i], 0);
            }
        }

        public bool Match(char[] word)
        {
            bool result = false;
            for (int i = 0; i < word.Length;)
            {
                int start_idx = i;
                int end_idx = i;
                bool r = root.Match(word, ref end_idx);
                if (r == true)
                {
                    result = true;
                    //Log.Warning(word.Substring(start_idx, end_idx - start_idx + 1));
                    i = end_idx;
                    for (int j = start_idx; j <= end_idx; ++j)
                    {
                        word[j] = '*';
                    }
                }
                else
                {
                    i++;
                }
            }
            return result;
        }
    }
}
