﻿using ETT.Model;
using ETT.Table;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;

namespace ETT.Client.Table
{
    public class TextParser : AbstractTableParser
    {
        private void ParseTextContent(IAbstractTable t, List<TextAsset> assets)
        {
            //Log.Debug(t.Name);
            CSVDataTable dt = new CSVDataTable(t.TableVO);
            foreach (var txtAsset in assets)
            {
                string content = txtAsset.text;
                //StringReader sr = new StringReader(content);
                //m_table.Deserialize(sr);
                var subdt = new CSVDataTable(t.TableVO);
                subdt.Parse(content);
                dt.DataTable.Union(subdt.DataTable);
            }

            t.CSVDT = dt;

            MemoryStream stream = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(stream);
            dt.DataTable.Build(bw);
            stream.Flush();
            byte[] buffer = stream.GetBuffer();
            stream.Close();
            stream.Close();

            MemoryStream stream2 = new MemoryStream(buffer);
            BinaryReader br = new BinaryReader(stream2);
            t.Deserialize(br);
        }


        public override void Parse()
        {
            //m_isall = true;
            var res_mgr = Context.Game.GetComponent<ResourcesComponent>();
            var table_list = Mgr.FindAll();
            if (table_list == null || table_list.Count == 0)
            {
                return;
            }

            Processing = true;

            for (int i = 0; i < table_list.Count; ++i)
            {
                var m_table = table_list[i];
                List<TextAsset> res_list = new List<TextAsset>();
                for (int j = 0; j < m_table.Path.Count; ++j)
                {
                    var obj = Resources.Load(m_table.Path[j]);
                    res_list.Add(obj as TextAsset);
                }
                ParseTextContent(m_table, res_list);
            }

            Processing = false;
            //onParseComplete?.Invoke();
        }

        public override void Parse(string name)
        {
            ////m_isall = false;
            //m_table = TableMgr.Find(name);
            //ResMgr.Instance.LoadAsync(m_table.Path, OnLoadSingleTableComplete);
        }

        //private void OnLoadSingleTableComplete(bool isComplete, List<AssetVO> assets)
        //{
        //    if (isComplete == true)
        //    {
        //        ParseTextContent(assets);
        //    }
        //    else
        //    {
        //        Log.Warning("Warning! Table Load Failed.");
        //    }
        //    Processing = false;
        //    onParseComplete?.Invoke();

        //}
    }
}
