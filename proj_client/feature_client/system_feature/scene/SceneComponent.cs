﻿using ETT.Client.Common;
using ETT.Client.Scene;
using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public class SceneComponent : Component, IAwake, IUpdate
    {
        public Action<GenericScene, GenericScene> OnSwitchBegin;
        public Action<GenericScene, GenericScene> OnSwitchFinish;
        public Action<GenericScene, GenericScene> OnSwitchFail;

        private readonly Dictionary<uint, GenericScene> m_dict = new Dictionary<uint, GenericScene>();

        private WndComponent WndMgr => Context.Game.GetComponent<WndComponent>();

        public GenericScene CurrScene { get; private set; }
        public GenericScene TgtScene { get; private set; }

        private bool m_loadbegin = false;
        public float ProgressValue
        {
            get
            {
                if (m_loadbegin == false)
                {
                    return 0;
                }
                return TgtScene.ProgressValue;
            }
        }

        public void Awake()
        {
            Log.Debug("SceneComponent Awake");
        }

        public void Update()
        {
            foreach (var pair in m_dict)
            {
                pair.Value.Update();
            }
        }

        public void Switch(uint id, uint bg_model_id)
        {
            m_loadbegin = false;

            TgtScene = null;
            if (m_dict.ContainsKey(id) == true)
            {
                TgtScene = m_dict[id];
            }
            else
            {
                TgtScene = new GenericScene(id);
                m_dict.Add(id, TgtScene);
            }
            OnSwitchBegin?.Invoke(CurrScene, TgtScene);

            TgtScene.OnEnterBegin = OnSceneEnterBeginHandler;
            TgtScene.OnEnterFinish = OnSceneEnterEndHandler;

            if (CurrScene != null)
            {
                CurrScene.OnExitBegin = OnSceneExitBeginHandler;
                CurrScene.OnExitFinish = OnSceneExitEndHandler;

                CurrScene.Exit();
            }
            else
            {
                SceneLoadingWnd.WindowParam param = new SceneLoadingWnd.WindowParam()
                {
                    BGModelId = bg_model_id
                };
                WndMgr.Show((int)ECommonWndTable.SceneLoading, param);
                TgtScene.Enter();
            }
        }


        #region Event Handler

        private void OnSceneEnterBeginHandler(GenericScene obj)
        {
            m_loadbegin = true;
        }

        private void OnSceneEnterEndHandler(GenericScene obj)
        {
            OnSwitchFinish?.Invoke(CurrScene, TgtScene);
        }

        private void OnSceneExitBeginHandler(GenericScene obj)
        {
            WndMgr.Show((int)ECommonWndTable.SceneLoading);
        }

        private void OnSceneExitEndHandler(GenericScene obj)
        {
            TgtScene.Enter();
        }

        #endregion
    }
}
