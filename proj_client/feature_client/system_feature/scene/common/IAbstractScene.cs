﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Scene
{
    public interface IAbstractScene
    {
        void Enter();

        void Exit();
    }
}
