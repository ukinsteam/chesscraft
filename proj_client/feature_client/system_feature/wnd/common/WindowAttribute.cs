﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Wnd
{
    public class WindowAttribute : BaseAttribute
    {
        public uint WndId { get; }

        public WindowAttribute(uint wnd_id)
        {
            WndId = wnd_id;
        }
    }
}
