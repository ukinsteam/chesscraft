﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.Wnd
{
    public abstract class AbstractPanel
    {
        protected GameObject m_gameObject;
        protected GameObject m_root;

        public GameObject GameObject => m_gameObject;

        protected ResourcesComponent ResMgr => Context.Game.GetComponent<ResourcesComponent>();

        protected bool m_isShow = false;
        public bool IsShow => m_isShow;
        public void SetPanel(string assetname, GameObject root)
        {
            m_gameObject = ResMgr.GetSyncInstantiate(assetname);

            m_root = root;

            //m_gameObject.transform.position = new Vector3(-1000, -1000);
            UIUtils.SetParent(m_gameObject, m_root);
            //UIUtils.SetActive(m_gameObject, false);
            m_isShow = false;
        }

        public virtual void OnInit()
        {

        }

        public virtual void OnShow()
        {
            //UIMgr.SetActive(m_gameObject, true);
            m_isShow = true;
        }

        public virtual void OnUpdate()
        {
        }

        public virtual void OnHide()
        {
            m_isShow = false;
            //UIMgr.SetActive(m_gameObject, false);
        }

        public virtual void OnDestroy()
        {
            ResMgr.Recycle(m_gameObject);
        }

        protected GameObject Find(string name)
        {
            var child = m_gameObject.transform.Find(name);
            return child.gameObject;
        }

        protected GameObject Find(GameObject parent, string name)
        {
            var child = parent.transform.Find(name);
            return child.gameObject;
        }
    }
}
