﻿using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Model;
using ETT.Widgets;
using System.Collections.Generic;
using UnityEngine;

namespace ETT.Client.Wnd
{
    [Window((int)ECommonWndTable.Tips)]
    [UIEventLabel]
    public class GenericTipsWnd : AbstractWnd
    {
        public const string ShowTipsEvent = "ShowTipsEvent";
        public const string ShowRollEvent = "ShowRollEvent";

#pragma warning disable 0649
        [UIField("root/tips")]
        private UIWidget go_tips_root;
        //[UIField("root/roll")]
        //private UIWidget go_roll_root;
#pragma warning restore 0649

        private Dictionary<uint, Queue<GameObject>> m_pool = new Dictionary<uint, Queue<GameObject>>();
        private TimeComponent TimeMgr => Context.Game.GetComponent<TimeComponent>();

        protected override void OnShow()
        {
        }

        protected override void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.Escape) == true)
            {
                var cmd_wnd = WndMgr.GetWnd((uint)ECommonWndTable.Command);
                if (cmd_wnd.WndState == EWndState.Show || cmd_wnd.WndState == EWndState.Loading)
                {
                    WndMgr.Hide((uint)ECommonWndTable.Command);
                }
                else
                {
                    WndMgr.Show((uint)ECommonWndTable.Command);
                }
            }
        }

        protected override void OnHide()
        {
        }

        protected override void OnDestroy()
        {
            foreach (var pair in m_pool)
            {
                var queue = pair.Value;
                while (queue.Count > 0)
                {
                    var go = queue.Dequeue();
                    ResMgr.Recycle(go);
                }
                queue.Clear();
            }
            m_pool.Clear();
        }

        #region Event Handler

        [UIEventHandler(ShowTipsEvent)]
        private async void ShowTips(string content)
        {
            var type = (uint)ECommonTipsTable.Common;
            if (m_pool.ContainsKey(type) == false)
            {
                m_pool.Add(type, new Queue<GameObject>());
            }
            var queue = m_pool[type];
            var conf = Context.Table.GetComponent<TipsTable>()[type];
            GameObject go = null;
            if (queue.Count == 0)
            {
                go = await ResMgr.GetInstantiate(conf.ResPath);
                UIUtils.SetParent(go, go_tips_root.gameObject);
            }
            else
            {
                go = queue.Dequeue();
                go.SetActive(true);
            }
            UIUtils.SetLabel(go, content);
            await TimeMgr.WaitAsync(conf.During);
            go.SetActive(false);
            queue.Enqueue(go);
        }

        [UIEventHandler(ShowRollEvent)]
        private void ShowRoll(string content)
        {
            Log.Debug($"ShowRoll content = {content}");
            //var conf = TableMgr.TipsTableVO.GenericFindVO((uint)EGenericTipsTable.Common);

        }

        #endregion
    }
}
