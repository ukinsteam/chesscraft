﻿using ETT.Client.Common;
using ETT.Client.Scene;
using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class WndComponent : Model.Component, IAwake, IUpdate//, IUIFieldParser
    {
        private enum EWndOperateState
        {
            Invalid,
            Show,
            Hide,
            Destroy
        }
        private struct WndOperate
        {
            public EWndOperateState state;
            public UInt32 wndid;
            public IWindowParam param;
        }
        private SceneComponent SceneMgr => Context.Game.GetComponent<SceneComponent>();

        public GameObject UIRoot { get; private set; }
        public Camera UICamera { get; private set; }
        public GameObject EventSystem { get; private set; }

        private Dictionary<ESortingLayer, GameObject> m_layerdict = new Dictionary<ESortingLayer, GameObject>();

        private Dictionary<uint, AbstractWnd> m_registerwnd = new Dictionary<uint, AbstractWnd>();

        private Dictionary<uint, AbstractWnd> m_showDict = new Dictionary<uint, AbstractWnd>();
        private Dictionary<uint, AbstractWnd> m_hideDict = new Dictionary<uint, AbstractWnd>();

        private Queue<WndOperate> m_operates = new Queue<WndOperate>();

        public void Awake()
        {
            UIRoot = GameObject.Find("UIRoot");

            InitLayer(ESortingLayer.Battle, "UIRoot/UI/BattleLayer");
            InitLayer(ESortingLayer.BattleTips, "UIRoot/UI/BattleTipsLayer");
            InitLayer(ESortingLayer.Normal, "UIRoot/UI/NormalLayer");
            InitLayer(ESortingLayer.Dialog, "UIRoot/UI/DialogLayer");
            InitLayer(ESortingLayer.Tips, "UIRoot/UI/TipsLayer");
            InitLayer(ESortingLayer.Guide, "UIRoot/UI/GuideLayer");
            InitLayer(ESortingLayer.Loading, "UIRoot/UI/LoadingLayer");
            InitLayer(ESortingLayer.Top, "UIRoot/UI/TopLayer");

            var uicamerago = GameObject.Find("UIRoot/Camera");
            if (uicamerago != null)
            {
                UICamera = uicamerago.GetComponent<Camera>();
            }

            EventSystem = GameObject.Find("UIRoot/EventSystem");

            UnityEngine.Object.DontDestroyOnLoad(UIRoot);

            SceneMgr.OnSwitchBegin += OnSceneSwitchBeginHandler;

            //CmdMgr.Register(new OpenWndOrder());
            //CmdMgr.Register(new ShowTipsOrder());
            //CmdMgr.Register(new ShowErrcodeOrder());

            m_registerwnd.Clear();
            var attr_types = Context.Event.GetAttrTypes(typeof(WindowAttribute));
            foreach (var type in attr_types)
            {
                object[] attr_list = type.GetCustomAttributes(typeof(WindowAttribute), false);
                if (attr_list.Length == 0)
                {
                    continue;
                }
                if (attr_list[0] is WindowAttribute win_attr)
                {
                    var generic_method = typeof(FactoryEntity).GetMethods();

                    for (int i = 0; i < generic_method.Length; ++i)
                    {
                        var m = generic_method[i];
                        if (string.CompareOrdinal(m.ToString(), "T CreateWithParent[T,A](ETT.Model.Component, A, Boolean)") == 0)
                        {
                            MethodInfo method = m.MakeGenericMethod(type, typeof(uint));
                            var r = method.Invoke(Context.Game.Factory, new object[] { this, win_attr.WndId, true });
                            m_registerwnd.Add(win_attr.WndId, r as AbstractWnd);
                            break;
                        }
                    }
                }
            }

            m_showDict.Clear();
            m_hideDict.Clear();

            Log.Debug("WndComponent Awake");
        }

        public void SwitchWndRoot(IRootComponent root)
        {
            foreach (var pair in m_registerwnd)
            {
                pair.Value.Root = root;
            }
        }

        private void InitLayer(ESortingLayer e, string path)
        {
            var layer = GameObject.Find(path);
            UIUtils.DestroyChildren(layer);
            m_layerdict.Add(e, layer);
        }

        public GameObject GetLayer(ESortingLayer e)
        {
            if (m_layerdict.ContainsKey(e) == false)
            {
                return null;
            }
            return m_layerdict[e];
        }

        public void Show(uint wnd_id, IWindowParam param = null)
        {
            WndOperate operate;
            operate.state = EWndOperateState.Show;
            operate.wndid = wnd_id;
            operate.param = param;
            m_operates.Enqueue(operate);
        }

        public void Hide(uint wnd_id)
        {
            WndOperate operate;
            operate.state = EWndOperateState.Hide;
            operate.wndid = wnd_id;
            operate.param = null;
            m_operates.Enqueue(operate);
        }

        private void DestroyWnd(uint wndid)
        {
            WndOperate operate;
            operate.state = EWndOperateState.Destroy;
            operate.wndid = wndid;
            operate.param = null;
            m_operates.Enqueue(operate);
        }

        public void Update()
        {
            foreach (var pair in m_showDict)
            {
                pair.Value.Update();
            }

            if (m_operates.Count == 0)
            {
                return;
            }
            var operate = m_operates.Dequeue();
            if (m_registerwnd.ContainsKey(operate.wndid) == false)
            {
                Log.Error($"Wnd Is Not Registed, id={operate.wndid}");
                return;
            }
            AbstractWnd wndvo;
            wndvo = m_registerwnd[operate.wndid];

            wndvo.WndParam = operate.param;
            switch (operate.state)
            {
                case EWndOperateState.Show:
                    OnShow(wndvo);
                    break;
                case EWndOperateState.Hide:
                    OnHide(wndvo);
                    break;
                case EWndOperateState.Destroy:
                    OnDestroy(wndvo);
                    break;
                default: break;
            }
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            SceneMgr.OnSwitchBegin -= OnSceneSwitchBeginHandler;

            foreach (var vo in m_registerwnd)
            {
                vo.Value.Dispose();
            }
            m_registerwnd.Clear();

        }
        private void OnShow(AbstractWnd vo)
        {
            int maxSortingOrder = 0;
            foreach (var pair in m_showDict)
            {
                if (pair.Value.sortingorder > maxSortingOrder)
                {
                    maxSortingOrder = pair.Value.sortingorder;
                }
            }
            if (m_hideDict.ContainsKey(vo.ConfId) == true)
            {
                vo.sortingorder = maxSortingOrder + 1;
                vo.Show();
                m_hideDict.Remove(vo.ConfId);
                m_showDict.Add(vo.ConfId, vo);
            }
            else
            {
                vo.sortingorder = maxSortingOrder + 1;
                vo.Show();
                m_showDict.Add(vo.ConfId, vo);
            }
        }

        private List<AbstractWnd> m_hide_lst = new List<AbstractWnd>();
        private void OnHide(AbstractWnd vo)
        {
            if (m_showDict.ContainsKey(vo.ConfId) == true)
            {
                m_hide_lst.Clear();
                m_hide_lst.Add(vo);

                foreach (var pair in m_showDict)
                {
                    if (pair.Value.WndConf.RelyWnd == vo.ConfId)
                    {
                        m_hide_lst.Add(pair.Value);
                    }
                }

                foreach (var wnd in m_hide_lst)
                {
                    wnd.Hide();
                    m_showDict.Remove(wnd.ConfId);
                    m_hideDict.Add(wnd.ConfId, wnd);
                }
            }
        }

        private void OnDestroy(AbstractWnd vo)
        {
            if (m_showDict.ContainsKey(vo.ConfId) == true)
            {
                OnHide(vo);
            }

            if (m_hideDict.ContainsKey(vo.ConfId) == true)
            {
                foreach (var pair in m_showDict)
                {
                    if (pair.Value.WndConf.RelyWnd == vo.ConfId)
                    {
                        OnDestroy(pair.Value);
                    }
                }

                vo.Dispose();
                m_hideDict.Remove(vo.ConfId);
            }
        }

        public AbstractWnd GetWnd(uint wndId)
        {
            if (m_registerwnd.ContainsKey(wndId))
            {
                return m_registerwnd[wndId];
            }
            return null;
        }
        private void DealWndAfterSwitchScene(AbstractWnd vo, GenericScene tgt_scene)
        {
            var conf = vo.WndConf;
            bool isdepend = false;
            for (int i = 0; i < conf.DependScene.Length; ++i)
            {
                if (conf.DependScene[i] != 0)
                {
                    isdepend = true;
                    break;
                }
            }
            if (isdepend == false)
            {
                return;
            }
            if (DataTypeUtils.Contain(conf.DependScene, tgt_scene.ConfId) == false)
            {
                DestroyWnd(vo.ConfId);
            }
        }
        private void OnSceneSwitchBeginHandler(GenericScene src_scene, GenericScene tgt_scene)
        {
            foreach (var pair in m_showDict)
            {
                DealWndAfterSwitchScene(pair.Value, tgt_scene);
            }

            foreach (var pair in m_hideDict)
            {
                DealWndAfterSwitchScene(pair.Value, tgt_scene);
            }
        }

        public void ShowGenericDialog(string content, string title = "", Action onsurefunc = null, Action oncancelfunc = null, string confirmlbl = "", string cancellbl = "")
        {
            GenericDialogWnd.WindowParam param = new GenericDialogWnd.WindowParam()
            {
                Title = title,
                Content = content,
                OnConfirmFunc = onsurefunc,
                OnCancelFunc = oncancelfunc,
                ConfirmText = confirmlbl,
                CancelText = cancellbl
            };
            Show((int)ECommonWndTable.Dialog, param);
        }

        public void ShowTips(int tips_type, string content)
        {

        }
    }
}
