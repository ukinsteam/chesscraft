using ETT.Model.proto;
using System.IO;

namespace ETT.Client
{
	public partial class PlayerVO : AbstractProtoPacker
	{
		/// <summary>
		/// 玩家ID
		/// <summary>
		public long PlayerId;

		/// <summary>
		/// 玩家名字
		/// <summary>
		public string PlayerName;

		public override void Pack(BinaryWriter writer)
		{
			writer.Write(PlayerId);
			writer.Write(PlayerName);
		}

		public override void UnPack(BinaryReader reader)
		{
			PlayerId = reader.ReadInt64();
			PlayerName = reader.ReadString();
		}
	}
}
