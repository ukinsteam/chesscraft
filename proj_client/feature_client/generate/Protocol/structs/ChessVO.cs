using ETT.Model.proto;
using System.IO;

namespace ETT.Client
{
	public class ChessVO : AbstractProtoPacker
	{
		/// <summary>
		/// 棋子id
		/// <summary>
		public uint id;

		/// <summary>
		/// 棋子x坐标
		/// <summary>
		public int x;

		/// <summary>
		/// 棋子y坐标
		/// <summary>
		public int y;

		public override void Pack(BinaryWriter writer)
		{
			writer.Write(id);
			writer.Write(x);
			writer.Write(y);
		}

		public override void UnPack(BinaryReader reader)
		{
			id = reader.ReadUInt32();
			x = reader.ReadInt32();
			y = reader.ReadInt32();
		}
	}
}
