using ETT.Model;

namespace ETT.Client
{
	public enum ERoomState
	{
		Invalid = 0,
		Wait = 1,
		Ready = 2,
		Battle = 3,
		MAX = 4,
	}
}
