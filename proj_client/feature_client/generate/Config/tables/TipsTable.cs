using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Client.Table
{
	public class TipsTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public string Desc { get; private set; }

		public string ResPath { get; private set; }

		public int During { get; private set; }

		public string Anim { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Desc = br.ReadString();

				ResPath = br.ReadString();

				During = br.ReadInt32();

				Anim = br.ReadString();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class TipsTable : SingleKeyTable<TipsTableConf,uint>
	{
		public override string Name { get { return "TipsTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"common/common/table/tips",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='TipsTable' >
						<field name='Id' type='uint' primarykey='true' desc='Id' />
						<field name='Name' type='string' desc='名称' />
						<field name='Desc' type='string' desc='描述' />
						<field name='ResPath' type='string' desc='Prefabs资源路径' />
						<field name='During' type='int' desc='持续时间单位毫秒' />
						<field name='Anim' type='string' desc='动画名称' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
