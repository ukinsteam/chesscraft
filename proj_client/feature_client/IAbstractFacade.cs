﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public interface IAbstractFacade
    {
        void Init();
        void Update();
        void LateUpdate();
        void Destroy();
    }
}
