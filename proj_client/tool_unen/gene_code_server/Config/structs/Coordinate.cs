using ETT.Model;
using System;
using System.IO;
using UKon.Config;
using UKon.Log;

namespace ETT.Server.Table
{
	public class Coordinate : AbstractTableStruct
	{
		public float X { get; private set; }

		public float Y { get; private set; }

		public bool Deserialize(BinaryReader br)
		{
			try
			{
				X = br.ReadSingle();
				Y = br.ReadSingle();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

		public static Coordinate CreateFromBinary(BinaryReader br)
		{
			var obj = new Coordinate();
			obj.Deserialize(br);
			return obj;
		}
	}
}
