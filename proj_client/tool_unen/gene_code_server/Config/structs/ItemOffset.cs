using ETT.Model;
using System;
using System.IO;
using UKon.Config;
using UKon.Log;

namespace ETT.Server.Table
{
	public class ItemOffset : AbstractTableStruct
	{
		public uint ItemID { get; private set; }

		private ItemTableConf m_itemid_conf;
		public ItemTableConf ItemIDConf
		{
			get
			{
				if (m_itemid_conf == null)
				{
					var tableMgr = Context.Game.GetComponent<TableComponent>();
					m_itemid_conf = tableMgr.ItemTableVO.GenericFindVO(ItemID);
				}
				return m_itemid_conf;
			}
		}

		public uint ItemCnt { get; private set; }

		public bool Deserialize(BinaryReader br)
		{
			try
			{
				ItemID = br.ReadUInt32();
				m_itemid_conf = null;

				ItemCnt = br.ReadUInt32();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

		public static ItemOffset CreateFromBinary(BinaryReader br)
		{
			var obj = new ItemOffset();
			obj.Deserialize(br);
			return obj;
		}
	}
}
