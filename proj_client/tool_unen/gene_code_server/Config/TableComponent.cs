using ETT.Server.Table;

namespace ETT.Server
{
	public partial class TableComponent
	{
		public ItemTable ItemTableVO { get; private set; } = new ItemTable();
		public AwardTable AwardTableVO { get; private set; } = new AwardTable();
		public WndTable WndTableVO { get; private set; } = new WndTable();
		public SceneTable SceneTableVO { get; private set; } = new SceneTable();
		public LocalizeTable LocalizeTableVO { get; private set; } = new LocalizeTable();
		public ErrCodeTable ErrCodeTableVO { get; private set; } = new ErrCodeTable();
		public ChessmanTable ChessmanTableVO { get; private set; } = new ChessmanTable();
		public FootholdTable FootholdTableVO { get; private set; } = new FootholdTable();
		public ProtocolTable ProtocolTableVO { get; private set; } = new ProtocolTable();

		private void InnerInit()
		{
			m_tabledict.Add(ItemTableVO.Name, ItemTableVO);
			m_tabledict.Add(AwardTableVO.Name, AwardTableVO);
			m_tabledict.Add(WndTableVO.Name, WndTableVO);
			m_tabledict.Add(SceneTableVO.Name, SceneTableVO);
			m_tabledict.Add(LocalizeTableVO.Name, LocalizeTableVO);
			m_tabledict.Add(ErrCodeTableVO.Name, ErrCodeTableVO);
			m_tabledict.Add(ChessmanTableVO.Name, ChessmanTableVO);
			m_tabledict.Add(FootholdTableVO.Name, FootholdTableVO);
			m_tabledict.Add(ProtocolTableVO.Name, ProtocolTableVO);
		}
	}
}
