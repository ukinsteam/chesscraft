using ETT.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon;
using UKon.Config;
using UKon.Log;
using UKon.AI.ChineseChess;

namespace ETT.Server.Table
{
	public class ChessmanTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public EChessPieceType Type { get; private set; }

		public UVector2Int BirthPoint { get; private set; }

		public string ResPath { get; private set; }

		public string[] Preload { get; private set; }

		public UVector3 LookAt { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Type = (EChessPieceType)Enum.Parse(typeof(EChessPieceType), br.ReadString());

				BirthPoint = UVector2Int.Parse(br.ReadString());

				ResPath = br.ReadString();

				int m_preload_cnt = br.ReadInt32();
				Preload = new string[m_preload_cnt];
				for (int i = 0; i < m_preload_cnt; ++i)
				{
					Preload[i] = br.ReadString();
				}

				LookAt = UVector3.Parse(br.ReadString());
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class ChessmanTable : SingleKeyTable<ChessmanTableConf,uint>
	{
		public override string Name { get { return "ChessmanTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"fbattle_chinesechess/table/chessman",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ChessmanTable' >
						<field name='Id' type='uint' primarykey='true' desc='棋子索引' />
						<field name='Name' type='string' desc='棋子名称' />
						<field name='Type' type='EChessPieceType' desc='棋子类型' />
						<field name='BirthPoint' type='vector2int' desc='棋子出生点位' />
						<field name='ResPath' type='string' desc='模型资源' />
						<field name='Preload' type='array(string)' desc='预加载资源数组' />
						<field name='LookAt' type='vector3' desc='棋子初始朝向' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
