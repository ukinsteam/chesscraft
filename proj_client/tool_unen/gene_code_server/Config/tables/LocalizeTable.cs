using ETT.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon;
using UKon.Config;
using UKon.Log;

namespace ETT.Server.Table
{
	public class LocalizeTableConf : SingleKeyItem<string>
	{
		public override string PrimaryKey => KeyCode;

		public string KeyCode { get; private set; }

		public string Translation { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				KeyCode = br.ReadString();

				Translation = br.ReadString();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class LocalizeTable : SingleKeyTable<LocalizeTableConf,string>
	{
		public override string Name { get { return "LocalizeTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"generic/table/localize",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='LocalizeTable' >
						<field name='KeyCode' type='string' primarykey='true' desc='索引' />
						<field name='Translation' type='string' desc='翻译内容' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				string primarykey = key;
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				string range1 = min;
				string range2 = max;
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
