using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Chinesechess
{
	public class ChinesechessSendServer
	{
		public static readonly ChinesechessSendServer Instance = new ChinesechessSendServer();
		private ChinesechessSendServer() { }

		private NetMgr m_netMgr = NetMgr.Instance;

		/// <summary>
		/// 请求选中棋子
		/// </summary>
		/// <param name="chessid">棋子id</param>
		public bool ReqSelect(uint chessid)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EChinesechessProto.REQ_SELECT;
			package.Stream.Write(chessid);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求移动棋子
		/// </summary>
		/// <param name="chess">棋子状态</param>
		public bool ReqMove(ChessVO chess)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EChinesechessProto.REQ_MOVE;
			chess.Encode(package.Stream);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求击杀棋子
		/// </summary>
		/// <param name="attackerid">进攻方棋子id</param>
		/// <param name="defenderid">防守方棋子id</param>
		public bool ReqKill(uint attackerid, uint defenderid)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EChinesechessProto.REQ_KILL;
			package.Stream.Write(attackerid);
			package.Stream.Write(defenderid);
			m_netMgr.Send(package);
			return true;
		}
	}
}
