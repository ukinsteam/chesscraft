using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Chinesechess
{
	public class ChinesechessRecvServer : AbstractRecvServer
	{

		/// <summary>
		/// 返回选中棋子
		/// </summary>
		/// <param name="errcode">返回结果</param>
		/// <param name="chessid">棋子id</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.RES_SELECT)]
		public bool ResSelect(uint errcode, uint chessid)
		{
			return true;
		}

		/// <summary>
		/// 通知选中棋子
		/// </summary>
		/// <param name="chessid">棋子id</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.NTF_SELECT)]
		public bool NtfSelect(uint chessid)
		{
			return true;
		}

		/// <summary>
		/// 返回移动棋子
		/// </summary>
		/// <param name="errcode">返回结果</param>
		/// <param name="chess">棋子状态</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.RES_MOVE)]
		public bool ResMove(uint errcode, ChessVO chess)
		{
			return true;
		}

		/// <summary>
		/// 通知移动棋子
		/// </summary>
		/// <param name="chess">棋子状态</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.NTF_MOVE)]
		public bool NtfMove(ChessVO chess)
		{
			return true;
		}

		/// <summary>
		/// 返回击杀棋子
		/// </summary>
		/// <param name="errcode">返回结果</param>
		/// <param name="attackerid">进攻方棋子id</param>
		/// <param name="defenderid">防守方棋子id</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.RES_KILL)]
		public bool ResKill(uint errcode, uint attackerid, uint defenderid)
		{
			return true;
		}

		/// <summary>
		/// 通知击杀棋子
		/// </summary>
		/// <param name="attackerid">进攻方棋子id</param>
		/// <param name="defenderid">防守方棋子id</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.NTF_KILL)]
		public bool NtfKill(uint attackerid, uint defenderid)
		{
			return true;
		}

		/// <summary>
		/// 通知顺序
		/// </summary>
		/// <param name="isOnTurn">是否轮到</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.NTF_TURN)]
		public bool NtfTurn(bool isOnTurn)
		{
			return true;
		}

		/// <summary>
		/// 被踢出房间
		/// </summary>
		/// <param name="errcode">被踢原因</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.NTF_KICK_OUT)]
		public bool NtfKickOut(uint errcode)
		{
			return true;
		}

		/// <summary>
		/// 游戏结束
		/// </summary>
		/// <param name="result">是否胜利</param>
		[CustomProtocolID(protoid: (uint)EChinesechessProto.NTF_GAME_OVER)]
		public bool NtfGameOver(bool result)
		{
			return true;
		}
	}
}
