using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Client.Lobby
{

	/// <summary>
	/// 请求进入地图 【Client, Gate】
	/// </summary>
	[Message((uint)ELobbyProto.C2G_Req_EnterMap)]
	public class C2G_Req_EnterMap : AbstractRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 进入地图响应 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Res_EnterMap)]
	public class G2C_Res_EnterMap : AbstractResponse
	{
		public long UnitId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(UnitId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			UnitId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 请求房间列表 【Client, Gate】
	/// </summary>
	[Message((uint)ELobbyProto.C2G_Req_GetRoomList)]
	public class C2G_Req_GetRoomList : AbstractRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 返回房间列表 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Res_GetRoomList)]
	public class G2C_Res_GetRoomList : AbstractResponse
	{
		public List<CCRoomVO> Roomlist {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(Roomlist.Count);
			for (int i = 0; i < Roomlist.Count; i++)
			{
				Roomlist[i].Pack(writer);
			}
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			int len = reader.ReadInt32();
			Roomlist = new List<CCRoomVO>(len); 
			for (int i = 0; i < len; i++)
			{
				Roomlist.Add(new CCRoomVO());
				Roomlist[i].UnPack(reader);
			}
		}
	}

	/// <summary>
	/// 请求单个房间信息 【Client, Gate】
	/// </summary>
	[Message((uint)ELobbyProto.C2G_Req_GetRoomVO)]
	public class C2G_Req_GetRoomVO : AbstractRequest
	{
		public uint RoomId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(RoomId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			RoomId = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 返回单个房间信息 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Res_GetRoomVO)]
	public class G2C_Res_GetRoomVO : AbstractResponse
	{
		public CCRoomVO RoomVO {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			RoomVO.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			RoomVO.UnPack(reader);
		}
	}

	/// <summary>
	/// 请求创建房间 【Client, World】
	/// </summary>
	[Message((uint)ELobbyProto.C2W_Req_CreateRoom)]
	public class C2W_Req_CreateRoom : AbstractLocationRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 响应创建房间 【World, Client】
	/// </summary>
	[Message((uint)ELobbyProto.W2C_Res_CreateRoom)]
	public class W2C_Res_CreateRoom : AbstractLocationResponse
	{
		public CCRoomVO RoomVO {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			RoomVO.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			RoomVO.UnPack(reader);
		}
	}

	/// <summary>
	/// 请求退出房间 【Client, Gate】
	/// </summary>
	[Message((uint)ELobbyProto.C2G_Req_QuitRoom)]
	public class C2G_Req_QuitRoom : AbstractRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 响应退出房间 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Res_QuitRoom)]
	public class G2C_Res_QuitRoom : AbstractResponse
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 请求加入房间 【Client, Gate】
	/// </summary>
	[Message((uint)ELobbyProto.C2G_Req_Join)]
	public class C2G_Req_Join : AbstractRequest
	{
		public uint RoomId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(RoomId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			RoomId = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 响应加入房间 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Res_Join)]
	public class G2C_Res_Join : AbstractResponse
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 加入房间 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Ntf_Join)]
	public class G2C_Ntf_Join : AbstractMessage
	{
		public CCPlayerVO Applier {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			Applier.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			Applier.UnPack(reader);
		}
	}

	/// <summary>
	/// 处理加入请求 【Client, Gate】
	/// </summary>
	[Message((uint)ELobbyProto.C2G_Req_DealJoin)]
	public class C2G_Req_DealJoin : AbstractRequest
	{
		public bool IsAccept {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(IsAccept);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			IsAccept = reader.ReadBoolean();
		}
	}

	/// <summary>
	/// 处理加入请求 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Res_DealJoin)]
	public class G2C_Res_DealJoin : AbstractResponse
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 响应加入房间 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Ntf_DealJoin)]
	public class G2C_Ntf_DealJoin : AbstractMessage
	{
		public bool IsAccept {get; set;}
		public CCPlayerVO Accepter {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(IsAccept);
			Accepter.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			IsAccept = reader.ReadBoolean();
			Accepter.UnPack(reader);
		}
	}

	/// <summary>
	/// 开始比赛通知 【Gate, Client】
	/// </summary>
	[Message((uint)ELobbyProto.G2C_Ntf_StartMatch)]
	public class G2C_Ntf_StartMatch : AbstractMessage
	{
		public long StartTime {get; set;}
		public CCRoomVO RoomVO {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(StartTime);
			RoomVO.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			StartTime = reader.ReadInt64();
			RoomVO.UnPack(reader);
		}
	}
}
