﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public abstract class Entitas : ComponentWithId, IEntitas
    {
        protected Dictionary<Type, Component> m_component_dict = new Dictionary<Type, Component>();

        public Entitas() { }

        protected Entitas(long id) : base(id) { }

        public virtual T AddComponent<T>()
            where T : Component, new()
        {
            Type type = typeof(T);
            if (m_component_dict.ContainsKey(type) == true)
            {
                throw new Exception($"Entitas.Add, component already exist, id=id, component.name={type.Name}");
            }

            T component = Root.Factory.CreateWithParent<T>(this, IsFromPool);

            m_component_dict.Add(type, component);

            return component;
        }

        public virtual T AddComponent<T, P1>(P1 p1)
         where T : Component, new()
        {
            Type type = typeof(T);
            if (m_component_dict.ContainsKey(type) == true)
            {
                throw new Exception($"Entitas.Add, component already exist, id=id, component.name={type.Name}");
            }

            T component = Root.Factory.CreateWithParent<T, P1>(this, p1, IsFromPool);

            m_component_dict.Add(type, component);

            return component;
        }

        public virtual T AddComponent<T, P1, P2>(P1 p1, P2 p2)
         where T : Component, new()
        {
            Type type = typeof(T);
            if (m_component_dict.ContainsKey(type) == true)
            {
                throw new Exception($"Entitas.Add, component already exist, id=id, component.name={type.Name}");
            }

            T component = Root.Factory.CreateWithParent<T, P1, P2>(this, p1, p2, IsFromPool);

            m_component_dict.Add(type, component);

            return component;
        }

        public virtual T AddComponent<T, P1, P2, P3>(P1 p1, P2 p2, P3 p3)
         where T : Component, new()
        {
            Type type = typeof(T);
            if (m_component_dict.ContainsKey(type) == true)
            {
                throw new Exception($"Entitas.Add, component already exist, id=id, component.name={type.Name}");
            }

            T component = Root.Factory.CreateWithParent<T, P1, P2, P3>(this, p1, p2, p3, IsFromPool);

            m_component_dict.Add(type, component);

            return component;
        }

        public virtual void RemoveComponent<T>()
            where T : Component
        {
            if (IsDisposed == true)
            {
                return;
            }
            Type type = typeof(T);
            if (m_component_dict.TryGetValue(type, out Component component) == false)
            {
                return;
            }

            m_component_dict.Remove(type);

            component.Dispose();
        }

        public T GetComponent<T>()
            where T : Component
        {
            if (m_component_dict.TryGetValue(typeof(T), out Component component) == false)
            {
                return default;
            }
            return (T)component;
        }

        public T GetComponentInHerit<T>()
            where T : class
        {
            foreach (var pair in m_component_dict)
            {
                if (pair.Value is T)
                {
                    return pair.Value as T;
                }
            }
            return default;
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            foreach (var pair in m_component_dict)
            {
                pair.Value.Dispose();
            }
            m_component_dict.Clear();
            base.Dispose();
        }
    }
}
