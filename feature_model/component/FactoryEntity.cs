﻿using ETT.Model.component;
using System;

namespace ETT.Model
{
    public interface IRootComponent : IEntitas
    {
        int RootId { get; set; }

        FactoryEntity Factory { get; }
    }

    public class FactoryEntity
    {
        private static readonly ComponentPool m_pool = new ComponentPool();

        private readonly IRootComponent Root;
        public FactoryEntity(IRootComponent root)
        {
            Root = root;
        }

        private T CreateInstance<T>(bool fromPool = true)
            where T : Component
        {
            T component;
            if (fromPool)
            {
                component = m_pool.Fetch<T>();
            }
            else
            {
                component = Activator.CreateInstance<T>();
            }

            component.InstanceId = SerialUtils.GenerateInstanceId(Root.RootId);

            Context.Event.Add(component);

            return component;
        }

        public T CreateWithParent<T>(Component parent, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Parent = parent;
            component.Root = Root;

            Context.Event.Awake(component);

            return component;
        }

        public T CreateWithParent<T, A>(Component parent, A a, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Parent = parent;
            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component, a);

            return component;
        }

        public T CreateWithParent<T, A, B>(Component parent, A a, B b, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Parent = parent;
            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component, a, b);

            return component;
        }

        public T CreateWithParent<T, A, B, C>(Component parent, A a, B b, C c, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Parent = parent;
            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component, a, b, c);

            return component;
        }

        public T Create<T>(bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component);

            return component;
        }

        public T Create<T, A>(A a, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component, a);

            return component;
        }

        public T Create<T, A, B>(A a, B b, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component, a, b);

            return component;
        }

        public T Create<T, A, B, C>(A a, B b, C c, bool fromPool = true)
            where T : Component
        {
            T component = CreateInstance<T>(fromPool);

            component.Root = Root;
            if (component is ComponentWithId componentWithId)
            {
                componentWithId.Id = component.InstanceId;
            }

            Context.Event.Awake(component, a, b, c);

            return component;
        }

        public T CreateWithId<T>(long id, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = id;
            component.Root = Root;

            Context.Event.Awake(component);

            return component;
        }

        public T CreateWithId<T, A>(long id, A a, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = id;
            component.Root = Root;

            Context.Event.Awake(component, a);

            return component;
        }

        public T CreateWithId<T, A, B>(long id, A a, B b, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = id;
            component.Root = Root;

            Context.Event.Awake(component, a, b);

            return component;
        }

        public T CreateWithId<T, A, B, C>(long id, A a, B b, C c, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = id;
            component.Root = Root;

            Context.Event.Awake(component, a, b, c);

            return component;
        }

        public T CreateWithId<T>(bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = SerialUtils.GenerateId(Root.RootId);
            component.Root = Root;

            Context.Event.Awake(component);

            return component;
        }

        public T CreateWithId<T, A>(A a, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = SerialUtils.GenerateId(Root.RootId);
            component.Root = Root;

            Context.Event.Awake(component, a);

            return component;
        }

        public T CreateWithId<T, A, B>(A a, B b, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = SerialUtils.GenerateId(Root.RootId);
            component.Root = Root;

            Context.Event.Awake(component, a, b);

            return component;
        }

        public T CreateWithId<T, A, B, C>(A a, B b, C c, bool fromPool = true)
            where T : ComponentWithId
        {
            T component = CreateInstance<T>(fromPool);

            component.Id = SerialUtils.GenerateId(Root.RootId);
            component.Root = Root;

            Context.Event.Awake(component, a, b, c);

            return component;
        }

        public void Recycle(Component obj)
        {
            m_pool.Recycle(obj);
        }
    }
}
