﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.component
{
    public class ComponentPool
    {
        private readonly Dictionary<Type, Queue<Component>> m_dict = new Dictionary<Type, Queue<Component>>();

        public Component Fetch(Type type)
        {
            Component obj;
            if (m_dict.TryGetValue(type, out Queue<Component> queue) == false)
            {
                obj = (Component)Activator.CreateInstance(type);
            }
            else if (queue.Count == 0)
            {
                obj = (Component)Activator.CreateInstance(type);
            }
            else
            {
                obj = queue.Dequeue();
            }

            obj.IsFromPool = true;
            return obj;
        }

        public T Fetch<T>() where T : Component
        {
            T t = (T)Fetch(typeof(T));
            return t;
        }

        public void Recycle(Component obj)
        {

            Type type = obj.GetType();
            if (m_dict.TryGetValue(type, out Queue<Component> queue) == false)
            {
                queue = new Queue<Component>();
                m_dict.Add(type, queue);
            }
            queue.Enqueue(obj);
        }

        public void Clear()
        {
            foreach (var pair in m_dict)
            {
                var queue = pair.Value;
                while (queue.Count > 0)
                {
                    var obj = queue.Dequeue();
                    obj.IsFromPool = false;
                    obj.Dispose();
                }
            }
            m_dict.Clear();
        }
    }
}
