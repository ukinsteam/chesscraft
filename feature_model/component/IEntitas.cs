﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IEntitas
    {
        T AddComponent<T>() where T : Component, new();
        T AddComponent<T, P1>(P1 p1) where T : Component, new();
        T AddComponent<T, P1, P2>(P1 p1, P2 p2) where T : Component, new();
        T AddComponent<T, P1, P2, P3>(P1 p1, P2 p2, P3 p3) where T : Component, new();
        void RemoveComponent<T>() where T : Component;
        T GetComponent<T>() where T : Component;
    }
}
