﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model
{
    /// <summary>
    /// 后续将协议与配置表的自定义结构统一实现一个序列化接口
    /// </summary>
    interface InterfaceSerialize
    {
        bool Serialize(BinaryWriter writer);

        bool Unserialize(BinaryReader reader);
    }
}
