﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.mailbox
{
    public class MailBoxHandlerAttribute : BaseAttribute
    {
        public EMailBoxType EMailBoxType { get; }

        public MailBoxHandlerAttribute(EMailBoxType eMailBoxType)
        {
            EMailBoxType = eMailBoxType;
        }
    }
}
