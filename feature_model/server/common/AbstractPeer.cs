﻿namespace ETT.Model.server
{
    public abstract class AbstractPeer : Entitas, IAbstractPeer, IRootComponent
    {
        public abstract EPeerType PeerType { get; }

        public virtual int RootId { get; set; }

        public virtual string InnerAddr { get; protected set; }

        public FactoryEntity Factory { get; }

        public AbstractPeer()
        {
            Factory = new FactoryEntity(this);
        }

    }
}
