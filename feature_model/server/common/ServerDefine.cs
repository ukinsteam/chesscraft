﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ETT.Model.server
{
    [Flags]
    public enum EPeerType
    {
        Master = 1,
        Database = 1 << 1,
        World = 1 << 2,
        Gate = 1 << 3,
        Login = 1 << 4,
        Location = 1 << 5,

        Client = 1 << 30,
    }

    public enum EErrCode
    {
        Success = 0,
        NotFoundActor = 100000,
        ActorNoMailBoxComponent = 100001,
    }

    public static class ServerDefine
    {
        public static IPEndPoint ToIPEndPoint(string address)
        {
            int index = address.LastIndexOf(':');
            string host = address.Substring(0, index);
            string p = address.Substring(index + 1);
            int port = int.Parse(p);
            return ToIPEndPoint(host, port);
        }
        public static IPEndPoint ToIPEndPoint(string host, int port)
        {
            return new IPEndPoint(IPAddress.Parse(host), port);
        }
    }
}
