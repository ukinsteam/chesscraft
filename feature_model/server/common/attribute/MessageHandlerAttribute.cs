﻿using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class MessageHandlerAttribute : BaseAttribute
    {
        public EPeerType ActorType { get; }

        public MessageHandlerAttribute(EPeerType eActor)
        {
            ActorType = eActor;
        }
    }
}
