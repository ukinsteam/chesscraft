﻿using ETT.Model.network;
using ETT.Model.proto;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.server
{
    public class ActorMessageDispatchComponent : Component, IAwake
    {
        private readonly Dictionary<Type, IMActorHandler> m_handler_dict = new Dictionary<Type, IMActorHandler>();

        public void Awake()
        {
            m_handler_dict.Clear();

            List<Type> types = Context.Event.GetAttrTypes(typeof(ActorMessageHandlerAttribute));

            var peer = Root as IAbstractPeer;

            foreach (Type type in types)
            {
                object[] attrs = type.GetCustomAttributes(typeof(ActorMessageHandlerAttribute), false);
                if (attrs.Length == 0)
                {
                    continue;
                }

                ActorMessageHandlerAttribute messageHandlerAttribute = attrs[0] as ActorMessageHandlerAttribute;
                if ((messageHandlerAttribute.ActorType & peer.PeerType) != peer.PeerType)
                {
                    continue;
                }

                IMActorHandler imHandler = Activator.CreateInstance(type) as IMActorHandler;
                if (imHandler == null)
                {
                    Log.Error($"message handle {type.Name} 需要继承 IMActorHandler");
                    continue;
                }

                Type messageType = imHandler.GetMessageType();
                //uint proto_id = Context.Game.GetComponent<ProtoComponent>().GetProtoId(messageType);
                //if (proto_id == 0)
                //{
                //    Log.Error($"协议Id不能为0:{messageType.Name}");
                //    continue;
                //}

                //if (m_handler_dict.ContainsKey(proto_id) == true)
                //{
                //    Log.Error($"一条协议不能对应多个处理方法，proto_id = {proto_id}");
                //    continue;
                //}
                m_handler_dict[messageType] = imHandler;
            }
        }

        public async ETTask Handle(Session session, Entitas entity, IProtoPacker message)
        {
            if (m_handler_dict.TryGetValue(message.GetType(), out IMActorHandler handler) == false)
            {
                Log.Error($"协议没有对应的处理方法：{message.GetType()},{message.ToJson()}");
                return;
            }

            await handler.Handle(session, entity, message);
        }
    }
}
