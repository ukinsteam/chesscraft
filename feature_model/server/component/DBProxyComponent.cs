﻿
namespace ETT.Model.server
{
    public class DBProxyComponent : Component, IStart
    {
        ////后续需要处理db服务器因宕机等原因导致的失效问题
        //private IPEndPoint m_db_addr;

        public void Start()
        {
            Log.Debug($"Start {TimeUtils.Now()}");
            GetDBActor();
        }

        private async void GetDBActor()
        {
            var time_comp = Context.Game.GetComponent<TimeComponent>();

            for (int i = 0; i < 3; ++i)
            {
                await time_comp.WaitAsync(200);

                //    Log.Debug($"await {TimeUtils.Now()}");

                //    var route = Entitas.GetComponent<RouteComponent>();
                //    if (route == null)
                //    {
                //        return;
                //    }
                //    m_db_addr = route.GetAddr(EActorType.Database);
                //    if (m_db_addr != null)
                //    {
                //        Log.Debug("found");
                //        break;
                //    }
            }

            //if (m_db_addr == null)
            //{
            //    throw new Exception("DB Addr Is Not Exist");
            //}
        }
    }
}
