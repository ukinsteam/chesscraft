﻿using ETT.Model.network;
using ETT.Model.proto;
using System;
using System.Collections.Generic;

namespace ETT.Model.server
{
    public class MessageDispatchComponent : Component, IAwake
    {
        private readonly Dictionary<uint, IMHandler> m_handler_dict = new Dictionary<uint, IMHandler>();

        public void Awake()
        {
            m_handler_dict.Clear();

            List<Type> types = Context.Event.GetAttrTypes(typeof(MessageHandlerAttribute));

            var peer = Root as IAbstractPeer;

            foreach (Type type in types)
            {
                object[] attrs = type.GetCustomAttributes(typeof(MessageHandlerAttribute), false);
                if (attrs.Length == 0)
                {
                    continue;
                }

                MessageHandlerAttribute messageHandlerAttribute = attrs[0] as MessageHandlerAttribute;
                if (peer != null)
                {
                    if ((messageHandlerAttribute.ActorType & peer.PeerType) != peer.PeerType)
                    {
                        continue;
                    }
                }

                IMHandler imHandler = Activator.CreateInstance(type) as IMHandler;
                if (imHandler == null)
                {
                    Log.Error($"message handle {type.Name} 需要继承 IMHandler");
                    continue;
                }

                Type messageType = imHandler.GetMessageType();
                uint proto_id = Context.Game.GetComponent<ProtoComponent>().GetProtoId(messageType);
                if (proto_id == 0)
                {
                    Log.Error($"协议Id不能为0:{messageType.Name}");
                    continue;
                }

                if (m_handler_dict.ContainsKey(proto_id) == true)
                {
                    //m_handler_dict.Add(proto_id, new List<IMHandler>());
                    Log.Error($"一条协议不能对应多个处理方法，proto_id = {proto_id}");
                    continue;
                }
                m_handler_dict[proto_id] = imHandler;
            }
        }

        public void Handle(Session session, uint proto_id, IProtoPacker message)
        {
            if (m_handler_dict.TryGetValue(proto_id, out IMHandler handler) == false)
            {
                Log.Error($"协议没有对应的处理方法：{proto_id},{message.ToJson()}");
                return;
            }

            try
            {
                handler.Handle(session, message);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
