﻿using System.Collections.Generic;

namespace ETT.Model
{
    public class TimeComponent : Component, IAwake, IUpdate
    {
        private struct Node
        {
            public long Id { get; set; }
            public long Time { get; set; }
            public ETTaskCompletionSource<object> tcs { get; set; }
        }

        private static long m_generator_id = 0;

        private SortedDictionary<long, Node> m_dict = new SortedDictionary<long, Node>();
        private Queue<Node> m_queue = new Queue<Node>();
        private long m_min_time;

        public void Awake()
        {
            Log.Debug("TimeComponent Awake");
        }

        public void Update()
        {
            if (m_dict.Count == 0)
            {
                return;
            }
            long time_now = TimeUtils.Now();
            if (time_now < m_min_time)
            {
                return;
            }
            foreach (var pair in m_dict)
            {
                if (pair.Value.Time > time_now)
                {
                    m_min_time = pair.Value.Time;
                    break;
                }
                m_queue.Enqueue(pair.Value);
            }

            while (m_queue.Count > 0)
            {
                var node = m_queue.Dequeue();
                m_dict.Remove(node.Id);
                node.tcs.SetResult(null);
            }
        }

        public ETTask WaitAsync(long time)
        {
            ETTaskCompletionSource<object> tcs = new ETTaskCompletionSource<object>();
            Node timer = new Node { Id = ++m_generator_id, Time = TimeUtils.Now() + time, tcs = tcs };
            m_dict[timer.Id] = timer;
            if (timer.Time < m_min_time)
            {
                m_min_time = timer.Time;
            }
            return tcs.Task;
        }

    }
}
