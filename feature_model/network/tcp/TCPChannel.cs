﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ETT.Model.network
{
    public sealed class TCPChannel : AbstractChannel,
        IAwake<Socket, TCPService>, IAwake<IPEndPoint, TCPService>
    {
        private Socket m_socket;
        private SocketAsyncEventArgs m_inn_args = new SocketAsyncEventArgs();
        private SocketAsyncEventArgs m_out_args = new SocketAsyncEventArgs();

        private readonly CircularBuffer m_recv_buffer = new CircularBuffer();
        private readonly CircularBuffer m_send_buffer = new CircularBuffer();

        private PacketParser m_parser;

        public bool IsSending { get; private set; }
        public bool IsRecving { get; private set; }
        public bool IsConnected { get; private set; }

        private readonly byte[] m_packet_size_cache = new byte[NetworkDefine.PacketSizeLength];

        private TCPService m_service;

        public void Awake(IPEndPoint ipEndPoint, TCPService service)
        {
            base.Awake(service, EChannelType.Connect);

            m_service = service;
            Stream = service.MemoryStreamManager.GetStream("message", ushort.MaxValue);
            m_socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_socket.NoDelay = true;
            m_parser = new PacketParser(m_recv_buffer, Stream);

            m_inn_args.Completed += OnCompleted;
            m_out_args.Completed += OnCompleted;

            RemoteAddress = ipEndPoint;

            IsConnected = false;
            IsSending = false;
        }

        public void Awake(Socket socket, TCPService service)
        {
            base.Awake(service, EChannelType.Connect);

            m_service = service;
            Stream = service.MemoryStreamManager.GetStream("message", ushort.MaxValue);

            this.m_socket = socket;
            this.m_socket.NoDelay = true;
            m_parser = new PacketParser(m_recv_buffer, Stream);

            m_inn_args.Completed += OnCompleted;
            m_out_args.Completed += OnCompleted;

            RemoteAddress = (IPEndPoint)socket.RemoteEndPoint;

            IsConnected = true;
            IsSending = false;
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            m_socket.Close();
            m_socket = null;

            m_inn_args.Dispose();
            m_out_args.Dispose();
            m_inn_args = null;
            m_out_args = null;

            Stream.Dispose();
        }

        public override void Start()
        {
            if (IsConnected == false)
            {
                ConnectAsync(RemoteAddress);
                return;
            }

            if (IsRecving == false)
            {
                IsRecving = true;
                StartRecv();
            }
            m_service.MarkNeedStartSend(Id);
        }

        public override void Send(MemoryStream stream)
        {
            if (stream.Length > ushort.MaxValue)
            {
                throw new Exception($"send packet too large:{stream.Length}");
            }
            m_packet_size_cache.WriteTo(0, (int)stream.Length);

            m_send_buffer.Write(m_packet_size_cache, 0, m_packet_size_cache.Length);
            m_send_buffer.Write(stream);

            m_service.MarkNeedStartSend(Id);
        }

        private void ConnectAsync(IPEndPoint ipEndPoint)
        {
            m_out_args.RemoteEndPoint = ipEndPoint;
            if (m_socket.ConnectAsync(m_out_args) == true)
            {
                return;
            }
            OnConnectComplete(m_out_args);
        }

        public void BeginSend()
        {
            if (IsConnected == false)
            {
                return;
            }
            if (m_send_buffer.Length == 0)
            {
                IsSending = false;
                return;
            }

            IsSending = true;

            int size = m_send_buffer.ChunkSize - m_send_buffer.FirstIndex;
            if (size > m_send_buffer.Length)
            {
                size = (int)m_send_buffer.Length;
            }
            SendAsync(m_send_buffer.First, m_send_buffer.FirstIndex, size);
        }


        private void SendAsync(byte[] buffer, int offset, int count)
        {
            try
            {
                m_out_args.SetBuffer(buffer, offset, count);
            }
            catch (Exception e)
            {
                throw new Exception($"socket set buffer error:{buffer.Length},{offset},{count}", e);
            }

            if (m_socket.SendAsync(m_out_args) == true)
            {
                return;
            }
            OnSendComplete(m_out_args);

        }

        private void StartRecv()
        {
            int size = m_recv_buffer.ChunkSize - m_recv_buffer.LastIndex;
            RecvAsync(m_recv_buffer.Last, m_recv_buffer.LastIndex, size);
        }

        private void RecvAsync(byte[] buffer, int offset, int count)
        {
            try
            {
                m_inn_args.SetBuffer(buffer, offset, count);
            }
            catch (Exception e)
            {
                throw new Exception($"socket set buffer errr:{buffer.Length},{offset},{count}", e);
            }
            if (m_socket.ReceiveAsync(m_inn_args) == true)
            {
                return;
            }
            OnRecvComplete(m_inn_args);
        }

        //Event Handler
        private void OnCompleted(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Connect:
                    ThreadSync.Instance.Post(this.OnConnectComplete, e);
                    break;
                case SocketAsyncOperation.Receive:
                    ThreadSync.Instance.Post(this.OnRecvComplete, e);
                    break;
                case SocketAsyncOperation.Send:
                    ThreadSync.Instance.Post(this.OnSendComplete, e);
                    break;
                case SocketAsyncOperation.Disconnect:
                    ThreadSync.Instance.Post(this.OnDisconnectComplete, e);
                    break;
                default:
                    throw new Exception($"socket error:{e.LastOperation}");
            }
        }

        private void OnConnectComplete(object o)
        {
            if (m_socket == null)
            {
                return;
            }
            SocketAsyncEventArgs e = (SocketAsyncEventArgs)o;
            if (e.SocketError != SocketError.Success)
            {
                OnError((int)e.SocketError);
                return;
            }

            e.RemoteEndPoint = null;
            IsConnected = true;

            Start();
        }

        private void OnRecvComplete(object o)
        {
            if (m_socket == null)
            {
                return;
            }
            SocketAsyncEventArgs e = (SocketAsyncEventArgs)o;
            if (e.SocketError != SocketError.Success)
            {
                OnError((int)e.SocketError);
                return;
            }
            if (e.BytesTransferred == 0)
            {
                OnError(ErrorCode.ERR_PeerDisconnect);
                return;
            }

            m_recv_buffer.LastIndex += e.BytesTransferred;
            if (m_recv_buffer.LastIndex == m_recv_buffer.ChunkSize)
            {
                m_recv_buffer.AddLast();
                m_recv_buffer.LastIndex = 0;
            }

            while (true)
            {
                try
                {
                    if (m_parser.Parse() == false)
                    {
                        break;
                    }
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                    OnError(ErrorCode.ERR_SocketError);
                    return;
                }

                try
                {
                    OnRead(m_parser.GetPacket());
                }
                catch (Exception ee)
                {
                    Log.Error(ee);
                }
            }

            if (m_socket == null)
            {
                return;
            }

            StartRecv();
        }

        private void OnSendComplete(object o)
        {
            if (m_socket == null)
            {
                return;
            }

            SocketAsyncEventArgs e = (SocketAsyncEventArgs)o;

            if (e.SocketError != SocketError.Success)
            {
                OnError((int)e.SocketError);
                return;
            }

            if (e.BytesTransferred == 0)
            {
                OnError(ErrorCode.ERR_PeerDisconnect);
                return;
            }

            m_send_buffer.FirstIndex += e.BytesTransferred;
            if (m_send_buffer.FirstIndex == m_send_buffer.ChunkSize)
            {
                m_send_buffer.FirstIndex = 0;
                m_send_buffer.RemoveFirst();
            }

            BeginSend();
        }

        private void OnDisconnectComplete(object o)
        {
            SocketAsyncEventArgs e = (SocketAsyncEventArgs)o;
            OnError((int)e.SocketError);
        }

    }
}
