﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model.network
{
    public class ActorResponse : IActorLocationResponse
    {
        public int Error { get; set; }
        public string Message { get; set; }
        public int RpcId { get; set; }

        public void Pack(BinaryWriter writer)
        {
            writer.Write(Error);
            writer.Write(Message);
            writer.Write(RpcId);
        }

        public void UnPack(BinaryReader reader)
        {
            Error = reader.ReadInt32();
            Message = reader.ReadString();
            RpcId = reader.ReadInt32();
        }

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
