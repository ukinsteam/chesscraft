﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IMessage : IProtoPacker
    {
    }

    public interface IRequest : IProtoPacker
    {
        int RpcId { get; set; }
    }

    public interface IResponse : IProtoPacker
    {
        int Error { get; set; }
        string Message { get; set; }
        int RpcId { get; set; }
    }
    
}
