﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model
{
    public interface IProtoPacker
    {

        void Pack(BinaryWriter writer);

        void UnPack(BinaryReader reader);

        string ToJson();
    }
}
