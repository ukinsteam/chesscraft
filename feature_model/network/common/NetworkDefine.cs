﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.network
{
    public static class ErrorCode
    {
        public const int ERR_Success = 0;

        // 1-11004 是SocketError请看SocketError定义
        //-----------------------------------
        // 100000 以上，避免跟SocketError冲突
        public const int ERR_MyErrorCode = 100000;

        public const int ERR_PacketParserError = 100005;

        public const int ERR_PeerDisconnect = 102008;

        public const int ERR_SocketError = 102010;
    }

    public enum ENetworkProtocol
    {
        TCP,
        KCP,
        ENet,
        WebSocket,
    }

    public static class NetworkDefine
    {
        public static readonly int PacketSizeLength = 4;
        public static readonly int ProtoIndexLength = 4;
    }
}
