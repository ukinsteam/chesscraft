﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ETT.Model.network
{
    public interface IAbstractChannel
    {
        int Error { get; set; }

        IPEndPoint RemoteAddress { get;  }

        MemoryStream Stream { get; }

        void Start();

        void Send(MemoryStream stream);

        void Dispose();
    }
}
