﻿using ETT.Model.mailbox;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Net;

namespace ETT.Model.network
{
    //管理内网【服务器】与【服务器】之间的连接
    public sealed class NetworkInnerComponent : NetworkComponent,
        IAwake<ENetworkProtocol, string>,
        IAwake<string>
    {
        private readonly Dictionary<IPEndPoint, Session> m_address_dict = new Dictionary<IPEndPoint, Session>();

        public void Awake(string address)
        {
            Awake(ENetworkProtocol.TCP, address);
        }

        public void Awake(ENetworkProtocol protocol, string address)
        {
            EProtocol = protocol;

            try
            {
                IPEndPoint ipEndPoint = NetworkUtils.ToIPEndPoint(address);

                switch (protocol)
                {
                    case ENetworkProtocol.TCP:
                        //Service = new TCPService(ipEndPoint, OnAccept);
                        Service = Root.Factory.Create<TCPService, IPEndPoint, Action<AbstractChannel>>(ipEndPoint, OnAccept);
                        break;
                    case ENetworkProtocol.KCP:

                        break;
                    case ENetworkProtocol.ENet:

                        break;
                    case ENetworkProtocol.WebSocket:

                        break;
                    default: break;
                }
            }
            catch (Exception e)
            {
                throw new Exception($"NetworkComponent Awake Error {address}", e);
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            m_address_dict.Clear();
        }

        private void OnAccept(AbstractChannel channel)
        {
            Session session = Root.Factory.CreateWithId<Session, AbstractChannel>(channel);
            session.Parent = this;
            m_session_dict.Add(session.Id, session);
            m_address_dict.Add(session.RemoteAddress, session);
            session.Start();
        }

        public Session Create(IPEndPoint ip_end_point)
        {
            AbstractChannel channel = Service.ConnectChannel(ip_end_point);
            Session session = Root.Factory.CreateWithId<Session, AbstractChannel>(channel);
            session.Parent = this;
            m_session_dict.Add(session.Id, session);
            m_address_dict.Add(session.RemoteAddress, session);
            session.Start();
            return session;
        }

        public Session Create(string address)
        {
            IPEndPoint ip_end_point = NetworkUtils.ToIPEndPoint(address);
            return Create(ip_end_point);
        }

        public Session Get(IPEndPoint ip_end_point)
        {
            if (m_address_dict.TryGetValue(ip_end_point, out Session session) == true)
            {
                return session;
            }

            session = Create(ip_end_point);
            return session;
        }

        public override void Remove(long id)
        {
            Session session = Get(id);
            if (session == null)
            {
                return;
            }
            m_address_dict.Remove(session.RemoteAddress);
            base.Remove(id);
        }

        public void SendAll(IMessage message)
        {
            foreach (var pair in m_address_dict)
            {
                pair.Value.Send(message);
            }
        }

        public override void Dispatch(Session session, uint proto_id, IProtoPacker message)
        {
            switch (message)
            {
                case IActorRequest actorRequest:
                    {
                        HandleIActorRequest(session, actorRequest).Coroutine();
                    }
                    break;
                case IActorMessage actorMessage:
                    {
                        HandleIActorMessage(session, actorMessage).Coroutine();
                    }
                    break;
                default:
                    var dispatcher = Entitas.GetComponent<MessageDispatchComponent>();
                    dispatcher.Handle(session, proto_id, message);
                    break;
            }
        }

        private async ETVoid HandleIActorRequest(Session session, IActorRequest message)
        {
            using (await Context.Game.GetComponent<CoroutineLockComponent>().Wait(message.ActorId))
            {
                Entitas entity = (Entitas)Context.Event.Get(message.ActorId);
                if (entity == null)
                {
                    Log.Warning($"not found actor: {message}");
                    ActorResponse response = new ActorResponse
                    {
                        Error = (int)EErrCode.NotFoundActor,
                        RpcId = message.RpcId
                    };
                    session.Reply(response);
                    return;
                }

                MailBoxComponent mailBoxComponent = entity.GetComponent<MailBoxComponent>();
                if (mailBoxComponent == null)
                {
                    ActorResponse response = new ActorResponse
                    {
                        Error = (int)EErrCode.ActorNoMailBoxComponent,
                        RpcId = message.RpcId
                    };
                    session.Reply(response);
                    Log.Error($"actor not add MailBoxComponent: {entity.GetType().Name} {message}");
                    return;
                }

                await mailBoxComponent.Add(session, message);
            }
        }

        private async ETVoid HandleIActorMessage(Session session, IActorMessage message)
        {
            using (await Context.Game.GetComponent<CoroutineLockComponent>().Wait(message.ActorId))
            {
                Entitas entity = (Entitas)Context.Event.Get(message.ActorId);
                if (entity == null)
                {
                    Log.Error($"not found actor: {message}");
                    return;
                }

                MailBoxComponent mailBoxComponent = entity.GetComponent<MailBoxComponent>();
                if (mailBoxComponent == null)
                {
                    Log.Error($"actor not add MailBoxComponent: {entity.GetType().Name} {message}");
                    return;
                }
                await mailBoxComponent.Add(session, message);
            }
        }
    }
}
