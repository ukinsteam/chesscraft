﻿using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Net;

namespace ETT.Model.network
{
    //管理【客户端】与【服务器】的连接
    public class NetworkOuterComponent : NetworkComponent,
        IAwake<ENetworkProtocol, string>, IAwake<string>,
        IAwake<ENetworkProtocol>, IAwake
    {
        private readonly Dictionary<IPEndPoint, Session> m_address_dict = new Dictionary<IPEndPoint, Session>();

        public IPEndPoint IPEndPoint { get; private set; }

        public void Awake(ENetworkProtocol protocol)
        {
            try
            {
                switch (protocol)
                {
                    case ENetworkProtocol.TCP:
                        Service = Root.Factory.CreateWithParent<TCPService>(this);
                        break;
                    case ENetworkProtocol.KCP:

                        break;
                    case ENetworkProtocol.ENet:

                        break;
                    case ENetworkProtocol.WebSocket:

                        break;
                    default: break;
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }

            Log.Debug("NetworkOuterComponent Awake");
        }
        public void Awake()
        {
            Awake(ENetworkProtocol.TCP);
        }

        public void Awake(string address)
        {
            Awake(ENetworkProtocol.TCP, address);
        }

        public void Awake(ENetworkProtocol protocol, string address)
        {
            EProtocol = protocol;

            try
            {
                IPEndPoint = NetworkUtils.ToIPEndPoint(address);

                switch (protocol)
                {
                    case ENetworkProtocol.TCP:
                        Service = Root.Factory.CreateWithParent<TCPService, IPEndPoint, Action<AbstractChannel>>(this, IPEndPoint, OnAccept);
                        break;
                    case ENetworkProtocol.KCP:

                        break;
                    case ENetworkProtocol.ENet:

                        break;
                    case ENetworkProtocol.WebSocket:

                        break;
                    default: break;
                }
            }
            catch (Exception e)
            {
                throw new Exception($"NetworkComponent Awake Error {address}", e);
            }

            Log.Debug("NetworkOuterComponent Awake");
        }

        private void OnAccept(AbstractChannel channel)
        {
            Session session = Root.Factory.CreateWithParent<Session, AbstractChannel>(this, channel);
            m_session_dict.Add(session.Id, session);
            m_address_dict.Add(session.RemoteAddress, session);
            session.Start();
        }


        public Session Get(IPEndPoint ip_end_point)
        {
            if (m_address_dict.TryGetValue(ip_end_point, out Session session) == true)
            {
                return session;
            }

            //不自动创建
            return null;
        }

        public Session Create(IPEndPoint ip_end_point, bool auto_start = true)
        {
            AbstractChannel channel = Service.ConnectChannel(ip_end_point);
            Session session = Root.Factory.CreateWithParent<Session, AbstractChannel>(this, channel);
            m_session_dict.Add(session.Id, session);
            m_address_dict.Add(session.RemoteAddress, session);
            if (auto_start == true)
            {
                session.Start();
            }
            return session;
        }

        public Session Create(string address)
        {
            IPEndPoint ip_end_point = NetworkUtils.ToIPEndPoint(address);
            return Create(ip_end_point);
        }

        public override void Dispatch(Session session, uint proto_id, IProtoPacker message)
        {
            var dispatcher = Entitas.GetComponent<MessageDispatchComponent>();
            dispatcher.Handle(session, proto_id, message);
        }
    }
}