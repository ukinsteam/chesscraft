﻿using ETT.Model.network;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.proto
{
    public interface IMActorHandler
    {
        ETTask Handle(Session session, Entitas entity, IProtoPacker message);
        Type GetMessageType();
    }
}
