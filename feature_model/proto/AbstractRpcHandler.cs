﻿using ETT.Model.network;
using System;

namespace ETT.Model.proto
{
    public abstract class AbstractRpcHandler<Request, Response> : IMHandler
        where Request : class, IRequest
        where Response : class, IResponse
    {
        public Type GetMessageType()
        {
            return typeof(Request);
        }

        public async ETVoid Handle(Session session, IProtoPacker message)
        {
            Request request = message as Request;
            if (request == null)
            {
                Log.Error($"消息类型转换错误：{message.GetType().Name} to {GetMessageType().Name}");
                return;
            }

            int rpc_id = request.RpcId;
            long session_id = session.InstanceId;
            Response response = Activator.CreateInstance<Response>();
            void Reply()
            {
                //等回调回来，session可能已经断开了，所以需要判断session.id是否一样
                if (session.InstanceId != session_id)
                {
                    return;
                }
                response.RpcId = rpc_id;
                session.Reply(response);
            }

            try
            {
                await this.Run(session, request, response, Reply);
            }
            catch (Exception e)
            {
                Log.Error(e);
                response.Error = (int)102001;
                response.Message = e.ToString();
                Reply();
            }
        }

        protected abstract ETTask Run(Session session, Request request, Response response, Action reply);
    }
}
