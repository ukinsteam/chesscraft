﻿using ETT.Model.network;
using System;

namespace ETT.Model.proto
{
    public abstract class AbstractMsgHandler<Message> : IMHandler
        where Message : class
    {
        public Type GetMessageType()
        {
            return typeof(Message);
        }

        public async ETVoid Handle(Session session, IProtoPacker message)
        {
            Message msg = message as Message;
            if (msg == null)
            {
                Log.Error($"消息类型转换错误：{message.GetType().Name} to {GetMessageType().Name}");
                return;
            }
            if (session.IsDisposed)
            {
                Log.Error($"session disconnet {msg}");
                return;
            }
            await this.Run(session, msg);
        }

        protected abstract ETTask Run(Session session, Message msg);
    }
}
