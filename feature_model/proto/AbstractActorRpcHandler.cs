﻿using System;
using System.Collections.Generic;
using System.Text;
using ETT.Model.network;

namespace ETT.Model.proto
{
    public abstract class AbstractActorRpcHandler<E, Request, Response> : IMActorHandler
        where E : Entitas
        where Request : class, IActorRequest
        where Response : class, IActorResponse
    {
        public Type GetMessageType()
        {
            return typeof(Request);
        }

        public async ETTask Handle(Session session, Entitas entity, IProtoPacker message)
        {
            try
            {
                Request request = message as Request;
                if (request == null)
                {
                    Log.Error($"消息类型转换错误：{message.GetType().FullName} to {typeof(Request).Name}");
                    return;
                }

                E e = entity as E;
                if (e == null)
                {
                    Log.Error($"Actor类型转换错误：{entity.GetType().Name} to {typeof(E).Name}");
                    return;
                }

                int rpc_id = request.RpcId;
                long session_id = session.InstanceId;
                Response response = Activator.CreateInstance<Response>();

                void Reply()
                {
                    if (session.InstanceId != session_id)
                    {
                        return;
                    }
                    response.RpcId = rpc_id;
                    session.Reply(response);
                }

                try
                {
                    await this.Run(e, request, response, Reply);
                }
                catch (Exception exception)
                {
                    Log.Error(exception);
                    response.Error = 102001;
                    response.Message = exception.ToString();
                    Reply();
                }
            }
            catch (Exception e)
            {
                Log.Error($"消息解析失败：{message.GetType().FullName}\n{e}");
            }
        }

        protected abstract ETTask Run(E unit, Request request, Response response, Action reply);
    }
}
