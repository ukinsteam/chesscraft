﻿using System;
using System.Collections.Generic;
using System.Text;
using ETT.Model.network;

namespace ETT.Model.proto
{
    public abstract class AbstractActorMsgHandler<E, Message> : IMActorHandler
        where E : Entitas
        where Message : class, IActorLocationMessage
    {
        public Type GetMessageType()
        {
            return typeof(Message);
        }

        public async ETTask Handle(Session session, Entitas entity, IProtoPacker message)
        {
            Message msg = message as Message;
            if (msg == null)
            {
                Log.Error($"消息类型转换错误：{message.GetType().FullName} to {typeof(Message).Name}");
                return;
            }

            E e = entity as E;
            if (e == null)
            {
                Log.Error($"Actor类型转换错误: {entity.GetType().Name} to {typeof(E).Name}");
                return;
            }

            try
            {
                await Run(e, msg);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }

        protected abstract ETTask Run(E unit, Message message);

    }
}
