﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public static class RandomUtils
    {
        private static readonly Random m_random = new Random();

        public static UInt64 RandUInt64()
        {
            var bytes = new byte[8];
            m_random.NextBytes(bytes);
            return BitConverter.ToUInt64(bytes, 0);
        }

        public static Int64 RandInt64()
        {
            var bytes = new byte[8];
            m_random.NextBytes(bytes);
            return BitConverter.ToInt64(bytes, 0);
        }

        /// <summary>
        /// 获取lower与Upper之间的随机数
        /// </summary>
        /// <param name="lower"></param>
        /// <param name="upper"></param>
        /// <returns></returns>
        public static int RandomNumber(int lower, int upper)
        {
            int value = m_random.Next(lower, upper);
            return value;
        }

    }
}
