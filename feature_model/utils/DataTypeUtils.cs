﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model
{
    public static class DataTypeUtils
    {
        public static T Any<T>(this List<T> lst)
        {
            if (lst.Count == 0)
            {
                return default;
            }
            return lst[RandomUtils.RandomNumber(0, lst.Count)];
        }

        public static T Any<K, T>(this Dictionary<K, T> dict)
        {
            int cnt = dict.Count;
            int idx = RandomUtils.RandomNumber(0, cnt);
            int i = 0;
            foreach (var pair in dict)
            {
                if (i == idx)
                {
                    return pair.Value;
                }
                else
                {
                    i++;
                }
            }
            return default;
        }

        public static bool Contain<T>(T[] arr, T v)
        {
            for (int i = 0; i < arr.Length; ++i)
            {
                if (arr[i].Equals(v))
                {
                    return true;
                }
            }

            return false;
        }

        public static void Parse(ref float value, string[] nums, int idx)
        {
            if (nums.Length <= idx || nums[idx].Trim() == string.Empty)
            {
                value = 0;
                return;
            }
            value = float.Parse(nums[idx].Trim());
        }

        public static void Parse(ref int value, string[] nums, int idx)
        {
            if (nums.Length <= idx || nums[idx].Trim() == string.Empty)
            {
                value = 0;
                return;
            }
            value = int.Parse(nums[idx].Trim());
        }

        public static T[] SubArray<T>(this T[] data, int index, int length)
        {
            T[] result = new T[length];
            Array.Copy(data, index, result, 0, length);
            return result;
        }

        public static T[] SubArrayDeepClone<T>(this T[] data, int index, int length)
        {
            T[] arrCopy = new T[length];
            Array.Copy(data, index, arrCopy, 0, length);
            using (MemoryStream ms = new MemoryStream())
            {
                var bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                bf.Serialize(ms, arrCopy);
                ms.Position = 0;
                return (T[])bf.Deserialize(ms);
            }
        }
    }
}
