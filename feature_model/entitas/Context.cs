﻿namespace ETT.Model
{
    public class Context
    {
        //public static ComponentFactory Factory { get; private set; }
        public static EventSystem Event { get; private set; }
        public static GameSystem Game { get; private set; }
        public static TableSystem Table { get; private set; }

        static Context()
        {
            //Factory = new ComponentFactory();

            Event = new EventSystem();

            Game = new GameSystem();

            Table = new TableSystem();
        }

        public static void Dispose()
        {
            Game.Dispose();

            Table.Dispose();

            Event = null;
        }
    }
}
