﻿namespace ETT.Model
{
    public sealed class GameSystem : Entitas, IRootComponent
    {
        public string Name { get; set; }
        public int RootId { get; set; }

        public FactoryEntity Factory { get; }

        public GameSystem()
        {
            Root = this;
            Factory = new FactoryEntity(this);
        }
    }
}
