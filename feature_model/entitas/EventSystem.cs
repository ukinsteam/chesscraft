﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace ETT.Model
{
    public enum DllType
    {
        Model,
        AI,
        Client,
        Server,
    }
    public class EventSystem
    {
        public Action<Component> OnAddCom;
        public Action<Component> OnDelCom;

        private readonly Dictionary<DllType, Assembly> m_assemblies = new Dictionary<DllType, Assembly>();
        private readonly Dictionary<Type, List<Type>> m_attribute_types = new Dictionary<Type, List<Type>>();

        private readonly Dictionary<long, Component> m_component_dict = new Dictionary<long, Component>();
        private readonly Dictionary<string, List<IAbstractEvent>> m_event_dict = new Dictionary<string, List<IAbstractEvent>>();

        private readonly Queue<long> m_starts_queue = new Queue<long>();

        private Queue<long> m_update_queue = new Queue<long>();
        private Queue<long> m_update_queue2 = new Queue<long>();

        private Queue<long> m_late_update_queue = new Queue<long>();
        private Queue<long> m_late_update_queue2 = new Queue<long>();

        public void Add(DllType dllType, Assembly assembly)
        {
            m_assemblies[dllType] = assembly;
            m_attribute_types.Clear();
            foreach (var pair in m_assemblies)
            {
                foreach (Type type in pair.Value.GetTypes())
                {
                    object[] attributes = type.GetCustomAttributes(typeof(BaseAttribute), false);
                    if (attributes.Length == 0)
                    {
                        continue;
                    }

                    foreach (var attr in attributes)
                    {
                        if (attr is BaseAttribute base_attribute)
                        {
                            var attr_type = base_attribute.AttributeType;
                            if (m_attribute_types.ContainsKey(attr_type) == false)
                            {
                                m_attribute_types.Add(attr_type, new List<Type>());
                            }
                            var type_list = m_attribute_types[attr_type];
                            type_list.Add(type);
                        }
                    }
                }
            }

            m_event_dict.Clear();
            if (m_attribute_types.ContainsKey(typeof(EventAttribute)))
            {
                foreach (Type type in m_attribute_types[typeof(EventAttribute)])
                {
                    object[] attrs = type.GetCustomAttributes(typeof(EventAttribute), false);
                    foreach (object attr in attrs)
                    {
                        EventAttribute event_attr = (EventAttribute)attr;
                        object obj = Activator.CreateInstance(type);

                        IAbstractEvent ievent = obj as IAbstractEvent;
                        if (obj == null)
                        {
                            Log.Error($"{obj.GetType().Name}没有继承IAbstractEvent");
                        }

                        if (!m_event_dict.ContainsKey(event_attr.Type))
                        {
                            m_event_dict.Add(event_attr.Type, new List<IAbstractEvent>());
                        }
                        m_event_dict[event_attr.Type].Add(ievent);
                    }
                }
            }
        }

        public List<Type> GetAttrTypes(Type attr_type)
        {
            if (m_attribute_types.ContainsKey(attr_type) == false)
            {
                return new List<Type>();
            }
            return m_attribute_types[attr_type];
        }

        public void Add(Component component)
        {
            if (m_component_dict.ContainsKey(component.InstanceId) == true)
            {
                throw new Exception("EventsHub.add component already exist");
            }

            m_component_dict.Add(component.InstanceId, component);
            OnAddCom?.Invoke(component);

            if (component is IUpdate)
            {
                m_update_queue.Enqueue(component.InstanceId);
            }
            if (component is IStart)
            {
                m_starts_queue.Enqueue(component.InstanceId);
            }
            if (component is ILateUpdate)
            {
                m_late_update_queue.Enqueue(component.InstanceId);
            }
        }

        public void Remove(long id)
        {
            if (m_component_dict.ContainsKey(id) == true)
            {
                OnDelCom?.Invoke(m_component_dict[id]);
                m_component_dict.Remove(id);
            }
        }

        public Component Get(long id)
        {
            Component component = null;
            m_component_dict.TryGetValue(id, out component);
            return component;
        }

        public void Awake(Component component)
        {
            try
            {
                if (component is IAwake obj)
                {
                    obj?.Awake();
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void Awake<T>(Component component, T t)
        {
            try
            {
                if (component is IAwake<T> obj)
                {
                    obj?.Awake(t);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void Awake<T1, T2>(Component component, T1 t1, T2 t2)
        {
            try
            {
                if (component is IAwake<T1, T2> obj)
                {
                    obj?.Awake(t1, t2);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void Awake<T1, T2, T3>(Component component, T1 t1, T2 t2, T3 t3)
        {
            try
            {
                if (component is IAwake<T1, T2, T3> obj)
                {
                    obj?.Awake(t1, t2, t3);
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void Update()
        {
            Start();

            while (m_update_queue.Count > 0)
            {
                long instance_id = m_update_queue.Dequeue();
                if (m_component_dict.TryGetValue(instance_id, out Component component) == false)
                {
                    continue;
                }
                if (component.IsDisposed == true)
                {
                    continue;
                }

                m_update_queue2.Enqueue(instance_id);

                try
                {
                    IUpdate obj = component as IUpdate;
                    obj?.Update();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }

            ObjectUtils.Swap(ref m_update_queue, ref m_update_queue2);
        }

        public void LateUpdate()
        {
            while (m_late_update_queue.Count > 0)
            {
                long instance_id = m_late_update_queue.Dequeue();
                if (m_component_dict.TryGetValue(instance_id, out Component component) == false)
                {
                    continue;
                }
                if (component.IsDisposed == true)
                {
                    continue;
                }

                m_late_update_queue2.Enqueue(instance_id);

                try
                {
                    ILateUpdate obj = component as ILateUpdate;
                    obj?.LateUpdate();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }

            ObjectUtils.Swap(ref m_late_update_queue, ref m_late_update_queue2);
        }

        private void Start()
        {
            while (m_starts_queue.Count > 0)
            {
                long instance_id = m_starts_queue.Dequeue();
                if (m_component_dict.TryGetValue(instance_id, out Component component) == false)
                {
                    continue;
                }

                try
                {
                    IStart obj = component as IStart;
                    obj?.Start();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }
        }

        public void Run(string event_type)
        {
            if (m_event_dict.TryGetValue(event_type, out List<IAbstractEvent> ievents) == false)
            {
                return;
            }
            foreach (IAbstractEvent ievent in ievents)
            {
                try
                {
                    (ievent as IEvent)?.Run();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }
        }
        public void Run<T1>(string event_type, T1 t1)
        {
            if (m_event_dict.TryGetValue(event_type, out List<IAbstractEvent> ievents) == false)
            {
                return;
            }
            foreach (IAbstractEvent ievent in ievents)
            {
                try
                {
                    (ievent as IEvent<T1>)?.Run(t1);
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }
        }
        public void Run<T1, T2>(string event_type, T1 t1, T2 t2)
        {
            if (m_event_dict.TryGetValue(event_type, out List<IAbstractEvent> ievents) == false)
            {
                return;
            }
            foreach (IAbstractEvent ievent in ievents)
            {
                try
                {
                    (ievent as IEvent<T1, T2>)?.Run(t1, t2);
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }
        }
        public void Run<T1, T2, T3>(string event_type, T1 t1, T2 t2, T3 t3)
        {
            if (m_event_dict.TryGetValue(event_type, out List<IAbstractEvent> ievents) == false)
            {
                return;
            }
            foreach (IAbstractEvent ievent in ievents)
            {
                try
                {
                    (ievent as IEvent<T1, T2, T3>)?.Run(t1, t2, t3);
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }
        }
        public void Destroy(Component component)
        {
            try
            {
                if (component is IDestroy obj)
                {
                    obj?.Destroy();
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
