﻿using ETT.Model.Command;
using System;
using System.Collections.Generic;

namespace ETT.Model
{
    public class CommandComponent : Component, IAwake
    {
        private Dictionary<string, AbstractCmd> m_dict = new Dictionary<string, AbstractCmd>();

        public void Awake()
        {
            var options = Context.Event.GetAttrTypes(typeof(CommandOptionAttribute));
            foreach (var type in options)
            {
                object[] attr_list = type.GetCustomAttributes(typeof(CommandOptionAttribute), false);
                if (attr_list.Length == 0)
                {
                    continue;
                }

                if (attr_list[0] is CommandOptionAttribute option_attr)
                {
                    var cmd_name = option_attr.Command.ToLower();
                    if (m_dict.ContainsKey(cmd_name) == true)
                    {
                        Log.Error($"Error! Cmd重复 {cmd_name}");
                        continue;
                    }
                    var cmd = Activator.CreateInstance(type) as AbstractCmd;
                    if (cmd == null)
                    {
                        Log.Error($"Error! Cmd需继承自AbstractCmd {cmd_name}");
                        continue;
                    }
                    m_dict.Add(cmd_name, cmd);
                }
            }
        }

        public void Parse(string[] args)
        {
            if (args.Length == 0)
            {
                Log.Error("指令无效");
                return;
            }

            var cmd_name = args[0].ToLower();
            if (m_dict.ContainsKey(cmd_name) == false)
            {
                Log.Error("指令不存在");
                return;
            }
            var cmd = m_dict[cmd_name];

            bool result = cmd.Check(args.SubArray(1, args.Length - 1));
            if (result == true)
            {
                cmd.Execute();
            }
            else
            {
                Log.Error("指令解析失败");
            }
        }
    }
}
