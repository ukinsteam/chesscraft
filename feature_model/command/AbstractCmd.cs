﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.Command
{
    public abstract class AbstractCmd
    {
        public abstract bool Check(string[] args);

        public abstract void Execute();
    }
}
