﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class EventAttribute : BaseAttribute
    {
        public string Type { get; }

        public EventAttribute(string event_type)
        {
            Type = event_type;
        }
    }
}
