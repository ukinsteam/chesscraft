﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IAbstractEvent
    {

    }

    public interface IEvent : IAbstractEvent
    {
        void Run();
    }

    public interface IEvent<T> : IAbstractEvent
    {
        void Run(T t);
    }

    public interface IEvent<T1, T2> : IAbstractEvent
    {
        void Run(T1 t1, T2 t2);
    }

    public interface IEvent<T1, T2, T3> : IAbstractEvent
    {
        void Run(T1 t1, T2 t2, T3 t3);
    }
}
