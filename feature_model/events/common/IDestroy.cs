﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IDestroy
    {
        void Destroy();
    }
}
