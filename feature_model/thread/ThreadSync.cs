﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ETT.Model
{
    public class ThreadSync : SynchronizationContext
    {
        public static ThreadSync Instance { get; } = new ThreadSync();

        private static int mainThreadId = Thread.CurrentThread.ManagedThreadId;

        private readonly ConcurrentQueue<Action> queue = new ConcurrentQueue<Action>();

        private Action a;

        public void Update()
        {
            while (true)
            {
                if (this.queue.TryDequeue(out a) == false)
                {
                    return;
                }
                a?.Invoke();
            }
        }

        public override void Post(SendOrPostCallback d, object state)
        {
            if (Thread.CurrentThread.ManagedThreadId == mainThreadId)
            {
                d?.Invoke(state);
                return;
            }
            queue.Enqueue(() => { d?.Invoke(state); });
        }
    }
}
