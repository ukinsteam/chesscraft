﻿namespace UKon.Timer
{
    /// <summary>
    /// 抽象定时器
    /// </summary>
    public abstract class AbstractTimer : IAbstractTimer
    {
        protected static AbstractTimeMgr m_timeMgr;
        public static void SetTimeMgr(AbstractTimeMgr mgr)
        {
            m_timeMgr = mgr;
        }

        protected long m_startValue;
        protected long m_intervalValue;
        protected int m_loopCnt;
        //protected bool m_isComplete;
        //protected int m_delayValue;
        protected TimeEventHandler m_handler;

        protected bool m_isPause = false;
        public bool IsPause { get { return m_isPause; } }

        protected bool m_isComplete = true;
        public bool IsComplete { get { return m_isComplete; } }

        /// <summary>
        /// 开始
        /// </summary>
        public abstract void Start();

        /// <summary>
        /// 暂停
        /// </summary>
        public virtual void Pause()
        {
            m_isPause = true;
        }

        /// <summary>
        /// 循环执行的方法
        /// </summary>
        public abstract void Tick();

        /// <summary>
        /// 恢复
        /// </summary>
        public virtual void Resume()
        {
            m_isPause = false;
        }

        /// <summary>
        /// 结束
        /// </summary>
        public abstract void Stop();

    }
}
