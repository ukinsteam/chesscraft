﻿using System.Collections.Generic;

namespace UKon.Shell
{
    public class ShellDictionary
    {
        private Dictionary<string, ShellType> m_dict = new Dictionary<string, ShellType>();

        public void Add<T>(string key, T value)
        {
            m_dict.Add(key, new ShellType<T>(value));
        }

        public bool Remove(string key)
        {
            return m_dict.Remove(key);
        }

        public bool ContainsKey(string key)
        {
            return m_dict.ContainsKey(key);
        }

        public bool ContainsKey<T>(string key)
        {
            if (m_dict.ContainsKey(key) == false)
            {
                return false;
            }
            var shell = m_dict[key];
            var value = shell as ShellType<T>;
            if (value == null)
            {
                return false;
            }
            return true;
        }

        public T Find<T>(string key)
        {
            T value = default(T);
            if (ContainsKey<T>(key) == true)
            {
                value = (m_dict[key] as ShellType<T>).Value;
            }
            return value;
        }

        public void Clear()
        {
            m_dict.Clear();
        }

    }
}
