﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：Dictionary
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/27 21:59:20
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Collections
{
    public class DictionaryAgent<K, T> : IDictionaryAgent<K, T>
       where T : IAgentElement<K>
    {
        protected Dictionary<K, T> m_dict = new Dictionary<K, T>();

        public T this[K key]
        {
            get
            {
                m_dict.TryGetValue(key, out T value);
                return value;
            }
        }

        public int Count => m_dict.Count;

        public DictionaryAgent()
        {
            m_dict.Clear();
        }

        public virtual void Add(T value)
        {
            if (m_dict.ContainsKey(value.Key) == true)
            {
                return;
            }
            m_dict.Add(value.Key, value);
        }

        public virtual T Remove(K key)
        {
            m_dict.TryGetValue(key, out T value);
            if (m_dict.ContainsKey(key) == true)
            {
                m_dict.Remove(key);
            }
            return value;
        }
    }
}
