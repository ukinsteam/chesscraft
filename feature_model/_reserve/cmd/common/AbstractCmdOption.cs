﻿using ETT.Model;

namespace UKon.Cmd
{
    public abstract class AbstractCmdOption
    {
        public abstract string type { get; }

        public abstract string[] names { get; }

        /// <summary>
        /// 该选项可处理的参数个数
        /// </summary>
        public virtual uint ArgumentCnt => 0;

        protected AbstractCommand m_Owner;

        public AbstractCmdOption(AbstractCommand command)
        {
            m_Owner = command;
        }

        protected bool IsParamValid(string param)
        {
            foreach (var n in names)
            {
                if (n == param)
                {
                    return true;
                }
            }
            return false;
        }

        protected int GetArgumentIdx(string[] args)
        {
            int idx = 2;
            if (m_Owner.IsDefaultOption(this) == true && IsParamValid(args[1]))
            {
                idx = 1;
            }
            return idx;
        }

        public virtual bool Match(string[] args)
        {
            int idx = GetArgumentIdx(args);
            if (args.Length > idx + ArgumentCnt)
            {
                Log.Error("Too many arguments!");
                return false;
            }
            return true;
        }

        public abstract bool Process(string[] args);
    }
}
