﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：AbstractOrder
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/5 9:51:36
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Cmd
{
    public enum EOrderParamType
    {
        None,
        Truncate,
        List,
        Complete
    }

    public abstract class AbstractOrder
    {
        public abstract string Cmd { get; }
        public abstract string[] Option { get; }
        public abstract EOrderParamType ParamType { get; }

        public abstract void Execute(string[] args);
    }
}
