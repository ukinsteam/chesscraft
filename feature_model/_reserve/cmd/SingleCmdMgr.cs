﻿using ETT.Model;
using System.Collections.Generic;

namespace UKon.Cmd
{
    public class SingleCmdMgr //: IRegisterable<string, AbstractCommand>
    {
        private Dictionary<string, AbstractCommand> m_dict = new Dictionary<string, AbstractCommand>();

        public bool Register(AbstractCommand command)
        {
            string key = command.Id;
            if (m_dict.ContainsKey(key) == true)
            {
                Log.Error(string.Format("Command Already Registered! Command Name = {0}.", key));
                return false;
            }
            m_dict.Add(key, command);
            return true;
        }

        public bool UnRegister(string key)
        {
            if (m_dict.ContainsKey(key) == true)
            {
                m_dict.Remove(key);
            }
            return true;
        }

        public bool Process(string[] args)
        {
            if (args.Length < 1)
            {
                Log.Error("please input valid command!");
                return false;
            }

            string name = args[0].ToLower();
            if (m_dict.ContainsKey(name) == false)
            {
                Log.Error("please input valid command!");
                return false;
            }

            bool result = m_dict[name].Process(args);

            return true;
        }

    }
}
