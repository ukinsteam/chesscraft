﻿using ETT.Model;
using System.Collections.Generic;

namespace UKon.Cmd
{
    public class MultiCmdMgr //: IMultiRegisterable<string, AbstractCommand>
    {
        private Dictionary<string, HashSet<AbstractCommand>> m_dict = new Dictionary<string, HashSet<AbstractCommand>>();

        public bool Register(AbstractCommand command)
        {
            string key = command.Id;
            if (m_dict.ContainsKey(key) == false)
            {
                m_dict.Add(key, new HashSet<AbstractCommand>());
            }
            if (m_dict[key].Contains(command) == true)
            {
                Log.Warning(string.Format("Command Already Registered! Command Name = {0}.", key));
                return false;
            }
            m_dict[key].Add(command);
            return true;
        }

        public bool UnRegister(AbstractCommand command)
        {
            string key = command.Id;

            if (m_dict.ContainsKey(key) == true)
            {
                var set = m_dict[key];
                set.Remove(command);
            }
            return true;
        }

        public bool Process(string[] args)
        {
            if (args.Length < 1)
            {
                Log.Error("please input valid command!");
                return false;
            }

            string name = args[0].ToLower();
            if (m_dict.ContainsKey(name) == false)
            {
                Log.Error("please input valid command!");
                return false;
            }

            var hashset = m_dict[name];

            bool result = false;
            foreach (var cmd in hashset)
            {
                result = cmd.Process(args);
                if (result == true)
                {
                    break;
                }
            }
            if (result == false)
            {
                Log.Error("Cmd cannot be parsed");
            }

            return true;
        }
    }
}
