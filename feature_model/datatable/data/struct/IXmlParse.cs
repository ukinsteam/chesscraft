﻿using System.Xml;

namespace ETT.Table.Analysis
{
    public interface IXmlParse
    {
        void Parse(XmlNode node);
    }
}
