﻿using System.Text;
using System.Xml;

namespace ETT.Table.Analysis
{
    public class NodeFieldVO : AbstractMultiDataVO, IFieldVO
    {
        /// <summary>
        /// 字段类型
        /// </summary>
        protected string m_type;
        protected TypeParser m_typeparser;
        public TypeParser Type { get { return m_typeparser; } }
        
        public bool InCollection
        {
            get
            {
                var parent = Owner as NodeFieldVO;
                while (parent != null)
                {
                    if (parent.Type.IsCollection == true)
                    {
                        return true;
                    }
                    parent = parent.Owner as NodeFieldVO;
                }
                return false;
            }
        }
        /// <summary>
        /// 与容器属性配合使用
        /// </summary>
        public bool IsVertical { get; protected set; }


        protected void ParseField(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");
            m_type = ExtractString(node, "type");
            IsVertical = ExtractBool(node, "isvertical");

            m_typeparser = new TypeParser(m_type);
        }

        public override void Parse(XmlNode node)
        {
            ParseField(node);

            ParseChildren<LeafFieldVO, NodeFieldVO>(node);
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (Name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", Name));
            }
            if (m_type != string.Empty)
            {
                sb.Append(string.Format("type=\'{0}\' ", m_type));
            }
            if (IsVertical == false)
            {
                sb.Append(string.Format("isvertical=\'{0}\' ", "false"));
            }
            sb.AppendLine(">");

            for (int i = 0; i < m_fields.Count; i++)
            {
                sb.Append(m_fields[i].ToXMLString(prefix + "\t"));
                sb.AppendLine();
            }
            sb.Append(prefix);
            sb.Append("</field>");
            return sb.ToString();
        }
    }
}
