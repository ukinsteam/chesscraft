﻿using ETT.Table.Analysis;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace ETT.Table
{
    public class TableVO : AbstractMultiDataVO
    {
        /// <summary>
        /// 枚举类型表格
        /// </summary>
        public bool IsEnum { get; protected set; }

        ///// <summary>
        ///// 表格的输出路径
        ///// </summary>
        //public string Outputpath { get; protected set; }

        /// <summary>
        /// 是否压缩
        /// </summary>
        public bool IsCompress { get; protected set; }

        /// <summary>
        /// 是否加密
        /// </summary>
        public bool IsEncrypt { get; protected set; }

        /// <summary>
        /// 表格会根据文件所在feature自动生成对应的解析类
        /// </summary>
        public bool IsSeparate { get; protected set; }

        public LeafFieldVO PrimaryKey { get; protected set; }

        public VOPair<LeafFieldVO, LeafFieldVO> UnionKey { get; protected set; } = new VOPair<LeafFieldVO, LeafFieldVO>();

        public ETableKeyType TableKeyType { get; protected set; } = ETableKeyType.Invalid;

        protected virtual void ParseAttr(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");
            IsEnum = ExtractBool(node, "isenum");
            //Outputpath = ExtractString(node, "outputpath");
            IsCompress = ExtractBool(node, "iscompress");
            IsEncrypt = ExtractBool(node, "isencrypt");
            IsSeparate = ExtractBool(node, "isseparate");
        }

        protected virtual void ParseChild(XmlNode node)
        {
            ParseChildren<LeafFieldVO, NodeFieldVO>(node);
        }

        protected virtual void ParseKey()
        {
            var m_leaves = FindLeaves();

            foreach (var vo in m_leaves)
            {
                if (vo.IsPrimaryKey == true)
                {
                    PrimaryKey = vo;
                    TableKeyType = ETableKeyType.PrimaryKey;
                }
            }

            if (TableKeyType == ETableKeyType.Invalid)
            {
                List<LeafFieldVO> unionkeys = m_leaves.FindAll(c => c.IsUnionKey == true);
                if (unionkeys.Count == 2)
                {
                    UnionKey.First = unionkeys[0];
                    UnionKey.Second = unionkeys[1];
                    TableKeyType = ETableKeyType.UnionKey;
                }
            }
        }

        protected virtual void ParseOther()
        {

        }

        public override void Parse(XmlNode node)
        {
            ParseAttr(node);

            ParseChild(node);

            ParseKey();

            ParseOther();
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<table ");
            if (Name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", Name));
            }
            //if (Outputpath != string.Empty)
            //{
            //    sb.Append(string.Format("outputpath=\'{0}\' ", Outputpath));
            //}
            if (IsCompress == true)
            {
                sb.Append(string.Format("iscompress=\'{0}\' ", "true"));
            }
            if (IsEncrypt == true)
            {
                sb.Append(string.Format("isencrypt=\'{0}\' ", "true"));
            }
            if(IsSeparate == true)
            {
                sb.Append(string.Format("isseparate=\'{0}\' ", "true"));
            }
            sb.AppendLine(">");


            for (int i = 0; i < m_fields.Count; i++)
            {
                sb.Append(m_fields[i].ToXMLString(prefix + "\t"));
                sb.AppendLine();
            }
            sb.Append(prefix);
            sb.Append("</table>");
            return sb.ToString();
        }
    }
}
