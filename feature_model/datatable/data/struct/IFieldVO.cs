﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETT.Table.Analysis
{
    public interface IFieldVO
    {
        string FullName { get; }
        string Name { get; }
        TypeParser Type { get; }
        string ToXMLString(string prefix);
        TableVO Root { get; }
        bool IsVertical { get; }
        bool InCollection { get; }
    }
}
