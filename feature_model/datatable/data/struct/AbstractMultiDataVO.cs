﻿using System.Collections.Generic;
using System.Xml;

namespace ETT.Table.Analysis
{
    public abstract class AbstractMultiDataVO : AbstractDataVO
    {
        protected List<IFieldVO> m_fields = new List<IFieldVO>();
        public List<IFieldVO> FindFields() { return m_fields; }

        public int Count => m_fields.Count;

        public IFieldVO this[int i] => m_fields[i];

        public IFieldVO this[string name] => m_fields.Find(c => c.FullName == name);

        public List<LeafFieldVO> FindLeaves()
        {
            List<LeafFieldVO> lst = new List<LeafFieldVO>();
            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_fields[i] is LeafFieldVO)
                {
                    lst.Add(m_fields[i] as LeafFieldVO);
                }
            }
            return lst;
        }

        public List<NodeFieldVO> FindNodes()
        {
            List<NodeFieldVO> lst = new List<NodeFieldVO>();
            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_fields[i] is NodeFieldVO)
                {
                    lst.Add(m_fields[i] as NodeFieldVO);
                }
            }
            return lst;
        }


        public virtual int CountChildVO(string name)
        {
            int cnt = 0;

            foreach (var vo in m_fields)
            {
                if (vo.Name.ToLower() == name.ToLower())
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public List<LeafFieldVO> FindAllLeaves()
        {
            List<LeafFieldVO> lst = new List<LeafFieldVO>();

            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_fields[i] is LeafFieldVO)
                {
                    lst.Add(m_fields[i] as LeafFieldVO);
                }
                else if (m_fields[i] is NodeFieldVO)
                {
                    lst.AddRange((m_fields[i] as NodeFieldVO).FindAllLeaves());
                }
            }

            return lst;
        }

        protected void ParseChildren<TF, TN>(XmlNode node)
           where TF : LeafFieldVO, new()
           where TN : NodeFieldVO, new()
        {
            XmlNodeList fields = node.SelectNodes("field");

            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                if (field.HasChildNodes == true)
                {
                    TN vo = new TN();
                    vo.Parse(fields[i]);
                    vo.Owner = this;
                    m_fields.Add(vo);
                }
                else
                {
                    TF vo = new TF();
                    vo.Parse(fields[i]);
                    vo.Owner = this;
                    m_fields.Add(vo);
                }
            }
        }

    }
}
