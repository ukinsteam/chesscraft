﻿using System.Xml;

namespace ETT.Table.Analysis
{
    public abstract class AbstractDataVO : IXmlParse
    {
        public virtual string Name { get; protected set; }

        public AbstractMultiDataVO Owner { get; set; }

        public virtual TableVO Root
        {
            get
            {
                if (Owner == null)
                {
                    return null;
                }
                var p = Owner;
                while ((p is TableVO) == false)
                {
                    p = p.Owner;
                }
                return p as TableVO;
            }
        }

        private string m_fullname = string.Empty;
        public virtual string FullName
        {
            get
            {
                if (m_fullname == string.Empty)
                {
                    m_fullname = Name;
                    if (Owner != null && (Owner is TableVO) == false)
                    {
                        m_fullname = $"{Owner.FullName}.{Name}";
                    }
                }
                return m_fullname;
            }
        }

        public XmlNode XmlNode { get; protected set; }


        protected string ExtractString(XmlNode node, string name)
        {
            var attr = node.Attributes[name];
            return attr != null ? attr.Value : string.Empty;
        }

        protected bool ExtractBool(XmlNode node, string name, bool defaultvalue = false)
        {
            var attr = node.Attributes[name];
            bool value = attr != null ? (attr.Value.ToLower() == "true" ? true : false) : defaultvalue;
            return value;
        }

        public abstract void Parse(XmlNode node);

        public abstract string ToXMLString(string prefix);
    }
}
