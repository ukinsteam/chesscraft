﻿namespace ETT.Table.Analysis
{
    public class PrimaryKeyDataTable : AbstractDataTable
    {
        public LeafFieldVO PrimaryKey => Columns.PrimaryKey;

        protected readonly PrimaryKeyDataRowCollection m_rows;
        public override GenericDataRowCollection Rows => m_rows;

        public PrimaryKeyDataTable(TableVO vo) : base(vo)
        {
            m_rows = new PrimaryKeyDataRowCollection(this);
        }

        public override void Union(AbstractDataTable table)
        {
            var primarykeytable = table as PrimaryKeyDataTable;
            if (primarykeytable == null || primarykeytable.TableName != TableName)
            {
                return;
            }
            m_rows.Union(primarykeytable.m_rows);
        }

        //public override string ToString()
        //{
        //    return string.Format("{0}\n{1}", m_columns, m_rows);
        //}

        public override AbstractDataTable Clone()
        {
            PrimaryKeyDataTable table = new PrimaryKeyDataTable(m_columns);
            table.m_rows.Union(m_rows);
            return table;
        }
    }

}
