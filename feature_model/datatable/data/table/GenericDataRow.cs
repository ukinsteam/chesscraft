﻿using ETT.Model;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Table.Analysis
{
    public class GenericDataRow
    {

        public readonly int RowIndex = 0;

        private int m_max_rowindex = 0;
        public int MaxRowIndex { get { return m_max_rowindex; } }

        private readonly List<GenericDataField> m_list = new List<GenericDataField>();

        public List<GenericDataLeafField> this[string columnname]
        {
            get
            {
                List<GenericDataLeafField> leaves = new List<GenericDataLeafField>();
                for (int i = 0; i < m_list.Count; i++)
                {
                    if (m_list[i] is GenericDataLeafField)
                    {
                        var leaf = m_list[i] as GenericDataLeafField;
                        if (leaf.Column.FullName == columnname)
                        {
                            leaves.Add(leaf);
                        }
                    }
                    else
                    {
                        var node = m_list[i] as GenericDataNodeField;
                        leaves.AddRange(node.FindAllLeafFields(columnname));
                    }
                }

                return leaves;
            }
        }

        public GenericDataLeafField FindLeafField(string columnname)
        {
            return m_list.Find(c => c.Column.FullName == columnname) as GenericDataLeafField;
        }

        public List<GenericDataLeafField> FindLeafFieldsByRowIdx(int rowidx)
        {
            List<GenericDataLeafField> leaves = new List<GenericDataLeafField>();
            for (int i = 0; i < m_list.Count; i++)
            {
                if (m_list[i] is GenericDataLeafField)
                {
                    var leaf = m_list[i] as GenericDataLeafField;
                    if (leaf.RowIndex == rowidx)
                    {
                        leaves.Add(leaf);
                    }
                }
                else
                {
                    var node = m_list[i] as GenericDataNodeField;
                    var allleaves = node.FindAllLeafFields();
                    leaves.AddRange(allleaves.FindAll(c => c.RowIndex == rowidx));
                }
            }

            return leaves;
        }

        protected AbstractDataTable m_table;

        protected internal GenericDataRow(AbstractDataTable table, int rowidx)
        {
            m_table = table;
            RowIndex = rowidx;
            m_max_rowindex = RowIndex;

            int columncnt = m_table.Columns.Count;

            for (int i = 0; i < columncnt; ++i)
            {
                var col = m_table.Columns[i];
                GenericDataField field = null;
                if (col is LeafFieldVO)
                {
                    field = new GenericDataLeafField(col, rowidx);
                }
                else if (col is NodeFieldVO)
                {
                    field = new GenericDataNodeField(col, rowidx);
                }

                if (field != null)
                {
                    m_list.Add(field);
                }
                else
                {
                    Log.Error("GenericDataRow Create Field Failed");
                }
            }
        }

        public void Merge(GenericDataRow row)
        {
            for (int i = 0; i < m_list.Count; ++i)
            {
                m_list[i].Merge(row.m_list[i]);
            }
            if (row.m_max_rowindex > m_max_rowindex)
            {
                m_max_rowindex = row.m_max_rowindex;
            }
        }

        public void SetValue(string[] row_fields)
        {
            int idx = 0;
            for (int i = 0; i < m_list.Count; i++)
            {
                m_list[i].SetValue(row_fields, ref idx);
            }
        }

        public void Build(BinaryWriter bw)
        {
            for (int i = 0; i < m_list.Count; ++i)
            {
                m_list[i].Build(bw);
            }
        }

        public string ToCSVString()
        {
            StringBuilder sb = new StringBuilder();
            var columns = m_table.Columns.FindAllLeaves();
            for (int i = RowIndex; i <= m_max_rowindex; ++i)
            {
                var leaves = FindLeafFieldsByRowIdx(i);
                if (leaves.Count == 0)
                {
                    continue;
                }
                sb.AppendFormat("line {0,4}:\t", i);

                for (int j = 0; j < columns.Count; ++j)
                {
                    var leaf = leaves.Find(c => ReferenceEquals(c.Column, columns[j]));
                    if (leaf != null)
                    {
                        sb.AppendFormat("[{0,5}]\t", leaf.ToString());
                    }
                    else
                    {
                        sb.AppendFormat("[{0,5}]\t", "");
                    }
                }
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}
