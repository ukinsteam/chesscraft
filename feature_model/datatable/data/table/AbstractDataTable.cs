﻿using System.IO;

namespace ETT.Table.Analysis
{
    public abstract class AbstractDataTable
    {
        public virtual string TableName => m_columns.Name;

        protected TableVO m_columns = new TableVO();
        public virtual TableVO Columns => m_columns;

        public AbstractDataTable(TableVO vo) => m_columns = vo;

        public abstract GenericDataRowCollection Rows { get; }


        private int m_total_row_cnt = 0;
        public int TotalRowCnt { get { return m_total_row_cnt; } }

        public virtual GenericDataRow NewRow()
        {
            m_total_row_cnt++;
            return new GenericDataRow(this, m_total_row_cnt);
        }

        public abstract void Union(AbstractDataTable table);

        public abstract AbstractDataTable Clone();

        public virtual void Build(BinaryWriter bw)
        {
            Rows.Build(bw);
        }

        public string ToCSVString()
        {
            return Rows.ToCSVString();
        }
    }
}
