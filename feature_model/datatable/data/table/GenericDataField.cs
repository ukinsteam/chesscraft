﻿using System.Collections.Generic;
using System.IO;

namespace ETT.Table.Analysis
{
    public abstract class GenericDataField
    {
        protected IFieldVO m_column;
        public IFieldVO Column => m_column;

        //public virtual void SetValue(string value)
        //{
        //    m_strvalue = value;
        //    if (m_column.Type.IsCollection == true)
        //    {
        //        if (m_column.IsVertical == false)
        //        {
        //            m_slice.AddRange(value.Split('|'));
        //            return;
        //        }
        //    }
        //    m_slice.Add(value);
        //}

        public abstract void SetValue(string[] row_fields, ref int idx);

        /// <summary>
        /// 数据分片数组，
        /// 常规字段，m_slice只有一个数据，就是m_strvalue
        /// 水平分组字段，m_slice有多个数据，SetValue时处理完成
        /// 垂直分组字段，m_slice有多个数据，SetValue时处理第一个数据，后续通过Merge合并进来
        /// </summary>
        protected List<string> m_slice = new List<string>();
        public virtual List<string> Slice => m_slice;

        public readonly int RowIndex = 0;

        protected internal GenericDataField(IFieldVO column, int rowidx)
        {
            RowIndex = rowidx;
            m_column = column;
        }

        public abstract void Merge(GenericDataField field);

        public abstract void Build(BinaryWriter bw);

        public override string ToString()
        {
            return string.Join("|", m_slice.ToArray());
        }

        //public void Clone()
        //{

        //}

    }
}
