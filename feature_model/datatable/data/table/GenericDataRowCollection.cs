﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Table.Analysis
{
    public abstract class GenericDataRowCollection
    {
        protected List<GenericDataRow> m_list = new List<GenericDataRow>();
        public int Count => m_list.Count;

        public GenericDataRow this[int index]
        {
            get { return m_list[index]; }
        }

        protected AbstractDataTable m_table;
        public virtual AbstractDataTable Table => m_table;

        public GenericDataRowCollection(AbstractDataTable table)
        {
            m_table = table;
        }

        public abstract void Add(GenericDataRow row);

        public void Build(BinaryWriter bw)
        {
            bw.Write(m_list.Count);
            for (int i = 0; i < m_list.Count; i++)
            {
                m_list[i].Build(bw);
            }
        }

        public string ToCSVString()
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < m_list.Count; i++)
            {
                sb.Append(m_list[i].ToCSVString());
            }
            return sb.ToString();

        }
    }
}
