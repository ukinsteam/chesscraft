﻿using ETT.Model;
using ETT.Table.Analysis;
using System.IO;

namespace ETT.Table
{
    public sealed class CSVDataTable
    {
        private TableVO m_vo;
        private AbstractDataTable m_dataTable;
        public AbstractDataTable DataTable { get { return m_dataTable; } }

        public CSVDataTable(TableVO vo)
        {
            m_vo = vo;
            switch (vo.TableKeyType)
            {
                case ETableKeyType.PrimaryKey:
                    m_dataTable = new PrimaryKeyDataTable(vo);
                    break;
                case ETableKeyType.UnionKey:
                    m_dataTable = new UnionKeyDataTable(vo);
                    break;
                default: break;
            }
        }

        public void Parse(string content)
        {
            StringReader sr = new StringReader(content);
            var leaves = m_vo.FindAllLeaves();
            //记录每次读取的一行记录  
            string strLine = "";
            //记录每行记录中的各字段内容  
            string[] row_fields = null;
            string[] col_fields = null;

            //逐行读取CSV中的数据  
            while ((strLine = sr.ReadLine()) != null)
            {
                row_fields = strLine.Split(',');
                if (row_fields[0].StartsWith("#") == true)
                {
                    continue;
                }
                if (row_fields.Length == 1 && leaves.Count >= 1)
                {
                    Log.Error("Error!  配置表分隔符必须为逗号。");
                    Log.Error(string.Format("\t tablename = {0}", m_vo.FullName));
                    sr.Close();
                    return;
                }

                //生成表头
                if (row_fields[0] == "&&")
                {
                    if (col_fields != null)
                    {
                        Log.Warning(string.Format("Warning! \"&&\"开头的行表示表头，不应该多于一行。 tablename = {0}", m_vo.Name));
                        continue;
                    }
                    col_fields = row_fields;


                    for (int i = 0; i < leaves.Count; ++i)
                    {
                        bool isexist = false;
                        for (int j = 0; j < col_fields.Length; j++)
                        {
                            if (col_fields[j] == leaves[i].FullName)
                            {
                                isexist = true;
                                break;
                            }
                        }
                        if (isexist == false)
                        {
                            Log.Error($"Error! 字段缺失。 table name = {m_vo.Name}, field name = {leaves[i].FullName}");
                            return;
                        }
                    }

                    for (int i = 0; i < col_fields.Length; ++i)
                    {
                        bool isexist = leaves.Exists(c => c.FullName == col_fields[i]);
                        if (isexist == false)
                        {
                            col_fields[i] = string.Empty;
                        }
                    }
                }
                else
                {
                    GenericDataRow dr = m_dataTable.NewRow();

                    int cnt = 0;
                    for (int i = 0; i < col_fields.Length; i++)
                    {
                        if (col_fields[i] != string.Empty)
                        {
                            cnt++;
                        }
                    }
                    string[] new_row_fields = new string[cnt];
                    for (int i = 0, idx = 0; i < col_fields.Length; i++)
                    {
                        if (i >= row_fields.Length)
                        {
                            new_row_fields[idx++] = string.Empty;
                            continue;
                        }
                        if (col_fields[i] != string.Empty)
                        {
                            new_row_fields[idx] = row_fields[i];
                            idx++;
                        }
                    }

                    dr.SetValue(new_row_fields);
                    m_dataTable.Rows.Add(dr);
                }
            }
        }
    }
}
