﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ETT.Table
{
    public abstract class SingleKeyTable<T, K> : AbstractTable<T>, ISingleKeyTable
        where T : SingleKeyItem<K>, new()
        where K : IComparable
    {
        protected readonly Dictionary<K, T> m_dict = new Dictionary<K, T>();

        public abstract AbstractItem FindVO(string key);

        public T GenericFindVO(K primarykey)
        {
            if (m_dict.ContainsKey(primarykey) == false)
            {
                return null;
            }
            return m_dict[primarykey];
        }

        public T this[K primarykey]
        {
            get
            {
                if (m_dict.ContainsKey(primarykey) == false)
                {
                    return null;
                }
                return m_dict[primarykey];
            }
        }

        ///// <summary>
        ///// 获取区间范围内的数据
        ///// </summary>
        public List<T> GenericRange(K min, K max)
        {
            return m_list.FindAll(c => c.PrimaryKey.CompareTo(min) >= 0 && c.PrimaryKey.CompareTo(max) <= 0);
        }

        public override bool Deserialize(BinaryReader br)
        {
            var len = br.ReadUInt32();
            for (int i = 0; i < len; ++i)
            {
                T conf = new T();
                conf.Deserialize(br);
                m_dict.Add(conf.PrimaryKey, conf);
                m_list.Add(conf);
            }
            return true;
        }

    }
}
