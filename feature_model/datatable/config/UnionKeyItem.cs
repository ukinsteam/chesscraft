﻿using System;

namespace ETT.Table
{
    public abstract class UnionKeyItem<K1, K2> : AbstractItem
        where K1 : IComparable
        where K2 : IComparable
    {
        public abstract K1 UnionKey1 { get; }
        public abstract K2 UnionKey2 { get; }
    }
}
