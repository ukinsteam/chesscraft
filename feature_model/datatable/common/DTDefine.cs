﻿using ETT.Model;
using System;
using System.IO;

namespace ETT.Table
{
    public enum EFieldType
    {
        Invalid,
        Single,
        Array,
        List,
        Dict
    }

    public enum EFieldLayoutType
    {
        Invalid,
        Vertical,
        Horizontal
    }

    public enum ETableKeyType
    {
        Invalid,
        PrimaryKey,
        UnionKey
    }

    public class VOPair<T, K>
        where T : new()
        where K : new()
    {
        public T First;
        public K Second;

        public VOPair() { }

        public VOPair(T t, K k)
        {
            First = t;
            Second = k;
        }
    }

    public static class DTDefine
    {
        public const string BOOL = "bool";
        public const string BYTE = "byte";
        public const string SHORT = "short";
        public const string USHORT = "ushort";
        public const string INT = "int";
        public const string UINT = "uint";
        public const string LONG = "long";
        public const string ULONG = "ulong";
        public const string FLOAT = "float";
        public const string STRING = "string";
        //public const string TEXT = "text";
        public const string TIME = "time";
        public const string TIMESPAN = "timespan";
        //public const string POINT = "point";
        public const string VECTOR2 = "vector2";
        public const string VECTOR2INT = "vector2int";
        public const string VECTOR3 = "vector3";
        public const string VECTOR3INT = "vector3int";
        public const string TUPLE = "tuple";


        public static bool CanSwitch2TgtType(string value, string type)
        {
            if (value == string.Empty)
            {
                return true;
            }
            try
            {
                switch (type)
                {
                    case BOOL:
                        bool b = bool.Parse(value);
                        break;
                    case BYTE:
                        byte i8 = byte.Parse(value);
                        break;
                    case SHORT:
                        Int16 i16 = Int16.Parse(value);
                        break;
                    case USHORT:
                        UInt16 u16 = UInt16.Parse(value);
                        break;
                    case INT:
                        Int32 i32 = Int32.Parse(value);
                        break;
                    case UINT:
                        UInt32 u32 = UInt32.Parse(value);
                        break;
                    case LONG:
                        Int64 i64 = Int64.Parse(value);
                        break;
                    case ULONG:
                        UInt64 u64 = UInt64.Parse(value);
                        break;
                    case FLOAT:
                        float f = float.Parse(value);
                        break;
                    case TIME:
                        DateTimeOffset time = DateTimeOffset.Parse(value);
                        break;
                    case TIMESPAN:
                        TimeSpan timespan = TimeSpan.Parse(value);
                        break;
                    //case POINT:
                    //    ChessPoint point = ChessPoint.Parse(value);
                    //    break;
                    case VECTOR2:
                        UVector2.Parse(value);
                        break;
                    case VECTOR2INT:
                        UVector2Int.Parse(value);
                        break;
                    case VECTOR3:
                        UVector3.Parse(value);
                        break;
                    case VECTOR3INT:
                        UVector3Int.Parse(value);
                        break;
                    case STRING:
                        return true;
                    default: return false;
                }
            }
            catch (Exception e)
            {
                Log.Error(string.Format("Error!  {0}", e.Message));
                return false;
            }
            return true;
        }

        public static string GetTypeByString(string type)
        {
            switch (type.ToLower())
            {
                case BOOL:
                    return "bool";
                case BYTE:
                    return "byte";
                case FLOAT:
                    return "float";
                case INT:
                    return "int";
                case UINT:
                    return "uint";
                case LONG:
                    return "long";
                case ULONG:
                    return "ulong";
                case SHORT:
                    return "short";
                case USHORT:
                    return "ushort";
                //case "text":
                case STRING:
                    return "string";
                case TIME:
                    return "DateTimeOffset";
                case TIMESPAN:
                    return "TimeSpan";
                //case POINT:
                //    return "ChessPoint";
                case VECTOR2:
                    return "UVector2";
                case VECTOR2INT:
                    return "UVector2Int";
                case VECTOR3:
                    return "UVector3";
                case VECTOR3INT:
                    return "UVector3Int";
                //case "guid":
                //    return "System.Guid";
                default:
                    return type;
            }
        }

        public static bool WriteData(BinaryWriter bw, string value, string type)
        {
            try
            {
                switch (type.ToLower())
                {
                    case BOOL:
                        bw.Write(value.ToLower() == "true" ? true : false);
                        break;
                    case BYTE:
                        if (value == string.Empty)
                        {
                            bw.Write(default(byte));
                        }
                        else
                        {
                            bw.Write(byte.Parse(value));
                        }
                        break;
                    case FLOAT:
                        if (value == string.Empty)
                        {
                            bw.Write(default(Single));
                        }
                        else
                        {
                            bw.Write(Single.Parse(value));
                        }
                        break;
                    case INT:
                        if (value == string.Empty)
                        {
                            bw.Write(default(Int32));
                        }
                        else
                        {
                            bw.Write(Int32.Parse(value));
                        }
                        break;
                    case UINT:
                        if (value == string.Empty)
                        {
                            bw.Write(default(UInt32));
                        }
                        else
                        {
                            bw.Write(UInt32.Parse(value));
                        }
                        break;
                    case LONG:
                        if (value == string.Empty)
                        {
                            bw.Write(default(Int64));
                        }
                        else
                        {
                            bw.Write(Int64.Parse(value));
                        }
                        break;
                    case ULONG:
                        if (value == string.Empty)
                        {
                            bw.Write(default(UInt64));
                        }
                        else
                        {
                            bw.Write(UInt64.Parse(value));
                        }
                        break;
                    case SHORT:
                        if (value == string.Empty)
                        {
                            bw.Write(default(Int16));
                        }
                        else
                        {
                            bw.Write(Int16.Parse(value));
                        }
                        break;
                    case USHORT:
                        if (value == string.Empty)
                        {
                            bw.Write(default(UInt16));
                        }
                        else
                        {
                            bw.Write(UInt16.Parse(value));
                        }
                        break;
                    //case "text":
                    case STRING:
                    case TIME:
                    case TIMESPAN:
                    case VECTOR2:
                    case VECTOR2INT:
                    case VECTOR3:
                    case VECTOR3INT:
                        bw.Write(value);
                        break;
                    default:
                        bw.Write(value);
                        return true;
                }
            }
            catch (Exception e)
            {
                Log.Warning(e.Message);
                Log.Error("AttrUtils.WriteData");
                return false;
            }

            return true;
        }



    }
}
