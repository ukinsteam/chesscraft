﻿using System;

namespace ETT.Model
{
    public struct UVector2 : IComparable, IComparable<UVector2>, IEquatable<UVector2>
    {
        public static readonly UVector2 Invalid = Parse(float.Epsilon, float.Epsilon);

        public float x;
        public float y;

        public UVector2(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public static UVector2 Parse(float x, float y)
        {
            UVector2 point;
            point.x = x;
            point.y = y;
            return point;
        }
        
        public static UVector2 Parse(string s)
        {
            UVector2 point = Parse(0, 0);
            s = s.Replace("(", "").Replace(")", "");
            string[] nums = s.Split(new char[] { '_', '|', '?', '!', '/', ':', ' ' });
            DataTypeUtils.Parse(ref point.x, nums, 0);
            DataTypeUtils.Parse(ref point.y, nums, 1);
            return point;
        }

        public static UVector2 operator +(UVector2 point, UVector2 vec)
        {
            UVector2 newpoint;
            newpoint.x = point.x + vec.x;
            newpoint.y = point.y + vec.y;
            return newpoint;
        }

        public static UVector2 operator -(UVector2 point, UVector2 vec)
        {
            UVector2 newpoint;
            newpoint.x = point.x - vec.x;
            newpoint.y = point.y - vec.y;
            return newpoint;
        }

        public int CompareTo(object obj)
        {
            if (obj == null || (obj is UVector2) == false)
            {
                return 1;
            }
            UVector2 other = (UVector2)obj;
            return CompareTo(other);
        }

        public int CompareTo(UVector2 other)
        {
            if (x > other.x)
            {
                return 1;
            }
            else if (x < other.x)
            {
                return -1;
            }

            if (y > other.y)
            {
                return 1;
            }
            else if (y < other.y)
            {
                return -1;
            }

            return 0;
        }

        public bool Equals(UVector2 other)
        {
            return CompareTo(other) == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || (obj is UVector2) == false)
            {
                return false;
            }
            return Equals((UVector2)obj);
        }

        public override int GetHashCode()
        {
            return ((x.GetHashCode() << 16) ^ y.GetHashCode()).GetHashCode();
        }

        public override string ToString()
        {
            return $"({x},{y})";
        }
    }
}
