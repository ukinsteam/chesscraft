﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class CoroutineLockComponent : Component
    {
        private readonly Dictionary<long, LockQueue> m_dict = new Dictionary<long, LockQueue>();

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            foreach (var kv in m_dict)
            {
                kv.Value.Dispose();
            }
            m_dict.Clear();
        }

        public async ETTask<CoroutineLock> Wait(long key)
        {
            if (m_dict.TryGetValue(key, out LockQueue lockQueue) == false)
            {
                m_dict.Add(key, Root.Factory.Create<LockQueue>());
                return Root.Factory.Create<CoroutineLock, long>(key);
            }
            ETTaskCompletionSource<CoroutineLock> tcs = new ETTaskCompletionSource<CoroutineLock>();
            lockQueue.Enqueue(tcs);
            return await tcs.Task;
        }

        public void Notify(long key)
        {
            if (m_dict.TryGetValue(key, out LockQueue lockQueue) == false)
            {
                Log.Error($"");
                return;
            }

            if (lockQueue.Count == 0)
            {
                m_dict.Remove(key);
                lockQueue.Dispose();
                return;
            }

            ETTaskCompletionSource<CoroutineLock> tcs = lockQueue.Dequeue();
            tcs.SetResult(Root.Factory.Create<CoroutineLock, long>(key));
        }
    }
}
