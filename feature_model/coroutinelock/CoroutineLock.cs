﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class CoroutineLock : Component, IAwake<long>
    {
        private long key { get; set; }

        public void Awake(long key)
        {
            this.key = key;
        }

        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }
            base.Dispose();

            Context.Game.GetComponent<CoroutineLockComponent>().Notify(this.key);
        }
    }
}
