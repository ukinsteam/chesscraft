﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.AI.Common
{
    /// <summary>
    /// 棋盘
    /// </summary>
    public class ChessBoardEntity : Entitas
    {
        private Dictionary<uint, ChessPieceEntity> m_dict = new Dictionary<uint, ChessPieceEntity>();

        public ChessPieceEntity this[uint id]
        {
            get
            {
                if (m_dict.ContainsKey(id) == false)
                {
                    return null;
                }
                return m_dict[id];
            }
        }

        public ChessPieceEntity this[UVector2Int point]
        {
            get
            {
                foreach (var pair in m_dict)
                {
                    if (pair.Value.CurrPos.Equals(point) == true)
                    {
                        return pair.Value;
                    }
                }
                return null;
            }
        }

        public void Add(uint id, ChessPieceEntity chess_piece)
        {
            m_dict.Add(id, chess_piece);
        }

        public void Remove(uint id)
        {
            if (m_dict.ContainsKey(id) == false)
            {
                return;
            }
            m_dict[id].Dispose();
            m_dict.Remove(id);
        }

        public void RemoveAll()
        {
            foreach (var pair in m_dict)
            {
                pair.Value.Dispose();
            }

            m_dict.Clear();
        }

        public bool IsAvailable(uint id)
        {
            if (m_dict.ContainsKey(id) == false || m_dict[id] == null || m_dict[id].IsAvailable == false)
            {
                return false;
            }

            return true;
        }

        public List<ChessPieceEntity> FindAllChessman(int faction)
        {
            List<ChessPieceEntity> list = new List<ChessPieceEntity>();
            foreach (var pair in m_dict)
            {
                if (pair.Value.Faction == faction)
                {
                    list.Add(pair.Value);
                }
            }
            return list;
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }

            base.Dispose();

            RemoveAll();
        }
    }
}
