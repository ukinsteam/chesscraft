﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.AI.Common
{
    /// <summary>
    /// 棋子
    /// </summary>
    public class ChessPieceEntity : Entitas
    {
        public ChessBoardEntity Board { get; set; }

        public UVector2Int CurrPos { get; private set; }

        public bool IsAvailable { get; set; }

        public int Faction { get; set; }

        public void SetPos(UVector2Int pos)
        {
            CurrPos = pos;
        }
    }
}
