﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：AbstractEvaluator
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/11 21:49:50
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace ETT.AI.ZhongGuoXiangQi
{
    public abstract class AbstractEvaluator
    {
        public abstract int Evaluator();
    }
}
