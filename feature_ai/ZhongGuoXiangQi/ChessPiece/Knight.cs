﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    /// <summary>
    /// 马 类
    /// </summary>
    public class Knight : AbstractChessPiece
    {
        public override EChessPieceType Chessmantype => EChessPieceType.Knight;

        private static readonly UVector2Int[] m_vecArr = new UVector2Int[] {
            new UVector2Int(2, -1),
            new UVector2Int(2, 1),
            new UVector2Int(1, 2),
            new UVector2Int(-1, 2),
            new UVector2Int(-2, 1),
            new UVector2Int(-2, -1),
            new UVector2Int(-1, -2),
            new UVector2Int(1, -2)
        };

        private readonly List<UVector2Int> m_points = new List<UVector2Int>();

        private bool CheckRange(UVector2Int point)
        {
            if (point.x < 0 || point.x > 8 || point.y < 0 || point.y > 9)
            {
                return false;
            }
            return true;
        }

        private bool CheckPoint(UVector2Int currpoint, UVector2Int vec)
        {
            UVector2Int point = currpoint + vec;
            if (CheckRange(point) == false)
            {
                return false;
            }
            ChessPieceEntity chessman;

            //检查马脚
            UVector2Int eyepoint = currpoint + new UVector2Int(vec.x / 2, vec.y / 2);
            chessman = Board[eyepoint];
            if (chessman != null)
            {
                return false;
            }

            chessman = Board[point];
            if (chessman != null && chessman.Faction == ChessPiece.Faction)
            {
                return false;
            }

            return true;
        }


        public override List<UVector2Int> FindPoints()
        {
            m_points.Clear();

            for (int i = 0; i < m_vecArr.Length; ++i)
            {
                if (CheckPoint(CurrPos, m_vecArr[i]) == true)
                {
                    m_points.Add(CurrPos + m_vecArr[i]);
                }
            }

            return m_points;
        }

        public override bool CanKill(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            UVector2Int vec = point - CurrPos;
            for (int i = 0; i < m_vecArr.Length; i++)
            {
                if (m_vecArr[i].Equals(vec))
                {
                    //检查马脚
                    UVector2Int eyepoint = CurrPos + new UVector2Int(vec.x / 2, vec.y / 2);
                    ChessPieceEntity chessman = Board[eyepoint];
                    if (chessman == null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
