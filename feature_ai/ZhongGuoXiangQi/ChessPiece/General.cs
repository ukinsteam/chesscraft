﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    /// <summary>
    /// 帅，将 类
    /// </summary>
    public class General : AbstractChessPiece
    {
        public override EChessPieceType Chessmantype => EChessPieceType.General;

        private static readonly UVector2Int[] m_vecArr = new UVector2Int[] {
            new UVector2Int(1, 0),
            new UVector2Int(-1, 0),
            new UVector2Int(0, 1),
            new UVector2Int(0, -1)
        };

        private readonly List<UVector2Int> m_points = new List<UVector2Int>();

        private bool CheckRange(UVector2Int point)
        {
            if (point.x < 3 || point.x > 5)
            {
                return false;
            }
            if (Faction == (int)EChessPieceFaction.RED && (point.y < 0 || point.y > 2))
            {
                return false;
            }
            if (Faction == (int)EChessPieceFaction.BLACK && (point.y < 7 || point.y > 9))
            {
                return false;
            }
            return true;
        }

        private bool CheckPoint(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            ChessPieceEntity chessman = Board[point];
            if (chessman != null && chessman.Faction == (int)Faction)
            {
                return false;
            }

            EChessPieceFaction enemyFaction = EChessPieceFaction.BLACK;
            if (Faction == (int)EChessPieceFaction.BLACK)
            {
                enemyFaction = EChessPieceFaction.RED;
            }

            var enemeyArr = Board.FindAllChessman((int)enemyFaction);

            foreach (var enemy in enemeyArr)
            {
                if (enemy == null)
                {
                    continue;
                }
                var agent = enemy.GetComponentInHerit<IAbstractChessPieceEx>();

                if (agent.Chessmantype == EChessPieceType.General)
                {
                    var enemygeneral = agent.ChessPiece as General;
                    if (enemygeneral.CanKillGeneral(point) == true)
                    {
                        return false;
                    }
                }
                else
                {
                    if (agent.CanKill(point) == true)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public override List<UVector2Int> FindPoints()
        {
            m_points.Clear();

            for (int i = 0; i < m_vecArr.Length; ++i)
            {
                if (CheckPoint(CurrPos + m_vecArr[i]) == true)
                {
                    m_points.Add(CurrPos + m_vecArr[i]);
                }
            }

            return m_points;
        }

        public override bool CanKill(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            UVector2Int vec = point - CurrPos;
            for (int i = 0; i < m_vecArr.Length; i++)
            {
                if (m_vecArr[i].Equals(vec))
                {
                    return true;
                }
            }

            return false;
        }

        private bool CanKillGeneral(UVector2Int point)
        {
            UVector2Int vec = point - CurrPos;
            if (vec.x == 0)
            {
                int degree = 0;
                UVector2Int unitvec = new UVector2Int();
                degree = vec.y > 0 ? vec.y : -vec.y;
                unitvec.x = 0;
                unitvec.y = vec.y > 0 ? 1 : -1;

                for (int i = 1; i < degree; ++i)
                {
                    UVector2Int eyepoint = CurrPos;
                    eyepoint += unitvec * i;
                    ChessPieceEntity chessman = Board[eyepoint];
                    if (chessman != null)
                    {
                        return false;
                    }
                }

                return true;
            }
            return false;
        }
    }
}
