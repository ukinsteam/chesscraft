﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    /// <summary>
    /// 炮，砲 类
    /// </summary>
    public class Cannon : AbstractChessPiece
    {
        public override EChessPieceType Chessmantype => EChessPieceType.Cannon;

        private static readonly UVector2Int[] m_vecArr = new UVector2Int[] {
            new UVector2Int(1, 0),
            new UVector2Int(-1, 0),
            new UVector2Int(0, 1),
            new UVector2Int(0, -1)
        };

        private readonly List<UVector2Int> m_points = new List<UVector2Int>();

        private bool CheckRange(UVector2Int point)
        {
            if (point.x < 0 || point.x > 8 || point.y < 0 || point.y > 9)
            {
                return false;
            }
            return true;
        }

        private bool CheckPoint(UVector2Int point, ref bool iscover, ref bool iscover2)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }
            ChessPieceEntity chessman = Board[point];
            if (iscover == false)
            {
                if (chessman != null)
                {
                    iscover = true;
                    return false;
                }
                return true;
            }
            else
            {
                if (chessman == null)
                {
                    return false;
                }
                iscover2 = true;
                if (chessman.Faction == (int)Faction)
                {
                    return false;
                }
                return true;
            }
        }

        public override List<UVector2Int> FindPoints()
        {
            m_points.Clear();

            for (int i = 0; i < m_vecArr.Length; ++i)
            {
                bool iscover = false;
                bool iscover2 = false;
                for (int scale = 1; scale < 8; scale++)
                {
                    if (CheckPoint(CurrPos + m_vecArr[i] * scale, ref iscover, ref iscover2) == true)
                    {
                        m_points.Add(CurrPos + m_vecArr[i] * scale);
                    }
                    if (iscover == true && iscover2 == true)
                    {
                        break;
                    }
                }
            }
            return m_points;
        }

        public override bool CanKill(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            UVector2Int vec = point - CurrPos;
            if (IsOneDimensionalVec(vec) == false)
            {
                return false;
            }

            int degree = 0;
            UVector2Int unitvec = new UVector2Int();
            if (vec.x == 0)
            {
                degree = vec.y > 0 ? vec.y : -vec.y;
                unitvec.x = 0;
                unitvec.y = vec.y > 0 ? 1 : -1;
            }
            else
            {

                degree = vec.x > 0 ? vec.x : -vec.x;
                unitvec.x = vec.x > 0 ? 1 : -1;
                unitvec.y = 0;
            }

            int cnt = 0;
            for (int i = 1; i < degree; ++i)
            {
                UVector2Int eyepoint = CurrPos;
                eyepoint += unitvec * i;
                ChessPieceEntity chessman = Board[eyepoint];
                if (chessman != null)
                {
                    cnt++;
                }
            }
            if (cnt != 1)
            {
                return false;
            }
            return true;
        }
    }
}
