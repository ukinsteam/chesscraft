﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ChineseChessState
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/11 21:24:51
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace ETT.AI.ZhongGuoXiangQi
{
    using PlayerDict = Dictionary<EChessPieceFaction, IAbstractChessPlayer>;

    public abstract class ChessState //: IChineseChessObject
    {
        public abstract EChessPieceFaction GetSelfFaction(uint playerid);

        /// <summary>
        /// 当前棋手
        /// </summary>
        public EChessPieceFaction Player { get { return m_player; } }
        protected EChessPieceFaction m_player;

        protected EChessPieceFaction m_winner = EChessPieceFaction.INVALID;
        public EChessPieceFaction Winner { get { return m_winner; } }

        /// <summary>
        /// 设置先手
        /// </summary>
        /// <param name="player"></param>
        public void SetFirst(EChessPieceFaction player)
        {
            m_player = player;
        }

        private PlayerDict m_playerdict = new PlayerDict();
        public IAbstractChessPlayer GetPlayer(EChessPieceFaction faction)
        {
            if (m_playerdict.ContainsKey(faction) == true)
            {
                return m_playerdict[faction];
            }
            return null;
        }

        public IAbstractChessPlayer GetPlayer(uint playerid)
        {
            foreach (var pair in m_playerdict)
            {
                if (pair.Value.Id == playerid)
                {
                    return pair.Value;
                }
            }
            return null;
        }

        public void SetPlayer(IAbstractChessPlayer player, EChessPieceFaction faction)
        {
            if (player == null)
            {
                throw new Exception("ChineseChessEngine.SetPlayer Param Invalid");
            }
            if (m_playerdict.ContainsKey(faction) == true)
            {
                throw new Exception("ChineseChessEngine.SetPlayer Key Duplicate");
            }
            player.Faction = faction;
            m_playerdict.Add(faction, player);
        }


        //protected AbstractChessBoard m_board;
        //public AbstractChessBoard Board { get { return m_board; } }
        //public void SetBoard(AbstractChessBoard board)
        //{
        //    m_board = board;
        //    m_board.State = this;
        //}

        private bool m_isGameOver = false;
        public bool IsGameOver()
        {
            return m_isGameOver;
        }

        public void SwitchPlayer()
        {
            if (m_player == EChessPieceFaction.INVALID)
            {
                return;
            }

            if (m_playerdict.ContainsKey(m_player) == true)
            {
                m_playerdict[m_player].OffTurn();
            }

            var red = EChessPieceFaction.RED;
            var black = EChessPieceFaction.BLACK;
            m_player = m_player == red ? black : red;

            if (m_playerdict.ContainsKey(m_player) == true)
            {
                m_playerdict[m_player].OnTurn();
            }
        }

        public void Start()
        {
            m_isGameOver = false;
            //m_board.Start();
            //m_board.OnFinishEvent += OnFinishHandler;
        }

        public void End()
        {
            //m_board.End();
            //m_board.OnFinishEvent -= OnFinishHandler;
        }

        //private void OnFinishHandler(IAbstractChessPiece attacker, IAbstractChessPiece defender)
        //{
        //    if (defender.Chessmantype == EChessPieceType.General)
        //    {
        //        m_isGameOver = true;
        //        m_winner = attacker.Faction;
        //    }
        //}
    }
}
