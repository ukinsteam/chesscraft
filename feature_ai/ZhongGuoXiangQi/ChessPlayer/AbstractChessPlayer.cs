﻿#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：ChessPlayer
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/11 20:58:05
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace ETT.AI.ZhongGuoXiangQi
{
    public abstract class AbstractChessPlayer
    {
        //public object Agent;

        public abstract void OnTurn();

        public abstract void OffTurn();
    }
}
