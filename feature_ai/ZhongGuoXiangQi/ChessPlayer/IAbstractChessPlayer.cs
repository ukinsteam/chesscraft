﻿#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：IChessPlayer
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/16 10:21:18
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace ETT.AI.ZhongGuoXiangQi
{
    public interface IAbstractChessPlayer
    {
        uint Id { get; }

        EChessPieceFaction Faction { get; set; }

        void OnTurn();

        void OffTurn();
    }
}
