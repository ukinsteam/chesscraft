﻿/// Generator finalColor for HighQuality-Standard
float4 cDiff = tex2D(_MainTex,i.texcoord);
clip(cDiff.a - 0.1);
fixed4 cSSS = tex2D(_SSSTex, i.texcoord);
fixed4 cILM = tex2D(_LitTex, i.texcoord);

half NdotL = CelStyle_GetDiffuseLightRatio(i.worldNormal, i.worldLightDir);
NdotL = NdotL*cILM.g;
//过滤阴影与AO
float aoMask = step(NdotL, _ShadowThreshold);
float gradient1 = step(NdotL, _ShadowThreshold2);
cDiff.rgb = aoMask * gradient1 * cSSS.rgb * cSSS.a + aoMask * (1 - gradient1) * cSSS.rgb + (1 - aoMask) * cDiff.rgb;
// add inner lines
float innerLine = step(_InnerLineThreshold, cILM.r);
fixed4 innerLineC = innerLine * fixed4(1, 1, 1, 1) + (1 - innerLine) * _InnerLineColor;
cDiff.rgb = cDiff.rgb * innerLineC *  _Shininess;

half3 h = normalize(i.worldLightDir + worldViewDir);
fixed diff = max(0, dot(i.worldNormal, i.worldLightDir));
float nh = max(0, dot(i.worldNormal, h));
float spec = (1 - aoMask) * step(_SpecularRange, cILM.b) * pow(nh, 128.0 * _SpecularBrightness) * _SpecularPower;
cDiff.rgb = cDiff.rgb + spec *_SpecularColor.rgb;
float4 finalColor = float4(cDiff.rgb, _Alpha);

