#ifndef CELSTYLE_LIGHTING_CGINC
#define CELSTYLE_LIGHTING_CGINC

#include "UnityCG.cginc"  
#include "AutoLight.cginc"
#include "Lighting.cginc"

#define DEFINE_STAND_V2F                 \
        float4 pos         :SV_POSITION; \
        float2 texcoord    :TEXCOORD1;   

#define DEFINE_STAND_LIGNT_V2F             \
        DEFINE_STAND_V2F                   \
        float3 worldNormal   :TEXCOORD2;   \
        float3 worldPos      :TEXCOORD3;   \
        float3 worldLightDir :TEXCOORD4;   

#define CELSTYLE_STAND_PARAMS              \
        sampler2D  _LitTex;                \
        sampler2D  _MainTex;               \
        float4     _MainTex_ST;            \
        float4     _DiffuseColor;          \
        float      _Shininess;             \
        float      _Alpha;                 \

#ifdef DEFINE_CELSTYLE_STAND_PARAMS
    CELSTYLE_STAND_PARAMS
#endif

#ifdef DEFINE_CELSTYLE_EDGE_HIGHLIGHT
    float     _EdgeHighLightRatio;
   // sampler2D _EdgeHighLightRampSampler; 
    float4    _EdgeHighLightColor;
    float     _EdgeHighLightThreshold;
#endif

#ifdef DEFINE_CELSTYLE_SPECULAR_LIGHT
    sampler2D _SpecularReflectMaskTex;
    float4    _SpecularReflectMaskTex_ST;
    float     _SpecularPower;
    float4    _SpecularColor;
    float     _SpecularBrightness;
    float     _SpecularRange;
#endif


#define CELSTYLE_SHADOW_PARAMS          \
float   _ShadowThreshold;               \
float4  _ShadowColor;                   \
float   _ShadowSharpness;


#ifdef DEFINE_CELSTYLE_SHADOW_PARAMS
    CELSTYLE_SHADOW_PARAMS
#endif


#ifdef DEFINE_CELSTYLE_HAIR
     sampler2D _HairLightRamp;
     float4    _HairLightRamp_ST;
     float4    _HairSpecularColor;
     fixed     _HairSpecularRange;
     fixed     _MainHairSpecularSmooth;
     fixed     _MainHairSpecularOffset;
     fixed     _FuHairSpecularSmooth;
     fixed     _FuHairSpecularOffset;
     fixed     _FresnelScale;
#endif

#define HANDLE_STAND_V2F                                                       \
            o.pos=UnityObjectToClipPos(v.vertex);                              \
            o.texcoord=TRANSFORM_TEX(v.texcoord, _MainTex);                  
           
#define HANDLE_STAND_LIGHT_V2F_VERT                                              \
            HANDLE_STAND_V2F                                                     \
            o.worldNormal = UnityObjectToWorldNormal(v.normal);                  \
            o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;                 \
            o.worldLightDir = UnityWorldSpaceLightDir(o.worldPos);     

#define HANDLE_STAND_LIGHT_V2F_FRAG                                              \
            i.worldNormal = normalize(i.worldNormal);                            \
            i.worldLightDir = normalize(i.worldLightDir);                        \
            float3 worldViewDir = normalize(UnityWorldSpaceViewDir(i.worldPos.xyz));                            



inline half CelStyle_Luminance( half3 c )
{
    return dot( c, half3(0.22, 0.707, 0.071) );
}

inline half CelStyle_SpecularReflect( half3 normal, half3 lightDir, half3 viewDir, half s )
{
    return saturate(pow(saturate(dot(normal, normalize(lightDir + viewDir))), s));
}

// same coord space
inline float CelStyle_GetDiffuseLightRatio( float3 normal, float3 lightDir )
{
    float diff = saturate(0.5 * (dot(normal,lightDir) + 1.0));
    return diff;
}

// same coord space
inline float4 CelStyle_GetDiffuseLightColor( float3 normal, float3 lightDir )
{
    float diff = CelStyle_GetDiffuseLightRatio(normal,lightDir);
    float4 color = _DiffuseColor * _LightColor0 * diff * _Shininess;
    return color;
}


#ifdef DEFINE_CELSTYLE_SHADOW_PARAMS
inline float4 CelStyle_GetRampShadowColor(float3 normal, float3 lightDir)
{
    //UNITY_LIGHT_ATTENUATION(atten, i, i.worldPos);//阴影值计算  
    // float4  lightColor = _LightColor0 * atten;

    float diff = dot(lightDir, normal) * 0.5 + 0.5;
    //ToonMapping
    ///                          Tex                    Tex
    float shadowRate = abs( max( -1.0, ( min( diff, _ShadowThreshold ) -_ShadowThreshold)*_ShadowSharpness ) )*_ShadowColor.a;
    float4 shadowToon = float4(1,1,1,1) * (1-shadowRate) +  _ShadowColor *shadowRate;

    //Output
    //float4 color = saturate( _Color * lightColor*2 ) * _Color;
    //float4 color =  (lightColor)  * (atten*2)  * c ;
    return shadowToon;
               
}

inline float4 CelStyle_GetDiffAndShadowColor(float3 normal, float3 lightDir, float2 uv, float4 mixColor)
{
    float ratio = CelStyle_GetDiffuseLightRatio(lightDir, normal);
    float shading = tex2D(_LitTex, uv).g;
    float diff = ratio * shading;

    float shadowRate = abs( max( -1.0, ( min( diff, _ShadowThreshold ) -_ShadowThreshold)*_ShadowSharpness ) )*_ShadowColor.a;
    float4 shadowToon = _Shininess * mixColor * (1-shadowRate) +  _ShadowColor *shadowRate;
    return shadowToon;
}

#endif

#ifdef DEFINE_CELSTYLE_EDGE_HIGHLIGHT

inline float4 CelStyle_GetEdgeHighLight(float3 normal, float3 lightDir, float3 viewDir)
{
    float falloffRatio = clamp(1.0-abs(dot(normal,viewDir)), 0.02, 0.98);
    falloffRatio = pow(falloffRatio, _EdgeHighLightRatio);//折射值  
    float diffRatio = CelStyle_GetDiffuseLightRatio(normal, lightDir);
    falloffRatio = saturate( diffRatio * falloffRatio );
    //FIXME: combine with other ramp texture later
    //falloffRatio = tex2D( _EdgeHighLightRampSampler, float2(falloffRatio, falloffRatio)).r;
    falloffRatio = falloffRatio > _EdgeHighLightThreshold?1:0;
    return falloffRatio * _EdgeHighLightColor;
} 

#endif

#ifdef DEFINE_CELSTYLE_SPECULAR_LIGHT

inline float4 CelStyle_GetSpacularColor(float3 normal, float3 lightDir, float3 viewDir, float2 texCoord, float4 mixColor) 
{
    float4 reflectionMaskColor = tex2D(_SpecularReflectMaskTex, texCoord);
    float specularDot = dot(normal, viewDir);
    //float spec = saturate(lit( specularDot, specularDot, _SpecularPower ).z); // trick from UnityChen
    float spec = CelStyle_SpecularReflect(normal, lightDir, viewDir, _SpecularPower);
    float4 specularColor = spec * reflectionMaskColor * mixColor;
    return specularColor;
}

inline float4 CelStyle_GetCelSpacularColor(float3 normal, float3 lightDir, float3 viewDir, float texB)
{
    half NdotL = CelStyle_GetDiffuseLightRatio(normal, lightDir);
    NdotL = NdotL - _SpecularBrightness;
    float spec = CelStyle_SpecularReflect(normal, lightDir, viewDir, _SpecularPower);
    spec = spec *  (NdotL) *  dot(viewDir, normal); //my trick
    spec -= _SpecularRange;
    spec = spec > 0.05?spec:0;
    //spec = spec > specularStrength? spec:0;
    float sr = texB > 0.4 ? texB:0;
    float4 specularColor = sr *spec *_SpecularColor;
    return specularColor;
}


inline float4 CelStyle_GetDiffShadowSpecularColor(float3 normal, float3 lightDir, float3 viewDir, float2 uv, float4 mixColor)
{
    float4 shading = tex2D(_LitTex,uv);
    float ratio = CelStyle_GetDiffuseLightRatio(lightDir, normal);
    float diff = ratio * shading.g;
    //float diff = ratio - (1- shading.g);
    float shadowRate = abs( max( -1.0, ( min( diff, _ShadowThreshold ) -_ShadowThreshold)*_ShadowSharpness ) )*_ShadowColor.a;
    float4 shadowToon = _Shininess * mixColor * (1-shadowRate) +  _ShadowColor *shadowRate;

    float4 specularColor = CelStyle_GetCelSpacularColor(normal, lightDir, viewDir, shading.b);;
    return specularColor + shadowToon ;
}


#endif

#ifdef DEFINE_CELSTYLE_HAIR
float hairSpecular(fixed3 halfDir, float3 tangent, float specularSmooth)  
{  
    float dotTH = dot(tangent, halfDir);  
    float sqrTH = max(0.01,sqrt(1 - pow(dotTH, 2)));  
    float atten = smoothstep(-1,0, dotTH);  
    float specMain = atten * pow(sqrTH, specularSmooth);  
    return specMain;  
}


inline float4 CelStyle_GetHairSpacularColor(float3 normal, float3 tangent, float3 viewDir, float3 lightDir, float2 hairUV,  float4 mixColor) 
{
    float3 mainHairOffset = tex2D(_HairLightRamp, hairUV).xyz;

    float3 halfDir = normalize(viewDir + lightDir);
    float3 mainSpecOff = tangent + mainHairOffset;
    float3 fuSpecOff = tangent + mainHairOffset;
    float hairSpecMain = hairSpecular(halfDir, mainSpecOff, _MainHairSpecularSmooth*100 ) * tex2D(_HairLightRamp, hairUV);
    float hairSpecFu = hairSpecular(halfDir, fuSpecOff, _FuHairSpecularSmooth*100 ) * tex2D(_HairLightRamp, hairUV);

    float fresne = _FresnelScale + (1 - _FresnelScale) * pow( 1 - dot(viewDir, normal),5);


    float4 hairSpecFinal = ( hairSpecMain+ hairSpecFu) * mixColor + fresne;

    return hairSpecFinal;
}


#endif


#endif // CELSTYLE_LIGHTING_CGINC
