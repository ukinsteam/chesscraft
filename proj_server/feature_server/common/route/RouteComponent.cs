﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.Master;

namespace ETT.Server
{
    public class RouteComponent : Component, IStart, IUpdate
    {
        public void Start()
        {
            Link2Master().Coroutine();
        }

        public void Update()
        {

        }

        private async ETVoid Link2Master()
        {
            var network = Entitas.GetComponent<NetworkInnerComponent>();

            var yaml = Context.Game.GetComponent<Yaml.YamlComponent>();

            var master_session = network.Get(ServerDefine.ToIPEndPoint(yaml.ServerConf.Master_Addr));

            var peer = Root as IAbstractPeer;

            var router_response = (RouterResponse)await master_session.Call(new RouterRequest()
            {
                ActorType = peer.PeerType,
                AccessAddr = peer.InnerAddr
            });

            peer.RootId = router_response.PeerId;

            var route_table = Context.Game.GetComponent<RouteTableComponent>();
            for (int i = 0; i < router_response.PeerList.Count; ++i)
            {
                route_table.AddActor(router_response.PeerList[i]);
            }

            Log.Debug($"Route Succ; ServerName = {peer.PeerType}, PeerId = {peer.RootId}");
        }
    }
}
