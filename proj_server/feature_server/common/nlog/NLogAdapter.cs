﻿using ETT.Model.log;
using NLog;
using System;

namespace ETT.Server
{
    public class NLogAdapter : ILog
    {
        private readonly Logger logger = LogManager.GetLogger("Logger");

        public void Debug(string message)
        {
            logger.Debug(message);
            Console.WriteLine(message);
        }

        public void Debug(string message, params object[] args)
        {
            logger.Debug(message, args);
            Console.WriteLine(message, args);
        }

        public void Error(string message)
        {
            logger.Error(message);
            Console.WriteLine(message);
        }

        public void Error(string message, params object[] args)
        {
            logger.Error(message, args);
            Console.WriteLine(message, args);
        }

        public void Fatal(string message)
        {
            logger.Fatal(message);
            Console.WriteLine(message);
        }

        public void Fatal(string message, params object[] args)
        {
            logger.Fatal(message, args);
            Console.WriteLine(message, args);
        }

        public void Info(string message)
        {
            logger.Info(message);
            Console.WriteLine(message);
        }

        public void Info(string message, params object[] args)
        {
            logger.Info(message, args);
            Console.WriteLine(message, args);
        }

        public void Trace(string message)
        {
            logger.Trace(message);
            Console.WriteLine(message);
        }

        public void Trace(string message, params object[] args)
        {
            logger.Trace(message, args);
            Console.WriteLine(message, args);
        }

        public void Warning(string message)
        {
            logger.Warn(message);
            Console.WriteLine(message);
        }

        public void Warning(string message, params object[] args)
        {
            logger.Warn(message, args);
            Console.WriteLine(message, args);
        }
    }
}
