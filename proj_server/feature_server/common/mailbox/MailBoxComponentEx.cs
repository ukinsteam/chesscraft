﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Server.Location;

namespace ETT.Server
{

    public static class MailBoxComponentEx 
    {
        public static async ETTask AddLocation(this MailBoxComponent mailBox)
        {
            await mailBox.Root.GetComponent<LocationProxyComponent>().Add(mailBox.Entitas.Id, mailBox.Entitas.InstanceId);
        }

        public static async ETTask RemoveLocation(this MailBoxComponent mailBox)
        {
            await mailBox.Root.GetComponent<LocationProxyComponent>().Remove(mailBox.Entitas.Id);
        }

    }
}
