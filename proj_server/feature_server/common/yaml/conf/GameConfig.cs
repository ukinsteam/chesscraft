﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Yaml
{
    public sealed class GameConfig
    {
        public List<string> Games { get; set; }
    }
}
