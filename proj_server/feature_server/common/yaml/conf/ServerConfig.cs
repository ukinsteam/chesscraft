﻿using System.Collections.Generic;

namespace ETT.Server.Yaml
{
    public sealed class Address
    {
        public string Type { get; set; }
        public string Inner { get; set; }
        public string Outer { get; set; }
    }

    public sealed class ServerConfig
    {
        public string Res_Path { get; set; }

        public string Master_Addr { get; set; }

        public Dictionary<string, Address> Server { get; set; }
    }
}
