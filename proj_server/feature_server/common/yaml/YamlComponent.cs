﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using YamlDotNet.Serialization;

namespace ETT.Server.Yaml
{
    public class YamlComponent : Component, IAwake<string>
    {
        public ServerConfig ServerConf { get; private set; }
        public GameConfig GameConf { get; private set; }

        public void Awake(string path)
        {
            using (StreamReader reader = new StreamReader(Path.Combine(path, "../../../../yaml/server.yml")))
            {
                var deserializer = new Deserializer();
                ServerConf = deserializer.Deserialize<ServerConfig>(reader);
                reader.Close();
            }
            using (StreamReader reader = new StreamReader(Path.Combine(path, "../../../../yaml/game.yml")))
            {
                var deserializer = new Deserializer();
                GameConf = deserializer.Deserialize<GameConfig>(reader);
                reader.Close();
            }
        }
    }
}
