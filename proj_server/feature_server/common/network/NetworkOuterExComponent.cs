﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.Gate;
using ETT.Server.Location;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ETT.Server
{
    public sealed class NetworkOuterExComponent : NetworkOuterComponent
    {

        public override void Dispatch(Session session, uint proto_id, IProtoPacker message)
        {
            DispatchAsync(session, proto_id, message).Coroutine();
        }

        public async ETVoid DispatchAsync(Session session, uint proto_id, IProtoPacker message)
        {
            switch (message)
            {
                case IActorLocationRequest actor_request:
                    {
                        long unit_id = session.GetComponent<PlayerOnSessionComponent>().Player.UnitId;
                        ActorLocationSender sender = Root.GetComponent<ActorLocationSenderComponent>().Get(unit_id);

                        int rpc_id = actor_request.RpcId;
                        long instance_id = session.InstanceId;
                        IResponse response = await sender.Call(actor_request);
                        response.RpcId = rpc_id;
                        if (session.InstanceId == instance_id)
                        {
                            session.Reply(response);
                        }
                    }
                    break;
                case IActorLocationMessage actor_message:
                    {
                        long unit_id = session.GetComponent<PlayerOnSessionComponent>().Player.UnitId;
                        ActorLocationSender sender = Root.GetComponent<ActorLocationSenderComponent>().Get(unit_id);
                        sender.Send(actor_message).Coroutine();
                    }
                    break;
                default:
                    {
                        var dispatcher = Entitas.GetComponent<MessageDispatchComponent>();
                        dispatcher.Handle(session, proto_id, message);
                    }
                    break;
            }
        }
    }
}