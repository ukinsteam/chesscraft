﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Server.Table
{
    public abstract class AbstractTableParser
    {
        public bool Processing { get; protected set; }

        public abstract void Parse();

        //public abstract void Parse(string name);
    }
}
