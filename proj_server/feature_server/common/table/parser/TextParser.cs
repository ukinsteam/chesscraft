﻿using ETT.Model;
using ETT.Table;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.Table
{
    public class TextParser : AbstractTableParser
    {
        public readonly TableComponent TableMgr;
        public TextParser(TableComponent table_mgr)
        {
            TableMgr = table_mgr;
        }

        private void ParseTextContent(IAbstractTable t, List<string> assets)
        {
            CSVDataTable dt = new CSVDataTable(t.TableVO);
            foreach (var content in assets)
            {
                var subdt = new CSVDataTable(t.TableVO);
                subdt.Parse(content);
                dt.DataTable.Union(subdt.DataTable);
            }

            t.CSVDT = dt;

            MemoryStream stream = new MemoryStream();
            BinaryWriter bw = new BinaryWriter(stream);
            dt.DataTable.Build(bw);
            stream.Flush();
            byte[] buffer = stream.GetBuffer();
            stream.Close();
            stream.Close();

            MemoryStream stream2 = new MemoryStream(buffer);
            BinaryReader br = new BinaryReader(stream2);
            t.Deserialize(br);
        }


        public override void Parse()
        {
            var table_list = TableMgr.FindAll();
            if (table_list == null || table_list.Count == 0)
            {
                return;
            }

            Processing = true;

            for (int i = 0; i < table_list.Count; ++i)
            {
                var m_table = table_list[i];
                List<string> res_list = new List<string>();
                for (int j = 0; j < m_table.Path.Count; ++j)
                {
                    var obj = File.ReadAllText(Path.Combine(TableMgr.ResPath, $"{m_table.Path[j]}.csv"));
                    res_list.Add(obj);
                }
                ParseTextContent(m_table, res_list);
            }

            Processing = false;
        }

        //public override void Parse(string name)
        //{
        //    var m_table = TableMgr.Find(name);
        //    if (m_table == null)
        //    {
        //        Log.Error($"name 不存在！");
        //        return;
        //    }
        //    List<string> res_list = new List<string>();
        //    for (int j = 0; j < m_table.Path.Count; ++j)
        //    {
        //        var obj = File.ReadAllText(Path.Combine(TableMgr.ResPath, $"{m_table.Path[j]}.csv"));
        //        res_list.Add(obj);
        //    }
        //    ParseTextContent(m_table, res_list);
        //}
    }
}
