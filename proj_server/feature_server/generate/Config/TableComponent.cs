using ETT.Server.Table;
using ETT.Model;

namespace ETT.Server
{
	public partial class TableComponent
	{
		private void InnerInit()
		{
			m_list.Add(Context.Table.AddComponent<WndTable>());
			m_list.Add(Context.Table.AddComponent<SceneTable>());
			m_list.Add(Context.Table.AddComponent<LocalizeTable>());
			m_list.Add(Context.Table.AddComponent<ErrCodeTable>());
			m_list.Add(Context.Table.AddComponent<ZhongGuoXiangQi.ChessmanTable>());
			m_list.Add(Context.Table.AddComponent<WuZiQi.ChessmanTable>());
			m_list.Add(Context.Table.AddComponent<ZhongGuoXiangQi.FootholdTable>());
			m_list.Add(Context.Table.AddComponent<WuZiQi.FootholdTable>());
			m_list.Add(Context.Table.AddComponent<ProtocolTable>());
			m_list.Add(Context.Table.AddComponent<ShieldwordTable>());
			m_list.Add(Context.Table.AddComponent<TipsTable>());
			m_list.Add(Context.Table.AddComponent<ModelTable>());
			m_list.Add(Context.Table.AddComponent<GameKindTable>());
		}
	}
}
