using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.Table
{
	public class ProtocolTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public string Desc { get; private set; }

		public EActor From { get; private set; }

		public EActor To { get; private set; }

		public EProtoStyle Style { get; private set; }

		public bool IsActor { get; private set; }

		public ProtoParam[] Parameter { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Desc = br.ReadString();

				From = (EActor)Enum.Parse(typeof(EActor), br.ReadString());

				To = (EActor)Enum.Parse(typeof(EActor), br.ReadString());

				Style = (EProtoStyle)Enum.Parse(typeof(EProtoStyle), br.ReadString());

				IsActor = br.ReadBoolean();

				int m_parameter_cnt = br.ReadInt32();
				Parameter = new ProtoParam[m_parameter_cnt];
				for (int i = 0; i < m_parameter_cnt; ++i)
				{
					Parameter[i] = new ProtoParam();
					Parameter[i].Deserialize(br);
				}
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class ProtocolTable : SingleKeyTable<ProtocolTableConf,uint>
	{
		public override string Name { get { return "ProtocolTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"common/common/table/protocol",
			@"zhongguoxiangqi/generic/table/protocol",
			@"zhongguoxiangqi/battle/table/protocol",
			@"wuziqi/generic/table/protocol",
			@"wuziqi/battle/table/protocol",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ProtocolTable' >
						<field name='Id' type='uint' primarykey='true' desc='协议id' />
						<field name='Name' type='string' desc='协议名称' />
						<field name='Desc' type='string' desc='协议描述' />
						<field name='From' type='EActor' desc='协议来源' />
						<field name='To' type='EActor' desc='协议去向' />
						<field name='Style' type='EProtoStyle' desc='协议类型' />
						<field name='IsActor' type='bool' desc='是否Actor协议' />
						<field name='Parameter' type='array(ProtoParam)' isvertical='false' >
							<field name='Name' type='string' desc='参数名称' />
							<field name='Type' type='string' desc='参数类型' />
							<field name='Desc' type='string' desc='参数描述' />
						</field>
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
