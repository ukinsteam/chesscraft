using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.Table
{
	public class ModelTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public string Desc { get; private set; }

		public string Path { get; private set; }

		public float Scale { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Desc = br.ReadString();

				Path = br.ReadString();

				Scale = br.ReadSingle();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class ModelTable : SingleKeyTable<ModelTableConf,uint>
	{
		public override string Name { get { return "ModelTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"zhongguoxiangqi/generic/table/model",
			@"zhongguoxiangqi/battle/table/model",
			@"wuziqi/generic/table/model",
			@"wuziqi/battle/table/model",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ModelTable' >
						<field name='Id' type='uint' primarykey='true' desc='Id' />
						<field name='Name' type='string' desc='模型名称' />
						<field name='Desc' type='string' desc='描述' />
						<field name='Path' type='string' desc='资源路径' />
						<field name='Scale' type='float' desc='缩放比例' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
