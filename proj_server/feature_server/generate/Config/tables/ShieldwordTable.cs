using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.Table
{
	public class ShieldwordTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Word { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Word = br.ReadString();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class ShieldwordTable : SingleKeyTable<ShieldwordTableConf,uint>
	{
		public override string Name { get { return "ShieldwordTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"common/common/table/shieldword",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ShieldwordTable' >
						<field name='Id' type='uint' primarykey='true' desc='Id' />
						<field name='Word' type='string' desc='屏蔽词' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
