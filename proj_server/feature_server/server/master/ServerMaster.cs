﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.Master;

namespace ETT.Server
{
    public class ServerMaster : AbstractPeer, IAwake<string>
    {
        public override EPeerType PeerType => EPeerType.Master;

        public void Awake(string addr)
        {
            Root = this;
            InnerAddr = addr;

            var comp = AddComponent<NetworkInnerComponent, string>(addr);
            comp.SetServiceName("ServerMaster_Inner");


            AddComponent<MessageDispatchComponent>();
            AddComponent<MasterRouteComponent>();
        }
    }
}
