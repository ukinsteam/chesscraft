﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;

namespace ETT.Server.Master
{
    [MessageHandler(EPeerType.Database | EPeerType.World | EPeerType.Gate | EPeerType.Login | EPeerType.Location)]
    public class M2A_RouteHandler : AbstractMsgHandler<RouterMessage>
    {
        protected override async ETTask Run(Session session, RouterMessage msg)
        {
            if (session.Root == null)
            {
                return;
            }

            var route_comp = Context.Game.GetComponent<RouteTableComponent>();
            if (route_comp == null)
            {
                return;
            }

            //Log.Debug($"M2A_RouteHandler = {session.Peer.PeerType} addlist.cnt = {msg.AddList.Count} dellist.cnt = {msg.DelList.Count}");

            foreach (var peer in msg.AddList)
            {
                route_comp.AddActor(peer);
            }

            foreach (var peer in msg.DelList)
            {
                route_comp.RemoveActor(peer);
            }

            await ETTask.CompletedTask;
        }
    }
}
