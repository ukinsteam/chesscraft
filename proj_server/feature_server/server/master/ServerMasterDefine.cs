﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Master
{
    /// <summary>
    /// Id [1000,2000)
    /// </summary>
    public enum EServerMasterProto
    {
        Req_Route = 1000,
        Res_Route = 1001,
        Ntf_Route = 1002,
    }

    public static class ServerMasterDefine
    {
    }
}
