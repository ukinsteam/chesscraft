﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;

namespace ETT.Server.World
{
    [MessageHandler(EPeerType.World)]
    public class G2W_CreateUnitHnadler : AbstractRpcHandler<G2W_Req_CreateUnit, W2G_Res_CreateUnit>
    {
        protected override async ETTask Run(Session session, G2W_Req_CreateUnit request, W2G_Res_CreateUnit response, Action reply)
        {
            var server = session.Root;
            Unit unit = server.Factory.CreateWithId<Unit>();
            server.GetComponent<UnitManager>().Add(unit);

            //unit.AddComponent<MoveComponent>();
            //unit.AddComponent<UnitPathComponent>();
            //unit.Position = new Vector3(-10, 0, -10);

            await unit.AddComponent<MailBoxComponent>().AddLocation();

            var agent = unit.AddComponent<UnitAgentComponent>();
            agent.GateSessionActorId = request.GateSessionId;
            agent.PlayerId = request.PlayerId;
            agent.Name = request.PlayerName;

            response.UnitId = unit.Id;

            reply();
        }
    }
}
