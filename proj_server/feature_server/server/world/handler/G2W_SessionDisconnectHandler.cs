﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.World
{
    [ActorMessageHandler(EPeerType.World)]
    public class G2W_SessionDisconnectHandler : AbstractActorMsgHandler<Unit, G2W_Ntf_SessionDisconnect>
    {
        protected override async ETTask Run(Unit unit, G2W_Ntf_SessionDisconnect message)
        {
            var agent = unit.GetComponent<UnitAgentComponent>();
            agent.IsDisconnect = true;
            if (agent.Room != null)
            {
                var room = agent.Room;

                //var turn_sys = room.GetComponent<TurnBasedTeamSystem>();
                //turn_sys.re
                Log.Debug($"Remove Room id = {room.SerialId}");

                RoomManager roomMgr = room.GetParent<RoomManager>();
                roomMgr.Remove(room.SerialId);
            }


            await unit.GetComponent<MailBoxComponent>().RemoveLocation();

            UnitManager unitMgr = unit.GetParent<UnitManager>();
            if (unitMgr != null)
            {
                unitMgr.Dispose(unit.Id);
            }

            await ETTask.CompletedTask;
        }
    }
}
