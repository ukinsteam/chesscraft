﻿using ETT.Model.proto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Server.World
{
    public class UnitVO : AbstractProtoPacker
    {
        public long UnitId { get; set; }
        public float X { get; set; }
        public float Y { get; set; }
        public float Z { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(UnitId);
            writer.Write(X);
            writer.Write(Y);
            writer.Write(Z);
        }

        public override void UnPack(BinaryReader reader)
        {
            UnitId = reader.ReadInt64();
            X = reader.ReadSingle();
            Y = reader.ReadSingle();
            Z = reader.ReadSingle();
        }
    }
}
