﻿using ETT.Model;
using ETT.Model.proto;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.World
{
    [Message((int)EWorldProto.G2W_Req_GetRoomList)]
    public class G2W_Req_GetRoomListRequest : AbstractRequest
    {
        public int PageIdx { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(PageIdx);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            PageIdx = reader.ReadInt32();
        }
    }

    [Message((int)EWorldProto.W2G_Res_GetRoomList)]
    public class W2G_Res_GetRoomListResponse : AbstractResponse
    {
        public List<RoomVO> RoomList { get; private set; } = new List<RoomVO>();

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            int cnt = RoomList.Count;
            writer.Write(cnt);
            for (int i = 0; i < cnt; i++)
            {
                RoomList[i].Pack(writer);
            }
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);

            RoomList.Clear();
            int cnt = reader.ReadInt32();
            for (int i = 0; i < cnt; i++)
            {
                var vo = new RoomVO();
                vo.UnPack(reader);
                RoomList.Add(vo);
            }
        }
    }
}
