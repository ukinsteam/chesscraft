﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.Location;
using ETT.Server.World;

namespace ETT.Server
{
    public class WorldServer : AbstractPeer, IAwake<string>
    {
        public override EPeerType PeerType => EPeerType.World;

        public void Awake(string addr)
        {
            Root = this;
            InnerAddr = addr;

            var comp1 = AddComponent<NetworkInnerComponent, string>(addr);
            comp1.SetServiceName("World_Inner");

            //var comp2 = AddComponent<NetworkInnerConnComponent>();
            //comp2.SetServiceName("Game_Inner_Conn");

            AddComponent<MessageDispatchComponent>();
            AddComponent<RouteComponent>();
            AddComponent<LocationProxyComponent>();
            AddComponent<UnitManager>();
            AddComponent<ActorMessageDispatchComponent>();

            AddComponent<ActorLocationSenderComponent>();
            AddComponent<ActorMessageSenderComponent>();

            var yaml_comp = Context.Game.GetComponent<Yaml.YamlComponent>();

            var games = yaml_comp.GameConf.Games;
            for (int i = 0; i < games.Count; ++i)
            {
                switch (games[i])
                {
                    case "chinese_chess":
                        AddComponent<ZhongGuoXiangQi.ChessEntitas>();
                        break;
                    case "animal_chess":
                        AddComponent<DouShouQi.ChessEntitas>();
                        break;
                    case "flight_chess":
                        AddComponent<FeiXingQi.ChessEntitas>();
                        break;
                    case "wuzi_chess":
                        AddComponent<WuZiQi.ChessEntitas>();
                        break;
                }
            }
        }
    }
}
