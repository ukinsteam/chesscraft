﻿using ETT.Model;

namespace ETT.Server.World
{
    public class UnitAgentComponent : Component
    {
        public long GateSessionActorId;
        public long PlayerId;
        public string Name;
        public bool IsDisconnect;

        public Room Room { get; set; }

        public override void Dispose()
        {
            if(IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            Room = null;
            GateSessionActorId = 0;
            PlayerId = 0;
            Name = string.Empty;
            IsDisconnect = false;
        }
    }
}
