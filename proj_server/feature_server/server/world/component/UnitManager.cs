﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ETT.Server.World
{
    public class UnitManager : Component
    {
        private readonly Dictionary<long, Unit> m_dict = new Dictionary<long, Unit>();

        public void Add(Unit unit)
        {
            unit.Parent = this;
            m_dict.Add(unit.Id, unit);
        }

        public void Remove(long id)
        {
            m_dict.Remove(id);
        }

        public Unit Get(long id)
        {
            m_dict.TryGetValue(id, out Unit unit);
            return unit;
        }

        public int Count => m_dict.Count;

        public Unit[] GetAll() => m_dict.Values.ToArray();

        public void Dispose(long id)
        {
            var unit = Get(id);
            Remove(id);
            unit?.Dispose();
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            foreach (var pair in m_dict)
            {
                pair.Value.Dispose();
            }
            m_dict.Clear();
        }
    }
}
