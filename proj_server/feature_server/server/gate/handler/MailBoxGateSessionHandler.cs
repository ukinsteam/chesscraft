﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Model.network;
using System;

namespace ETT.Server.Gate
{
    [MailBoxHandler(EMailBoxType.GateSession)]
    public class MailBoxGateSessionHandler : IMailBoxHandler
    {
        public async ETTask Handle(Session session, Entitas entitas, object actorMessage)
        {
            try
            {
                IActorLocationMessage message = actorMessage as IActorLocationMessage;

                Session clientSession = entitas as Session;
                message.ActorId = 0;
                clientSession.Send(message);

                await ETTask.CompletedTask;
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
