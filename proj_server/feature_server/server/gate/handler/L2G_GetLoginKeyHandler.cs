﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;

namespace ETT.Server.Gate
{
    [MessageHandler(EPeerType.Gate)]
    public class L2G_GetLoginKeyHandler : AbstractRpcHandler<L2G_Req_Login, G2L_Res_Login>
    {
        protected override async ETTask Run(Session session, L2G_Req_Login request, G2L_Res_Login response, Action reply)
        {
            var server = session.Root;
            long key = RandomUtils.RandInt64();
            server.GetComponent<GateSessionKeyComponent>().Add(key, request.AccountId);
            response.key = key;
            response.gate_addr = server.GetComponent<NetworkOuterExComponent>().IPEndPoint.ToString();
            reply();
            await ETTask.CompletedTask;
        }
    }
}
