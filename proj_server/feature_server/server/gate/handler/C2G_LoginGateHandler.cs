﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;

namespace ETT.Server.Gate
{
    [MessageHandler(EPeerType.Gate)]
    public class C2G_LoginGateHandler : AbstractRpcHandler<C2G_Req_Login, G2C_Res_Login>
    {
        protected override async ETTask Run(Session session, C2G_Req_Login request, G2C_Res_Login response, Action reply)
        {
            var server = session.Root;
            long account_id = server.GetComponent<GateSessionKeyComponent>().Get(request.key);
            if (account_id == 0)
            {
                response.Error = (int)EGateErrorCode.Err_ConnectGate_InvalidKey;
                response.Message = "Gate Key 验证失败";
                goto ReplyLabel;
            }

            response.playerid = account_id;

            PlayerOnGate player = server.Factory.CreateWithId<PlayerOnGate, string>(account_id, request.AccountName);
            server.GetComponent<PlayerGroupOnGateComponent>().Add(player);
            session.AddComponent<PlayerOnSessionComponent>().Player = player;
            session.AddComponent<MailBoxComponent, EMailBoxType>(EMailBoxType.GateSession);

            ReplyLabel:
            reply();
            await ETTask.CompletedTask;
        }
    }
}
