﻿using ETT.Model;

namespace ETT.Server.Gate
{
    public sealed class PlayerOnGate : Entitas, IAwake<string>
    {
        public string Name { get; private set; }
        public long UnitId { get; set; }

        public void Awake(string t)
        {
            Name = t;
        }
    }
}
