﻿using ETT.Model;
using System.Collections.Generic;

namespace ETT.Server.Gate
{
    public class PlayerGroupOnGateComponent : Component, IAwake
    {
        private readonly Dictionary<long, PlayerOnGate> m_dict = new Dictionary<long, PlayerOnGate>();

        public void Awake()
        {
            m_dict.Clear();
        }

        public void Add(PlayerOnGate player)
        {
            m_dict.Add(player.Id, player);
        }

        public PlayerOnGate Get(long id)
        {
            m_dict.TryGetValue(id, out PlayerOnGate player);
            return player;
        }

        public void Remove(long id)
        {
            m_dict.Remove(id);
        }

        public int Count => m_dict.Count;

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            foreach (var pair in m_dict)
            {
                pair.Value.Dispose();
            }
        }
    }
}
