﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;

namespace ETT.Server
{
    public class LoginServer : AbstractPeer, IAwake<string, string>
    {
        public override EPeerType PeerType => EPeerType.Login;

        public void Awake(string addr_inner,string addr_outer)
        {
            Root = this;
            InnerAddr = addr_inner;

            var comp1 = AddComponent<NetworkInnerComponent, string>(addr_inner);
            comp1.SetServiceName("Login_Inner");

            var comp2 = AddComponent<NetworkOuterExComponent, string>(addr_outer);
            comp2.SetServiceName("Login_Outer");

            AddComponent<MessageDispatchComponent>();
            AddComponent<RouteComponent>();
            //AddComponent<DBProxyComponent>();
        }
    }
}
