﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;

namespace ETT.Server
{
    public class DatabaseServer : AbstractPeer, IAwake<string>
    {
        public override EPeerType PeerType => EPeerType.Database;

        public void Awake(string addr)
        {
            Root = this;
            InnerAddr = addr;

            var comp1 = AddComponent<NetworkInnerComponent, string>(addr);
            comp1.SetServiceName("Database_Inner");

            //var comp2 = AddComponent<NetworkInnerConnComponent, string>("Database_Inner_Conn");
            //comp2.SetServiceName("Database_Inner_Conn");

            AddComponent<MessageDispatchComponent>();
            AddComponent<RouteComponent>();
        }
    }
}
