﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;

namespace ETT.Server.Database
{
    [MessageHandler(EPeerType.Database)]
    public class L2D_LoginHandler : AbstractRpcHandler<L2D_Req_Login, D2L_Res_Login>
    {
        protected override async ETTask Run(Session session, L2D_Req_Login request, D2L_Res_Login response, Action reply)
        {
            var server_db = Context.Game.GetComponent<DatabaseServer>();
            if (server_db == null)
            {
                response.Error = 11;
                response.Message = "找不到ServerMaster服务器";
                goto ReplyLabel;
            }

            //var db_comp = server_db.GetComponent<DBComponent>();
            //if (db_comp == null)
            //{
            //    response.Error = 12;
            //    response.Message = "找不到MasterRouteComponent组件";
            //    goto ReplyLabel;
            //}

            Log.Debug("Query!!!!!!!!!!!!");

            //response.IsExist = true;
            response.AccountId = SerialUtils.GenerateId(server_db.RootId);

            await ETTask.CompletedTask;

        ReplyLabel:
            reply();

        }
    }
}
