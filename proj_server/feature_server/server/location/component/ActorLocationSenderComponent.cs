﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    public class ActorLocationSenderComponent : Component, IStart
    {
        private readonly Dictionary<long, ActorLocationSender> m_dict = new Dictionary<long, ActorLocationSender>();

        public void Start()
        {
            StartAsync().Coroutine();
        }

        // 每10s扫描一次过期的ActorLocationSender进行回收,过期时间是1分钟
        // 可能由于bug或者进程挂掉，导致ActorLocationSender发送的消息没有确认，结果无法自动删除，每一分钟清理一次这种ActorLocationSender
        private async ETVoid StartAsync()
        {
            List<long> timeoutActorProxyIds = new List<long>();

            while (true)
            {
                await Context.Game.GetComponent<TimeComponent>().WaitAsync(10 * 1000);
                if (IsDisposed == true)
                {
                    return;
                }

                timeoutActorProxyIds.Clear();

                long timeNow = TimeUtils.Now();

                foreach (long id in m_dict.Keys)
                {
                    var sender = m_dict[id];
                    if (sender == null)
                    {
                        continue;
                    }
                    if (timeNow < sender.LastRecvTime + 60 * 1000)
                    {
                        continue;
                    }
                    timeoutActorProxyIds.Add(id);
                }

                foreach (long id in timeoutActorProxyIds)
                {
                    Remove(id);
                }
            }
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            foreach (var sender in m_dict.Values)
            {
                sender.Dispose();
            }
            m_dict.Clear();
        }

        public ActorLocationSender Get(long id)
        {
            if (id == 0)
            {
                throw new Exception("actor id is 0");
            }

            if (m_dict.TryGetValue(id, out ActorLocationSender sender))
            {
                return sender;
            }

            sender = Root.Factory.CreateWithId<ActorLocationSender>(id);
            sender.Parent = this;
            m_dict[id] = sender;
            return sender;
        }

        public void Remove(long id)
        {
            if (m_dict.TryGetValue(id, out ActorLocationSender sender) == false)
            {
                return;
            }
            m_dict.Remove(id);
            sender.Dispose();
        }
    }
}
