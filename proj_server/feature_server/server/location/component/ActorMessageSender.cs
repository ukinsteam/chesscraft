﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ETT.Server.Location
{
    // 知道对方的instanceId，使用这个类发actor消息
    public struct ActorMessageSender
    {
        public IPEndPoint Address { get; }

        public long ActorInstanceId { get; }

        public IRootComponent Root { get; }

        public ActorMessageSender(long actorid, IPEndPoint address, IRootComponent root)
        {
            ActorInstanceId = actorid;
            Address = address;
            Root = root;
        }

        public void Send(IActorLocationMessage message)
        {
            Session session = Root.GetComponent<NetworkInnerComponent>().Get(Address);
            message.ActorId = ActorInstanceId;
            session.Send(message);
        }

        public async ETTask<IActorResponse> Call(IActorRequest request)
        {
            Session session = Root.GetComponent<NetworkInnerComponent>().Get(Address);
            request.ActorId = ActorInstanceId;
            return (IActorResponse)await session.Call(request);
        }

        public async ETTask<IActorResponse> CallWithoutException(IActorRequest request)
        {
            Session session = Root.GetComponent<NetworkInnerComponent>().Get(Address);
            request.ActorId = ActorInstanceId;
            return (IActorResponse)await session.CallWithoutException(request);
        }
    }
}
