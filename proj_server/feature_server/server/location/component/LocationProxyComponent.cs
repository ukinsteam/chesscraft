﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using System.Net;

namespace ETT.Server.Location
{
    public class LocationProxyComponent : Component, IAwake
    {
        public IPEndPoint LocationAddress;

        public async void Awake()
        {
            LocationAddress = await Context.Game.GetComponent<RouteTableComponent>().GetAddrAsync(EPeerType.Location);
        }

        public async ETTask Add(long key, long instanceid)
        {
            Session session = Entitas.GetComponent<NetworkInnerComponent>().Get(LocationAddress);
            await session.Call(new ObjectAddRequest() { Key = key, InstanceId = instanceid });
        }

        public async ETTask Remove(long key)
        {
            Session session = Entitas.GetComponent<NetworkInnerComponent>().Get(LocationAddress);
            await session.Call(new ObjectDelRequest() { Key = key });
        }

        public async ETTask<long> Get(long key)
        {
            Session session = Entitas.GetComponent<NetworkInnerComponent>().Get(LocationAddress);
            ObjectGetResponse response = (ObjectGetResponse)await session.Call(new ObjectGetRequest() { Key = key });
            return response.InstanceId;
        }

        public async ETTask Lock(long key, long instanceid, int time = 1000)
        {
            Session session = Entitas.GetComponent<NetworkInnerComponent>().Get(LocationAddress);
            await session.Call(new ObjectLockRequest() { Key = key, InstanceId = instanceid, Time = time });
        }

        public async ETTask Unlock(long key, long oldinstanceid, long newinstanceid)
        {
            Session session = Entitas.GetComponent<NetworkInnerComponent>().Get(LocationAddress);
            await session.Call(new ObjectUnLockRequest() { Key = key, OldInstanceId = oldinstanceid, NewInstanceId = newinstanceid });
        }
    }
}
