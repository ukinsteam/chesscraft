﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    /// <summary>
    /// Id [3000,4000)
    /// </summary>
    public enum ELocationProto
    {
        Req_ObjectAdd = 3000,
        Res_ObjectAdd = 3001,
        Req_ObjectDel = 3002,
        Res_ObjectDel = 3003,
        Req_ObjectGet = 3004,
        Res_ObjectGet = 3005,
        Req_ObjectLock = 3006,
        Res_ObjectLock = 3007,
        Req_ObjectUnLock = 3008,
        Res_ObjectUnLock = 3009,
    }

    public static class LocationDefine
    {
    }
}
