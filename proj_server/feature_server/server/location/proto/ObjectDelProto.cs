﻿using ETT.Model;
using ETT.Model.proto;
using System.IO;

namespace ETT.Server.Location
{
    [Message((int)ELocationProto.Req_ObjectDel)]
    public class ObjectDelRequest : AbstractRequest
    {
        public long Key { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(Key);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            Key = reader.ReadInt64();
        }
    }

    [Message((int)ELocationProto.Res_ObjectDel)]
    public class ObjectDelResponse : AbstractResponse
    {

    }
}
