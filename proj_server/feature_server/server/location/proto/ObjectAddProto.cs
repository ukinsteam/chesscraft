﻿using ETT.Model;
using ETT.Model.proto;
using System.IO;

namespace ETT.Server.Location
{
    [Message((int)ELocationProto.Req_ObjectAdd)]
    public class ObjectAddRequest : AbstractRequest
    {
        public long Key { get; set; }
        public long InstanceId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(Key);
            writer.Write(InstanceId);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            Key = reader.ReadInt64();
            InstanceId = reader.ReadInt64();
        }
    }

    [Message((int)ELocationProto.Res_ObjectAdd)]
    public class ObjectAddResponse : AbstractResponse
    {

    }
}
