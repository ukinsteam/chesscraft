﻿using ETT.Model;
using ETT.Model.proto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Server.Location
{
    [Message((int)ELocationProto.Req_ObjectUnLock)]
    public class ObjectUnLockRequest : AbstractRequest
    {
        public long Key { get; set; }
        public long OldInstanceId { get; set; }
        public long NewInstanceId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(Key);
            writer.Write(OldInstanceId);
            writer.Write(NewInstanceId);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            Key = reader.ReadInt64();
            OldInstanceId = reader.ReadInt64();
            NewInstanceId = reader.ReadInt64();
        }
    }

    [Message((int)ELocationProto.Res_ObjectUnLock)]
    public class ObjectUnLockResponse : AbstractResponse
    {

    }
}
