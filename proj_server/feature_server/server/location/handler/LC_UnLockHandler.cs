﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    [MessageHandler(EPeerType.Location)]
    public class LC_UnLockHandler : AbstractRpcHandler<ObjectUnLockRequest, ObjectUnLockResponse>
    {
        protected override async ETTask Run(Session session, ObjectUnLockRequest request, ObjectUnLockResponse response, Action reply)
        {
            session.Root.GetComponent<LocationComponent>().UnLockAndUpdate(request.Key, request.OldInstanceId, request.NewInstanceId);

            reply();

            await ETTask.CompletedTask;
        }
    }
}
