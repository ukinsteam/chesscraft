﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    [MessageHandler(EPeerType.Location)]
    public class LC_LockHandler : AbstractRpcHandler<ObjectLockRequest, ObjectLockResponse>
    {
        protected override async ETTask Run(Session session, ObjectLockRequest request, ObjectLockResponse response, Action reply)
        {
            session.Root.GetComponent<LocationComponent>().Lock(request.Key, request.InstanceId, request.Time).Coroutine();

            reply();

            await ETTask.CompletedTask;
        }
    }
}
