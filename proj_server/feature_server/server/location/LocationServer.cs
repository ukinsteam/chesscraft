﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.Location;

namespace ETT.Server
{
    public class LocationServer : AbstractPeer, IAwake<string>
    {
        public override EPeerType PeerType => EPeerType.Location;

        public void Awake(string addr)
        {
            Root = this;
            InnerAddr = addr;

            var comp = AddComponent<NetworkInnerComponent, string>(addr);
            comp.SetServiceName("location_inner");

            AddComponent<MessageDispatchComponent>();
            AddComponent<RouteComponent>();
            AddComponent<LocationComponent>();
        }
    }
}
