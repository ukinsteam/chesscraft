namespace ETT.Server.WuZiQi
{
	/// <summary>
	/// Battle协议枚举 Range[35000,40000)
	/// <summary>
	public enum EBattleProto
	{
		C2W_Req_Place = 35100,
		W2C_Res_Place = 35101,
		W2C_Ntf_Place = 35102,
		W2C_Ntf_Turn = 35130,
		W2C_Ntf_GameOver = 35150,
	}

}
