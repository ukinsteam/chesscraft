﻿using ETT.Server.World;

namespace ETT.Server.WuZiQi
{
    /// <summary>
    /// 棋手类
    /// </summary>
    public class TeamMember : AbstractTeamMember
    {
        public override void TurnOn()
        {
            W2C_Ntf_Turn ntf = new W2C_Ntf_Turn()
            {
                isOnTurn = true
            };
            Unicast(ntf);
        }

        public override void TurnOff()
        {
            W2C_Ntf_Turn ntf = new W2C_Ntf_Turn()
            {
                isOnTurn = false
            };
            Unicast(ntf);
        }
    }
}
