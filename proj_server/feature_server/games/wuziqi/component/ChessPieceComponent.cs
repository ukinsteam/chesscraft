﻿using ETT.AI.Common;
using ETT.Model;
using System;
using System.Collections.Generic;

namespace ETT.Server.WuZiQi
{
    public class ChessPieceComponent : Component, IAwake<ChessmanTableConf, FootholdTableConf>
    {
        public ChessmanTableConf ChessmanConf { get; private set; }

        public FootholdTableConf FootholdConf { get; private set; }

        public void Awake(ChessmanTableConf chessman, FootholdTableConf foothold)
        {
            ChessmanConf = chessman;
            FootholdConf = foothold;
        }
    }
}
