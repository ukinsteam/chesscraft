﻿using ETT.Model;
using ETT.Server.World;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.WuZiQi
{
    /// <summary>
    /// 五子棋
    /// </summary>
    public class ChessEntitas : Entitas, IAwake
    {
        public void Awake()
        {
            AddComponent<RoomManager>();
        }
    }
}
