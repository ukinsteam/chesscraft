using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Server.Common
{

	/// <summary>
	/// 登陆协议 【Client, Login】
	/// </summary>
	[Message((uint)ECommonProto.C2L_Req_Login)]
	public class C2L_Req_Login : AbstractRequest
	{
		public string name {get; set;}
		public string pwd {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(name);
			writer.Write(pwd);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			name = reader.ReadString();
			pwd = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆协议 【Login, Client】
	/// </summary>
	[Message((uint)ECommonProto.L2C_Res_Login)]
	public class L2C_Res_Login : AbstractResponse
	{
		public long key {get; set;}
		public string addr {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(addr);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			addr = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆网关 【Client, Gate】
	/// </summary>
	[Message((uint)ECommonProto.C2G_Req_Login)]
	public class C2G_Req_Login : AbstractRequest
	{
		public long key {get; set;}
		public string AccountName {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(AccountName);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			AccountName = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆网关 【Gate, Client】
	/// </summary>
	[Message((uint)ECommonProto.G2C_Res_Login)]
	public class G2C_Res_Login : AbstractResponse
	{
		public long playerid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(playerid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			playerid = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 登陆查询 【Login, DB】
	/// </summary>
	[Message((uint)ECommonProto.L2D_Req_Login)]
	public class L2D_Req_Login : AbstractRequest
	{
		public string name {get; set;}
		public string pwd {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(name);
			writer.Write(pwd);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			name = reader.ReadString();
			pwd = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆查询反馈 【DB, Login】
	/// </summary>
	[Message((uint)ECommonProto.D2L_Res_Login)]
	public class D2L_Res_Login : AbstractResponse
	{
		public long AccountId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(AccountId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			AccountId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 登陆 【Login, Gate】
	/// </summary>
	[Message((uint)ECommonProto.L2G_Req_Login)]
	public class L2G_Req_Login : AbstractRequest
	{
		public long AccountId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(AccountId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			AccountId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 登陆 【Gate, Login】
	/// </summary>
	[Message((uint)ECommonProto.G2L_Res_Login)]
	public class G2L_Res_Login : AbstractResponse
	{
		public long key {get; set;}
		public string gate_addr {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(gate_addr);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			gate_addr = reader.ReadString();
		}
	}

	/// <summary>
	/// 请求进入地图 【Client, Gate】
	/// </summary>
	[Message((uint)ECommonProto.C2G_Req_EnterMap)]
	public class C2G_Req_EnterMap : AbstractRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 进入地图响应 【Gate, Client】
	/// </summary>
	[Message((uint)ECommonProto.G2C_Res_EnterMap)]
	public class G2C_Res_EnterMap : AbstractResponse
	{
		public long UnitId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(UnitId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			UnitId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 请求创建单位 【Gate, World】
	/// </summary>
	[Message((uint)ECommonProto.G2W_Req_CreateUnit)]
	public class G2W_Req_CreateUnit : AbstractRequest
	{
		public long PlayerId {get; set;}
		public string PlayerName {get; set;}
		public long GateSessionId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(PlayerId);
			writer.Write(PlayerName);
			writer.Write(GateSessionId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			PlayerId = reader.ReadInt64();
			PlayerName = reader.ReadString();
			GateSessionId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 响应创建单位 【World, Gate】
	/// </summary>
	[Message((uint)ECommonProto.W2G_Res_CreateUnit)]
	public class W2G_Res_CreateUnit : AbstractResponse
	{
		public long UnitId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(UnitId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			UnitId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 断开连接 【Gate, World】
	/// </summary>
	[Message((uint)ECommonProto.G2W_Ntf_SessionDisconnect)]
	public class G2W_Ntf_SessionDisconnect : AbstractLocationMessage
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}
}
