﻿using ETT.Model;

namespace ETT.Server.World
{
    public interface ITurnBasedTeamMember
    {
        long Id { get; }
        Unit unit { get; }

        int TurnIndex { get; set; }
        void TurnOn();
        void TurnOff();

        void Quit();

        void Unicast(IActorLocationMessage message);
    }
}
