﻿using ETT.Model;
using System;
using System.Collections.Generic;

namespace ETT.Server.World
{
    public class Room : Entitas
    {
        public uint SerialId { get; set; }
        
    }

    public class RoomManager : Component, IAwake
    {
        private readonly Dictionary<uint, Room> m_dict = new Dictionary<uint, Room>();

        #region Random Id
        private static readonly uint N = 10;
        private static readonly uint MAX = 999999;
        private uint[] startArray = new uint[N];
        private HashSet<uint> m_roomIdSet = new HashSet<uint>();

        private uint FindNew(uint key)
        {
            uint temp = key;
            while (true)
            {
                if (m_roomIdSet.Contains(key) == true)
                {
                    key++;
                    key = key % MAX;
                    if (temp == key)
                    {
                        return uint.MaxValue;
                    }
                }
                else
                {
                    return key;
                }
            }
        }
        private uint GeneRoomId()
        {
            Random rand = new Random();
            for (int i = 0; i < N; i++)
            {
                int seed = rand.Next(100);
                seed = seed % 10;

                uint tem = startArray[seed];
                startArray[seed] = startArray[i];
                startArray[i] = tem;
            }
            uint result = 0;
            for (int i = 0; i < 6; i++)
            {
                result = result * 10 + startArray[i];
            }
            if (m_roomIdSet.Contains(result) == true)
            {
                result = FindNew(result);
                if (result == uint.MaxValue)
                {
                    Log.Error("Error! RoomId Broken");
                }
                else
                {
                    m_roomIdSet.Add(result);
                }
            }
            else
            {
                m_roomIdSet.Add(result);
            }
            return result;
        }
        #endregion

        public void Awake()
        {
            m_dict.Clear();

            for (uint i = 0; i < N; i++)
            {
                startArray[i] = i;
            }
            m_roomIdSet.Clear();
        }

        public Room Get(uint id)
        {
            m_dict.TryGetValue(id, out Room room);
            return room;
        }

        public void Add(Room room)
        {
            if (m_dict.ContainsKey(room.SerialId) == true)
            {
                Log.Error($"Room Is Exist Id={room.SerialId}");
                return;
            }
            room.SerialId = GeneRoomId();
            m_dict[room.SerialId] = room;
        }

        public void Remove(uint id)
        {
            if (m_dict.ContainsKey(id) == false)
            {
                return;
            }
            m_roomIdSet.Remove(m_dict[id].SerialId);
            m_dict[id].Dispose();
            m_dict.Remove(id);
        }
    }
}
