﻿using ETT.AI.Common;
using ETT.AI.ZhongGuoXiangQi;
using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.Location;
using ETT.Server.Table;
using ETT.Server.World;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.ZhongGuoXiangQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_KillHandler : AbstractActorRpcHandler<Unit, C2W_Req_Kill, W2C_Res_Kill>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_Kill request, W2C_Res_Kill response, Action reply)
        {
            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();

            var agent = unit.GetComponent<UnitAgentComponent>();

            bool is_game_over = false;

            TurnBasedTeamSystem sys_team = null;
            TeamMember member = null;
            EChessPieceFaction Faction = EChessPieceFaction.INVALID;
            int err_code = 0;
            if (agent.Room == null)
            {
                err_code = (int)ECommonErrCodeTable.NoRoom;
                goto ReplyLabel;
            }

            sys_team = agent.Room.GetComponent<TurnBasedTeamSystem>();
            if (sys_team.CurrUnit.Id != unit.Id)
            {
                err_code = (int)ECommonErrCodeTable.NotYourTurn;
                goto ReplyLabel;
            }

            var board_entity = agent.Room.GetComponent<ChessBoardEntity>();
            var attacker = board_entity[request.attackerid];
            var defender = board_entity[request.defenderid];
            if (attacker == null || attacker.IsAvailable == false ||
                defender == null || defender.IsAvailable == false)
            {
                err_code = (int)ECommonErrCodeTable.ChessmanIsNotAvailable;
                goto ReplyLabel;
            }

            var attacker_agent = attacker.GetComponent<ChessPieceComponent>();
            var defender_agent = defender.GetComponent<ChessPieceComponent>();

            member = unit.GetComponent<TeamMember>();
            Faction = (EChessPieceFaction)(member.TurnIndex + 1);
            if ((int)Faction != attacker.Faction)
            {
                err_code = (int)ECommonErrCodeTable.NotYourChessman;
                goto ReplyLabel;
            }

            if (attacker_agent.CanKill(defender.CurrPos) == false)
            {
                err_code = (int)EBattleErrCodeTable.CannotKill;
                goto ReplyLabel;
            }

            attacker_agent.Kill(defender.CurrPos);

            if (defender_agent.Chessmantype == EChessPieceType.General)
            {
                is_game_over = true;
                roomMgr.Remove(agent.Room.SerialId);
            }


            ReplyLabel:

            response.Error = err_code;
            reply();

            if (err_code == 0)
            {
                W2C_Ntf_Kill ntf = new W2C_Ntf_Kill()
                {
                    attackerid = request.attackerid,
                    defenderid = request.defenderid,
                };

                sys_team.Broadcast(ntf, unit.Id);
            }

            if (is_game_over == true)
            {
                W2C_Ntf_GameOver ntf = new W2C_Ntf_GameOver() { faction = (int)Faction };
                sys_team.Broadcast(ntf);
            }
            else
            {
                sys_team.NextTurn();
            }

            await ETTask.CompletedTask;
        }
    }
}
