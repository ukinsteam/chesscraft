﻿using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.World;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.ZhongGuoXiangQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_QuitRoomHandler : AbstractActorRpcHandler<Unit, C2W_Req_QuitRoom, W2C_Res_QuitRoom>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_QuitRoom request, W2C_Res_QuitRoom response, Action reply)
        {
            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();

            var agent = unit.GetComponent<UnitAgentComponent>();
            var room = agent.Room;

            int err_code = 0;
            if (room == null)
            {
                err_code = (int)ECommonErrCodeTable.NoRoom;
                goto ReplyLabel;
            }

            roomMgr.Remove(room.SerialId);
            agent.Room = null;

            unit.RemoveComponent<TeamMember>();

            ReplyLabel:

            response.Error = err_code;
            reply();

            await ETTask.CompletedTask;
        }
    }
}
