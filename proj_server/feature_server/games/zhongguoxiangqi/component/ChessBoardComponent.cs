﻿using ETT.AI.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.ZhongGuoXiangQi
{
    public class ChessBoardComponent : Component, IAwake
    {
        public void Awake()
        {
            TableComponent tableMgr = Context.Game.GetComponent<TableComponent>();
            ChessmanTable table = Context.Table.GetComponent<ChessmanTable>();

            var chess_board = GetParent<ChessBoardEntity>();

            var lst = table.FindAllVO();

            foreach (var conf in lst)
            {
                var chess_piece = Root.Factory.CreateWithId<ChessPieceEntity>();
                chess_board.Add(conf.Id, chess_piece);

                chess_piece.AddComponent<ChessPieceComponent, ChessmanTableConf>(conf);
                chess_piece.IsAvailable = true;
                chess_piece.Board = chess_board;
            }
        }
    }
}
