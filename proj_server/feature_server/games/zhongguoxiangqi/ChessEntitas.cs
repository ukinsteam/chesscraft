﻿using ETT.Model;
using ETT.Server.World;

namespace ETT.Server.ZhongGuoXiangQi
{
    /// <summary>
    /// 中国象棋
    /// </summary>
    public class ChessEntitas : Entitas, IAwake
    {
        public void Awake()
        {
            AddComponent<RoomManager>();
        }
    }
}
