namespace ETT.Server.ZhongGuoXiangQi
{
	/// <summary>
	/// Range [1000,2000)
	/// <summary>
	public enum EGenericWndTable
	{
		//登陆窗口
		Login = 1001,
		//大厅窗口
		Lobby = 1010,
		//匹配界面
		Match = 1011,
		//加入房间界面
		JoinRoom = 1012,
		//战斗窗口
		Battle = 1020,
		//操作窗口
		Operate = 1021,
	}

}
