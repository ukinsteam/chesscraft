namespace ETT.Server.ZhongGuoXiangQi
{
	/// <summary>
	/// Range [1000,2000)
	/// <summary>
	public enum EGenericModelTable
	{
		//路径点
		PathPoint = 1000,
		//精灵
		Elven = 1010,
		//哥布林
		Goblin = 1011,
		//人类
		Human = 1012,
		//亡灵
		Undead = 1013,
	}

	/// <summary>
	/// Range [0,1000)
	/// <summary>
	public enum EBattleModelTable
	{
		//红方_兵
		RedSolider = 1,
		//红方_炮
		RedCannon = 2,
		//红方_车
		RedChariot = 3,
		//红方_马
		RedKnight = 4,
		//红方_相
		RedMinister = 5,
		//红方_仕
		RedScholar = 6,
		//红方_帅
		RedGeneral = 7,
		//黑方_卒
		BlackSoldier = 8,
		//黑方_砲
		BlackCannon = 9,
		//黑方_车
		BlackChariot = 10,
		//黑方_马
		BlackKnight = 11,
		//黑方_象
		BlackMinister = 12,
		//黑方_士
		BlackScholar = 13,
		//黑方_将
		BlackGeneral = 14,
		//路径点
		PathPoint = 15,
	}

}
