﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Server;
using ETT.Server.Yaml;
using System;
using System.Threading;

namespace ETT
{
    class Program
    {
        static void Main(string[] args)
        {

            try
            {
                Log.Init(new NLogAdapter());

                SynchronizationContext.SetSynchronizationContext(ThreadSync.Instance);

                Context.Event.Add(DllType.Model, typeof(Context).Assembly);
                //Context.Event.Add(DllType.AI, typeof(ChessEntitas).Assembly);
                Context.Event.Add(DllType.Server, typeof(ServerMaster).Assembly);

                //Context.Proto.Scan();

                var yaml = Context.Game.AddComponent<YamlComponent, string>("yaml");

                Context.Game.AddComponent<CommandComponent>();
                Context.Game.AddComponent<ConsoleComponent>();
                Context.Game.AddComponent<ProtoComponent, bool>(true);
                Context.Game.AddComponent<TimeComponent>();
                Context.Game.AddComponent<MailBoxDispatcherComponent>();
                Context.Game.AddComponent<RouteTableComponent>();
                Context.Game.AddComponent<TableComponent, string>(yaml.ServerConf.Res_Path);
                Context.Game.AddComponent<CoroutineLockComponent>();

                foreach (var pair in yaml.ServerConf.Server)
                {
                    var addr = pair.Value;
                    switch (addr.Type.ToLower())
                    {
                        case "master":
                            Context.Game.AddComponent<ServerMaster, string>(addr.Inner);
                            break;
                        case "database":
                            Context.Game.AddComponent<DatabaseServer, string>(addr.Inner);
                            break;
                        case "world":
                            Context.Game.AddComponent<WorldServer, string>(addr.Inner);
                            break;
                        case "gate":
                            Context.Game.AddComponent<GateServer, string, string>(addr.Inner, addr.Outer);
                            break;
                        case "login":
                            Context.Game.AddComponent<LoginServer, string, string>(addr.Inner, addr.Outer);
                            break;
                        case "location":
                            Context.Game.AddComponent<LocationServer, string>(addr.Inner);
                            break;
                    }
                }


                while (true)
                {
                    try
                    {
                        Thread.Sleep(1);
                        ThreadSync.Instance.Update();
                        Context.Event.Update();
                    }
                    catch (Exception e)
                    {
                        Log.Error(e);
                    }
                }
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
